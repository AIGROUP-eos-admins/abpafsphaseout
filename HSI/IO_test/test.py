#!python
# coding: utf-8

# # Introduction
# In this notebook we tested and compare EOS performance with another file system. Using SWAN, the comparison is done with the local disk but one can use other volumes (e.g. AFS).

# In[1]:


import os
import time
import filecmp
import sys

# # Test on EOS

# In[18]:


filePath=sys.argv[1]


# In[19]:


# Initial test
# Creating M files with a size of file_size bytes and a sleepTime between two consecutive file writing.
# Each file is overwritten (this slows the loop).
M=200
sleepTime=.001
start = time.time()
file_name = 'test_file'
file_size = 1024 # size in bytes
for i in range(M):
    with open(filePath+file_name, "wb") as f:
        f.write(os.urandom(file_size))
    time.sleep(sleepTime)

end = time.time()
print('=============================')
print('TEST 1')
print('filePath=' +filePath)
print('M=' +str(M))
print('file_size=' +str(file_size)+' bytes')
print('sleepTime=' +str(sleepTime)+' second')
print('Execution is '+ str(end - start) + ' s.')
os.system('rm '+filePath+'test_file*');


# In[13]:


# This cell write MxN files on the filepath. N is the number of copies of the same file.
# The sleep time between the writing of the files is set be the variable sleepTime.
# The name of the file will start with file_name.
# A test is performed to verifies that all N copies are identical.

M=50
N=4
sleepTime=.000
file_name = 'test_file'
file_size = 1024 # size in bytes

start = time.time()
for i in range(M):
    myFile=os.urandom(file_size)
    for j in range(N):
        with open(filePath+file_name+'_'+str(i)+'_'+str(j), "wb") as f:
            f.write(myFile)
        time.sleep(sleepTime)

end = time.time()
print('=============================')
print('TEST 2')
print('filePath=' +filePath)
print('M (cycles)=' +str(M))
print('N (copies)=' +str(N))
print('file_size=' +str(file_size)+' bytes')
print('sleepTime=' +str(sleepTime)+' second')
print('Writing execution is '+ str(end - start) + ' s.')

start = time.time()
for i in range(M):
    for j in range(N):
        if not filecmp.cmp(filePath+file_name+'_'+str(i)+'_'+str(0),filePath+file_name+'_'+str(i)+'_'+str(j)):
            print('Problem with '+ filePath+file_name+'_'+str(i)+'_'+str(0))
end = time.time()
print('Reading execution is '+ str(end - start) + ' s.')

start = time.time()
for i in range(M):
    for j in range(N):
        os.remove(filePath+file_name+'_'+str(i)+'_'+str(j))
end = time.time()
print('Deleting execution is '+ str(end - start) + ' s.')



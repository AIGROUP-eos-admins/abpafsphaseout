#!/bin/bash

#######################################################################
#   Script to test PTC-PyORBIT on EOS
#######################################################################
# The following files should be present in the directory $PATHTEST for
# the examples to run
#
# setup_ifort.sh
# CheckGitStatus.sh
#
# these files can be found here:
# runfiles="/afs/cern.ch/user/p/pyorbit/public/pyorbit_examples_repository/AFS_Phaseout_Test"
# Let's assume you already have these from the examples repository
runfiles="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

if [ -z "$1" ]
  then
    echo "No test directory supplied." >&2
    exit 1
fi
# The script will stop on the first error
trap 'echo -ne "Error in $0:${LINENO}\nPWD=${PWD}\n\t${LINENO}\t" >&2; sed -n -e "${LINENO}p" "$0" >&2' ERR
set -e

# Create the test folder
mkdir -p "$1/HSI"
PATHTEST=$(mktemp --directory "$1/HSI/PyORBIT_HTC_XXXXXX")
export PATHTEST

echo "Starting on ${HOSTNAME} with eos-$(rpm -qf /usr/bin/eosxd --queryformat '%{V}-%{R}') at $(date) using ${PATHTEST}"

TESTLOG="${PATHTEST}/test.out"

STARTTIME=$( date '+%s.%03N' )
cd "$PATHTEST"

# Set matplotlib backend to Agg, else get '_tkinter.TclError: no display name' when checking results
export MPLBACKEND=Agg

# copy required files
cp $runfiles/setup_ifort.sh .
cp $runfiles/CheckGitStatus.sh .

# Source intel fortran libraries
source /cvmfs/projects.cern.ch/intelsw/psxe/linux/setup.sh
source /cvmfs/projects.cern.ch/intelsw/psxe/linux/x86_64/2019/compilers_and_libraries_2019.1.144/linux/bin/ifortvars.sh intel64
source /cvmfs/projects.cern.ch/intelsw/psxe/linux/x86_64/2019/compilers_and_libraries_2019.1.144/linux/bin/iccvars.sh intel64

# download and untar sources
echo "download and untar sources..." | tee -a "$TESTLOG"
curl --retry 5 --location -sS http://www.mpich.org/static/downloads/3.2/mpich-3.2.tar.gz | tar xz
curl --retry 5 --location -sS https://www.python.org/ftp/python/2.7.12/Python-2.7.12.tgz | tar xz
curl --retry 5 --location -sS http://zlib.net/fossils/zlib-1.2.11.tar.gz | tar xz
curl --retry 5 --location -sS http://www.fftw.org/fftw-3.3.5.tar.gz | tar xz
curl --retry 5 --location -sS https://files.pythonhosted.org/packages/eb/74/724d0dcc1632de285499ddd67035dd9313b84c28673add274b9c151dbb65/virtualenv-15.0.0.tar.gz | tar xz

# build python
echo "build python2.7..." | tee -a "$TESTLOG"
cd Python-2.7.12
CXX=g++ ./configure -prefix=`pwd`/..  &>> "$TESTLOG"
make   &>> "$TESTLOG"   # several warnings on STDERR
make install  &>> "$TESTLOG"
cd ..

# build zlib
echo "build zlib..."  | tee -a "$TESTLOG"
cd zlib-1.2.11   &>> "$TESTLOG"
./configure -prefix=`pwd`/..  &>> "$TESTLOG"
make  &>> "$TESTLOG"
make install &>> "$TESTLOG"
cd ..

# build mpi
echo "build mpich..." | tee -a "$TESTLOG"
cd mpich-3.2
./configure -prefix=`pwd`/.. --disable-fortran &>> "$TESTLOG"
make &>> "$TESTLOG"
make install &>> "$TESTLOG"
cd ..

# build fftw
echo "build fftw..." | tee -a "$TESTLOG"
cd fftw-3.3.5
./configure -prefix=`pwd`/.. --disable-fortran --enable-mpi MPICC=`pwd`/../bin/mpicc &>> "$TESTLOG"
make &>> "$TESTLOG"
make install &>> "$TESTLOG"
cd ..

# build python packages
echo "build python packages..." | tee -a "$TESTLOG"

PATH=`pwd`/bin/:$PATH
export PATH

PYTHON_VERSION=`python -c "from distutils import sysconfig; print sysconfig.get_config_var('VERSION');"`
export PYTHON_VERSION
echo "Python version is $PYTHON_VERSION" >> "$TESTLOG"

PYTHON_LIB_DIR=`python -c "from distutils import sysconfig; print sysconfig.get_config_var('LIBPL');"`
if [ -f $PYTHON_LIB_DIR/libpython${PYTHON_VERSION}.a ]
   then
	export PYTHON_ROOT_LIB=$PYTHON_LIB_DIR/libpython${PYTHON_VERSION}.a
	LIB_TYPE=static
   else
	export PYTHON_ROOT_LIB="-L $PYTHON_LIB_DIR -lpython${PYTHON_VERSION}"
	LIB_TYPE=dynamic
fi

echo "Found python library: ${PYTHON_LIB_DIR} will use $LIB_TYPE library" >> "$TESTLOG"

PYTHON_ROOT_INC=`python -c "from distutils import sysconfig; print sysconfig.get_config_var('INCLUDEPY');"`
export PYTHON_ROOT_INC
echo "Found Python include directory: $PYTHON_ROOT_INC" >> "$TESTLOG"

export PYTHONPATH=${PYTHONPATH}:${ORBIT_ROOT}/py:${ORBIT_ROOT}/lib
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${ORBIT_ROOT}/lib

cd virtualenv-15.0.0
../bin/python setup.py install &>> "$TESTLOG"

# Make and source virtual environment
cd ..
mkdir virtualenvs
cd virtualenvs
../bin/virtualenv py2.7 --python=../bin/python &>> "$TESTLOG"
cd py2.7/bin
source "$PATHTEST"/virtualenvs/py2.7/bin/activate

echo "Installing Python packages" | tee -a "$TESTLOG"
echo "--------------------------" >> "$TESTLOG"

# Install python packages
## several are pinned to be still Python2-compatible
echo "installing setuptools - Python2 version" >> "$TESTLOG"
./pip --quiet install setuptools==44 &>> "$TESTLOG"

echo "installing numpy..." >> "$TESTLOG"
./pip --quiet install numpy &>> "$TESTLOG"
echo "installing scipy..." >> "$TESTLOG"
./pip --quiet install scipy &>> "$TESTLOG"
echo "installing ipython - Python2 version" >> "$TESTLOG"
./pip install ipython==4 &>> "$TESTLOG"
echo "installing matplotlib..." >> "$TESTLOG"
./pip --quiet install matplotlib &>> "$TESTLOG"
echo "installing h5py..." >> "$TESTLOG"
./pip --quiet install h5py &>> "$TESTLOG"
echo "installing imageio - Python2 version" >> "$TESTLOG"
./pip --quiet install imageio==2.6 &>> "$TESTLOG"
echo "DONE" >> "$TESTLOG"
echo >> "$TESTLOG"
cd ../../..

# PyORBIT installation
pyorbit_dir=py-orbit

# clone pyorbit version from github:
git clone --quiet --branch=master https://github.com/hannes-bartosik/py-orbit.git $pyorbit_dir &>> "$TESTLOG"

# clone PTC from github
cd $pyorbit_dir/ext
git clone --quiet --branch=master https://github.com/hannes-bartosik/PTC.git &>> "$TESTLOG"
cd PTC
mkdir obj/
cd ../../..
# enable the compilation of PTC and disable compilation of ecloud
sed -i 's/.*#DIRS += .\/PTC*/DIRS += .\/PTC/' $pyorbit_dir/ext/Makefile
sed -i 's/.*DIRS += .\/ecloud*/#DIRS += .\/ecloud/' $pyorbit_dir/ext/Makefile

# activate environments
source $pyorbit_dir/customEnvironment.sh &>> "$TESTLOG"
source $pyorbit_dir/../virtualenvs/py2.7/bin/activate &>> "$TESTLOG"

# Compile PyORBIT
echo >> "$TESTLOG"
echo "Building pyORBIT..." | tee -a "$TESTLOG"
echo "-------------------" >> "$TESTLOG"
echo >> "$TESTLOG"
cd $pyorbit_dir
make clean &>> "$TESTLOG"
make &>> "$TESTLOG"   # warnings on STDERR
echo "pyORBIT Built" >> "$TESTLOG"
echo "-------------------" >> "$TESTLOG"

echo "Cloning pyORBIT examples repository" |tee -a "$TESTLOG"
echo "-----------------------------------" >> "$TESTLOG"

cd "$PATHTEST"
mkdir examples
cd examples
git clone --quiet https://:@gitlab.cern.ch:8443/pyorbit/pyorbit_examples.git . &>> "$TESTLOG"

cd "$PATHTEST"
test_run_dir='Simulation_Test_Runs'
mkdir "$test_run_dir"
cd "$test_run_dir"

testdir=`date +%Y%m%dT%H%M%S`
mkdir "$testdir"
cd "$testdir"

echo "Duplicate PS example for HTCondor submission" >> "$TESTLOG"
echo "--------------------------------------------" >> "$TESTLOG"

# Function creates N duplicate folders and submits N jobs to HTCondor, with hardcoded job log
# also ask for CPU info to be sent on updates, since 'condor_history' is not reliable
# also make the final 'rm junk.txt' (which doesn't exist) ignore the exit code, else get CONDOR mails..
Duplicate_and_Submit(){
	
	for (( c=1; c<=$1; c++ )) ; do
		echo "Duplicating PS_1p4GeV_Injection into directory $c" >> "$TESTLOG"
		cp -r "$PATHTEST"/examples/Machines/PS/PS_1p4GeV_Injection "$c"
		cd "$c"
		
		echo "Edit setup_environment.sh to point to this installed version of PyORBIT" >> "$TESTLOG"
		echo "-----------------------------------------------------------------------" >> "$TESTLOG"
		old_po_dir="/afs/cern.ch/user/p/pyorbit/public/PyOrbit_env/py-orbit"
		new_po_dir="$PATHTEST/py-orbit"
		sed -i "s|pyOrbit_dir=$old_po_dir|pyOrbit_dir=$new_po_dir|" setup_environment.sh
			
                sed -i -e "s;^log.*;log = ${PATHTEST}/CONDORLOG.${c};"  htcond.sub
                sed -i -e '/^log/a job_ad_information_attrs = RemoteUserCpu, RemoteSysCpu, RemoteWallClockTime, CumulativeSuspensionTime' htcond.sub
                sed -i -e "s;^notification.*;notification = Error;"  htcond.sub
		sed -i -e '/^rm junk.txt/s/$/ ||:/' run_mpiHTCondorjob_CH2.sh
		echo "Submitting Condor Job ${c}" | tee -a "$TESTLOG"
		echo "---------------------" >> "$TESTLOG"
		condor_submit -batch-name HSI_PyORBIT  htcond.sub &>> "$TESTLOG"
		cd ..
		
	done
}

#####################################################
# Change No of duplicate HTCondor simulations here: #
#####################################################
Number_of_duplicates=3
Duplicate_and_Submit $Number_of_duplicates

# run local job
echo "Starting local job on ${HOSTNAME} at $(date) using ${PATHTEST}" | tee -a "$TESTLOG"
echo "---------------------" >> "$TESTLOG"

cp -r "$PATHTEST"/examples/Machines/PS/PS_1p4GeV_Injection "$PATHTEST"/"$test_run_dir"/"$testdir"

cd "$PATHTEST"/"$test_run_dir"/"$testdir"/PS_1p4GeV_Injection

# Edit the setup_environment file
old_po_dir="/afs/cern.ch/user/p/pyorbit/public/PyOrbit_env/py-orbit"
new_po_dir="$PATHTEST/py-orbit"
sed -i "s|pyOrbit_dir=$old_po_dir|pyOrbit_dir=$new_po_dir|" setup_environment.sh

# setup environment manually
./START_local.sh pyOrbit.py 4 &>> "$TESTLOG"

echo "Finished local job" | tee -a "$TESTLOG"

# wait for local execution to finish
ENDTIME=$( date '+%s.%03N' )

### wait for HTCondor jobs to finish, and sum up the actual runtimes
RUNTIME=$( echo "$ENDTIME - $STARTTIME" | bc -l )
for (( c=1; c<=$Number_of_duplicates; c++ )) ; do
  joblog="${PATHTEST}/CONDORLOG.${c}"
  if [[ ! -s "${joblog}" ]]; then
     echo "$(date): HTCondor joblog '${joblog}' is still empty or missing, waiting"
     sleep 60
     if [[ ! -e "${joblog}" ]]; then
        echo "ERROR: HTCondor joblog ${joblog} still not created after 60sec - job submission failed" >&2
        exit 61
     fi
     if [[ ! -s "${joblog}" ]]; then
        echo "ERROR: HTCondor joblog ${joblog} still empty after 60sec - job submission failed" >&2
        exit 62
     fi
  fi
  condor_wait -status "${joblog}"
  echo "Condor Job ${c} is done" | tee -a "$TESTLOG"

  # condor_history is unreliable for these jobs, so have added CPu info to log. Last entry is valid.
  wallclocktime=$( grep RemoteWallClockTime "${joblog}" | tail -1 | cut -d' ' -f 3 )
  if [[ -z "${wallclocktime}" ]]; then
    echo "$(date): cannot get RemoteWallClockTime for ${c}.job by parsing ${joblog}" >&2
    exit 72
  fi
  RUNTIME=$( echo "$RUNTIME + $wallclocktime" | bc -l )
done

#######################################################################
#######################################################################
##    CHECK results

echo "All jobs done, checking results" | tee -a "$TESTLOG"


# Outputs can only be plotted when the simulations have completed tracking 25 turns
# Simulations are not complete until 500 turns are completed
Check_and_plot(){
	pushd "$1"
	
	# check if Check_Simulation_Status.py exists in this directory
	CSS_File=Check_Simulation_Status.py
	if [[ -f "$CSS_File" ]]; then
		#echo "$CSS_File exists"		
		# check if results file exists
		R_File=result.txt
		if [[ -f "$R_File" ]]; then	
			#echo "$R_File exists"			
			# read contents
			while IFS= read -r line
			do
				#echo "$line"
				if [ "$line" = 'True' ]; then
					echo >> "$TESTLOG"
					echo simulation  >> "$TESTLOG"
					echo "$1"  >> "$TESTLOG"
					echo SUCCEEDED >> "$TESTLOG"
					echo >> "$TESTLOG"
				else
					if [ "$line" = 'False' ]; then
						echo >> "$TESTLOG"
						echo simulation  >> "$TESTLOG"
						echo "$1"  >> "$TESTLOG"
						echo FAILED >> "$TESTLOG"
						echo >> "$TESTLOG"
                                                echo "simulation $1 FAILED" >&2
                                                exit 17  #
					fi
				fi
			done <"$R_File"			
		# results file doesn't exist
		else
			# Run Check_Simulation_Status.py
			python "$CSS_File"
			# Plot outputs
			python Plot_All_Outputs.py
		fi		
	fi
	popd
}

# Enter place where the results are
cd "$PATHTEST"/"$test_run_dir"/"$testdir"

# Assume all directories here are PyORBIT simulation tests
for d in "${PWD}"/*/ ; do
    echo "Checking simulation in directory $d" | tee -a "$TESTLOG"
    Check_and_plot "${d}"
done

echo "OK: all checked" | tee -a "$TESTLOG"
echo "test_complete_test_condor_time_runtime_value_for_our_high_level_driver: $RUNTIME"

rm -rf "$PATHTEST" >& /dev/null ||:
exit 0
# Count files in a directory
# ls -1q *.mat | wc -l

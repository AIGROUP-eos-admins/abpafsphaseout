#!/bin/bash

# setup path (e.g. the files in Gitlab's abpafsphaseout/LAT/placet_condor)
export PATHTEST=/afs/cern.ch/user/a/alatina/abpafsphaseout/LAT/placet_condor

# setup placet etc.
source /cvmfs/clicbp.cern.ch/x86_64-slc6-gcc62-opt/setup.sh

# prepare for storing the results
mkdir -p $PATHTEST/Results

# execute placet script
placet $PATHTEST/my_placet_script.tcl

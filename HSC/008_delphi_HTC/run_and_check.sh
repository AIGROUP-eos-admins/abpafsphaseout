#!/bin/bash

###
# Bash script for DELPHI installation, and run on HTCondor
# The repository is cloned from GitLab
# Then install GMP, MPFR and ALGLIB and compile the code
# Then we launch the code on HTContor.
###

###
# For more infos on AFS phaseout, see G.Iadarola presentation: https://indico.cern.ch/event/812709/
# and script example: https://github.com/giadarol/afs_phaseout_ecloud_test/blob/master/test_pyecloud.sh
###

###
# 2019-05-07: script created by N. Mounet (nicolas.mounet@cern.ch)
###

# The script will stop on the first error 
trap 'echo -ne "Error in $0:${LINENO}\nPWD=${PWD}\n\t${LINENO}\t" >&2; sed -n -e "${LINENO}p" "$0" >&2' ERR
set -e

# workaround for condor_wait breakage on AFS since 8.8.6
condor_wait() { while [[ "$1" == -* ]]; do shift; done;  while ! grep Job.terminated "$1" ; do sleep 10; done }

##############################
# Path definition
##############################

# Folder in which the tests will be performed 
if [ -z "$1" ]
  then
    echo "No test directory supplied."
    exit 1
fi
mkdir -p "$1/HSC"
PATHTEST=$(mktemp --directory "$1/HSC/008_delphi_HTC_XXXXXX")
export PATHTEST
echo "Starting on ${HOSTNAME} with eos-$(rpm -qf /usr/bin/eosxd --queryformat '%{V}-%{R}') at $(date) using ${PATHTEST}"

COMPILATION_OUTPUT="compilation.outerr"
CONDA_OUTPUT="conda.outerr"

cd "$PATHTEST"
STARTTIME=$( date '+%s.%03N' )

#####################################################
# Clone the repository using kerberos authentication
# WARNING: the repository should be made public for the purpose of the test
#####################################################
git clone --quiet https://:@gitlab.cern.ch:8443/IRIS/DELPHI.git
# we also need the Impedance python code
git clone --quiet https://:@gitlab.cern.ch:8443/IRIS/IW2D.git


##############################
# Compilation of the C++ code
##############################

printf "\n\n**********************\nCompiling DELPHI C++ code\n"

cd "$PATHTEST"/DELPHI/DELPHI
make > "$PATHTEST/$COMPILATION_OUTPUT" 2>&1

printf "Done\n**********************\n"


#########################################
# Download and install miniconda (python)
#########################################

printf "\n\n**********************\nInstalling Miniconda\n"

cd "$PATHTEST"
mkdir downloads
cd downloads

wget --progress='dot:giga' https://repo.anaconda.com/miniconda/Miniconda2-latest-Linux-x86_64.sh
# conda looks into $HOME/.conda/ ? should not.
# Similar for $HOME/.cache/pip
printf "using HOME=${PATHTEST}\n"
oldHOME="${HOME}"
export HOME="${PATHTEST}"
bash Miniconda2-latest-Linux-x86_64.sh -b -p "$PATHTEST"/miniconda2 > "$PATHTEST/$CONDA_OUTPUT" 2>&1
printf "Done\n**********************\n"


######################
# Activate miniconda #
######################
oldpython=$(which python)

printf "\n\n**********************\nActivating and installing dependencies\n"
source "${PATHTEST}/miniconda2/etc/profile.d/conda.sh"
conda activate "${PATHTEST}/miniconda2"
PYTHONEXE=$(which python)
echo "Python executable: $PYTHONEXE"
if [[ "$oldpython" = "$PYTHONEXE" ]]; then
  echo "ERROR: failed to activate miniconda python" >&2
  exit 17
fi

# Set matplotlib backend to Agg 
# (to avoid errors if display not avaialable)
export MPLBACKEND=Agg

##############################
# Install required libraries #
##############################
# overwriting log files is OK, we stop anyway on first error
pip install numpy > "$PATHTEST/$CONDA_OUTPUT" 2>&1
pip install scipy > "$PATHTEST/$CONDA_OUTPUT" 2>&1
pip install matplotlib > "$PATHTEST/$CONDA_OUTPUT" 2>&1

printf "Done\n**********************\n"

##################################
# Installation of DELPHI python code
##################################

printf "\n\n**********************\nAdding the PYTHON paths in conda activate script\n"

# pythonpath for general python tools
cd "$PATHTEST"/IW2D/PYTHON_codes_and_scripts/General_Python_tools
echo $'\n# Python path for General_Python_tools' >> "$PATHTEST"/miniconda2/bin/activate
echo "PYTHONPATH="`pwd`":\$PYTHONPATH" >> "$PATHTEST"/miniconda2/bin/activate
echo "export PYTHONPATH" >> "$PATHTEST"/miniconda2/bin/activate

# pythonpath for impedance library
cd "$PATHTEST"/IW2D/PYTHON_codes_and_scripts/Impedance_lib_Python
echo $'\n# paths for Impedance_lib_Python' >> "$PATHTEST"/miniconda2/bin/activate
echo "PYTHONPATH="`pwd`":\$PYTHONPATH" >> "$PATHTEST"/miniconda2/bin/activate
echo "export PYTHONPATH" >> "$PATHTEST"/miniconda2/bin/activate

# ld_library_path for DELPHI
cd "$PATHTEST"/DELPHI/DELPHI
echo $'\n# library path for DELPHI' >> "$PATHTEST"/miniconda2/bin/activate
echo "LD_LIBRARY_PATH="`pwd`":\$LD_LIBRARY_PATH" >> "$PATHTEST"/miniconda2/bin/activate
echo "export LD_LIBRARY_PATH" >> "$PATHTEST"/miniconda2/bin/activate

# pythonpath for DELPHI library
cd "$PATHTEST"/DELPHI/DELPHI_Python
echo $'\n# paths for DELPHI_Python' >> "$PATHTEST"/miniconda2/bin/activate
echo "PYTHONPATH="`pwd`":\$PYTHONPATH" >> "$PATHTEST"/miniconda2/bin/activate
echo "export PYTHONPATH" >> "$PATHTEST"/miniconda2/bin/activate
echo "PATH="`pwd`":\$PATH" >> "$PATHTEST"/miniconda2/bin/activate
echo "export PATH" >> "$PATHTEST"/miniconda2/bin/activate

source "$PATHTEST"/miniconda2/bin/activate "$PATHTEST/miniconda2"

printf "Done\n**********************\n"

printf "Switching back to real HOME\n"
export HOME="${oldHOME}"

### patch: want meaningful job names, and to run on EL7
sed -i -e '/^\s\+os.system("condor_submit/s/condor_submit/condor_submit -batch-name HSC_008_delphii MY.WantOS=el7/' "$PATHTEST"/DELPHI/DELPHI_Python/DELPHI.py

##############################
# Run on HTCondor
##############################

printf "\n\n**********************\nLaunching calculations on HTCondor\n"

DELPHI_RESULTS_PATH="$PATHTEST"/DELPHI_results
mkdir "$DELPHI_RESULTS_PATH"

cat > "$PATHTEST"/run_HTCondor.py << EOFrun
#!$PYTHONEXE

from particle_param import proton_param
from Impedance import imp_model_resonator,freq_param
from DELPHI import Qs_from_RF_param,longdistribution_decomp,DELPHI_wrapper
from string_lib import float_to_str
import os,json
import numpy as np

lxplusbatchDEL = 'launch'
electron, m0, clight, E0 = proton_param()
machine = 'LHC'

# scan definitions
planes = ['x','y']
dphase = 0.
Nbscan = np.arange(0.1,8.1,0.1)*1.e11 # bunch intensity
Mscan = np.array([1])
Qpscan = np.arange(-40,1)
dampscan = np.array([0.,0.02])

# some flags and miscellaneous
flagdamperimp = 0 # 1 to use frequency dependent damper gain (provided in Zd,fd)
d = None
freqd = None
strnorm = ['']
flagnorm = 0 # 1 if damper matrix normalized at current chromaticity (instead of at zero chroma)
nevery = 1 # downsampling of the impedance (take less points than in the full model)
kmax = 1 # number of converged eigenvalues (kmax most unstable ones are converged)
kmaxplot = 200; # number of kept and plotted eigenvalues (in TMCI plot)
flagm0 = True
flageigenvect = False
queue = '2nd' # LXBATCH queue (converted in DELPHI to a ht_condor one)

# DELPHI convergence criteria
crit = 5e-2
abseps = 1e-4
result_folder = '$DELPHI_RESULTS_PATH'

# machine and beam parameters (those not given by optics file)
circ = 26658.8832
R = circ/(2.*np.pi) # machine radius
E = 6500e9
Estr = float_to_str(round(E/1e9))+'GeV'
gamma = E*electron/E0
beta=np.sqrt(1.-1./(gamma**2))
f0=clight*beta/circ # rev. frequency
omega0=2.*np.pi*f0

# optics
Qx = 62.31
Qy = 60.32
avbetax = R/Qx
avbetay = R/Qy
alphap = 0.000347974726757016
print("gamma={}, R={}, av. beta function={} m, tune={}".format(gamma,R,avbetax,Qx))

# Broad-band impedance
Rt = 25e6 # shunt impedance in MOhm/m
fr = 2e9 # cutoff frequency in Hz
Q = 1 # quality factor
imp_mod, _ = imp_model_resonator(Rlist=Rt,frlist=fr,Qlist=Q,beta=avbetax,
                                 fpar=freq_param(fmin=10,fmax=1e13,ftypescan=2,
                                                 nflog=10,fminrefine=1e9,fmaxrefine=1e10,nrefine=200,fadded=[]),
                                 listcomp=['Zxdip','Zydip'])

# longitudinal parameters
h = 35640 # harmonic number
V = 12e6
taub = 1.e-9 # bunch length (sec)
sigmaz = taub*beta*clight/4. # RMS bunch length (m)
typelong = 'Gaussian' # Longitudinal distribution
eta = alphap-1./(gamma*gamma) # slip factor
Qs = 0.0019
omegas = Qs*omega0
print("eta={}, Qs={}, sigmaz={}".format(eta,Qs,sigmaz))
# Longitudinal distribution decomposition
g, a, b = longdistribution_decomp(taub, typelong=typelong)

# DELPHI run
DELPHI_params = {'Mscan': Mscan.tolist(),
                 'Qpscan': Qpscan.tolist(),
                 'dampscan': dampscan.tolist(),
                 'Nbscan': Nbscan.tolist(),
                 'omegasscan': [omegas],
                 'dphasescan': [dphase],
                 'omega0': omega0,
                 'Qx': Qx, 'Qy': Qy,
                 'gamma': gamma, 'eta': eta,
                 'a': a,'b': b,
                 'taub': taub, 'g': g.tolist(),
                 'planes': planes,
                 'nevery': nevery,
                 'particle': 'proton',
                 'flagnorm': flagnorm,'flagdamperimp': flagdamperimp,
                 'd': d, 'freqd': freqd,
                 'kmax': kmax, 'kmaxplot': kmaxplot,
                 'crit': crit, 'abseps': abseps, 
                 'flagm0': flagm0, 'flageigenvect': flageigenvect,
                 'lxplusbatch': lxplusbatchDEL,
                 'comment': "{}".format(machine),
                 'queue': queue, 'dire': result_folder+'/',
                 'flagQpscan_outside': True,
                 }

with open(os.path.join(result_folder,'DELPHI_parameters.json'), 'w') as f:
    f.write(json.dumps(DELPHI_params,indent=4))

tuneshiftQp, tuneshiftm0Qp = DELPHI_wrapper(imp_mod, Mscan, Qpscan, dampscan, Nbscan,
                                           [omegas], [dphase], omega0, Qx, Qy, gamma, eta, a, b, taub, g,
                                           planes, nevery=nevery, particle='proton', flagnorm=flagnorm,
                                           flagdamperimp=flagdamperimp, d=d, freqd=freqd, kmax=kmax, kmaxplot=kmaxplot,
                                           crit=crit, abseps=abseps, flagm0=flagm0, flageigenvect=flageigenvect,
                                           lxplusbatch=lxplusbatchDEL, comment="{}".format(machine),
                                           queue=queue, dire=result_folder+'/', flagQpscan_outside=True)

EOFrun

chmod +x "$PATHTEST"/run_HTCondor.py

mkdir "$PATHTEST"/HTCondor
cd "$PATHTEST"/HTCondor
"$PATHTEST"/run_HTCondor.py > "$PATHTEST"/run_HTCondor.out

ENDTIME=$( date '+%s.%03N' )
RUNTIME_init=$( echo "0$ENDTIME - 0$STARTTIME" | bc -l )


printf "HTCondor jobs submitted\n**********************\n"

############################# WAIT ########################
printf "Waiting for HTCondor jobs\n**********************\n"
# The above creates several jobs, each with own job file
#
declare -a RUNTIMES_jobs

for joblog in "${PATHTEST}"/HTCondor/HTCondor_job_*.log; do
  jobid_tmp="${joblog##*HTCondor_job_}"
  jobid="${jobid_tmp%.log}"
  if [[ ! -s "${joblog}" ]]; then
     echo "$(date): HTCondor joblog '${joblog}' is still empty or missing, waiting"
     sleep 60
     if [[ ! -e "${joblog}" ]]; then
        echo "ERROR: HTCondor joblog ${joblog} still not created after 60sec - job submission failed" >&2
        exit 61
     fi
     if [[ ! -s "${joblog}" ]]; then
        echo "ERROR: HTCondor joblog ${joblog} still empty after 60sec - job submission failed" >&2
        exit 62
     fi
  fi
  if condor_wait -status "$joblog"; then
    wallclocktime_tmp=$(condor_history -limit 1 -long "${jobid}" | grep RemoteWallClockTime | cut -d' ' -f 3)
    if [[ -z "${wallclocktime_tmp}" ]]; then
       ENDTIME_job=$( date '+%s.%03N' )
       wallclocktime_tmp=$( echo "0$ENDTIME_job - 0$STARTTIME" | bc -l )
       echo "WARN: HTCondor does not return RemoteWallClockTime for job ${jobid}, using wall clock" >&2
    fi
    RUNTIMES_jobs+=( "0${wallclocktime_tmp}" )
  else
    echo "ERROR: HTCondor issue with job ${jobid} logfile ${joblog}" >&2
    exit 62
  fi
done
RUNTIMES_flat="${RUNTIMES_jobs[@]}"
RUNTIME=$( echo "0${RUNTIME_init}+${RUNTIMES_flat// /+}" | bc -l)

############################# CHECK #######################

#####################################
# Check what was launched on HTCondor
#####################################

printf "\n\n**********************\nChecking calculations launched on HTCondor\n"

DELPHI_RESULTS_PATH="$PATHTEST"/DELPHI_results

cat > "$PATHTEST"/check_HTCondor.py << EOFcheck
#!$PYTHONEXE

from particle_param import proton_param
from Impedance import imp_model_resonator,freq_param
from DELPHI import Qs_from_RF_param,longdistribution_decomp,DELPHI_wrapper
from string_lib import float_to_str
from io_lib import write_ncol_file
import os
import numpy as np

lxplusbatchDEL = 'retrieve'
electron, m0, clight, E0 = proton_param()
machine = 'LHC'

# scan definitions
planes = ['x','y']
dphase = 0.
Nbscan = np.arange(0.1,8.1,0.1)*1.e11 # bunch intensity
Mscan = np.array([1])
Qpscan = np.arange(-40,1)
dampscan = np.array([0.,0.02])

# some flags and miscellaneous
flagdamperimp = 0 # 1 to use frequency dependent damper gain (provided in Zd,fd)
d = None
freqd = None
strnorm = ['']
flagnorm = 0 # 1 if damper matrix normalized at current chromaticity (instead of at zero chroma)
nevery = 1 # downsampling of the impedance (take less points than in the full model)
kmax = 1 # number of converged eigenvalues (kmax most unstable ones are converged)
kmaxplot = 200; # number of kept and plotted eigenvalues (in TMCI plot)
flagm0 = True
flageigenvect = False
queue = '2nd' # LXBATCH queue (converted in DELPHI to a ht_condor one)

# DELPHI convergence criteria
crit = 5e-2
abseps = 1e-4
result_folder = '$DELPHI_RESULTS_PATH'

# machine and beam parameters (those not given by optics file)
circ = 26658.8832
R = circ/(2.*np.pi) # machine radius
E = 6500e9
Estr = float_to_str(round(E/1e9))+'GeV'
gamma = E*electron/E0
beta=np.sqrt(1.-1./(gamma**2))
f0=clight*beta/circ # rev. frequency
omega0=2.*np.pi*f0

# optics
Qx = 62.31
Qy = 60.32
avbetax = R/Qx
avbetay = R/Qy
alphap = 0.000347974726757016
print("gamma={}, R={}, av. beta function={} m, tune={}".format(gamma,R,avbetax,Qx))

# Broad-band impedance
Rt = 25e6 # shunt impedance in MOhm/m
fr = 2e9 # cutoff frequency in Hz
Q = 1 # quality factor
imp_mod, _ = imp_model_resonator(Rlist=Rt,frlist=fr,Qlist=Q,beta=avbetax,
                                 fpar=freq_param(fmin=10,fmax=1e13,ftypescan=2,
                                                 nflog=10,fminrefine=1e9,fmaxrefine=1e10,nrefine=200,fadded=[]),
                                 listcomp=['Zxdip','Zydip'])

# longitudinal parameters
h = 35640 # harmonic number
V = 12e6
taub = 1.e-9 # bunch length (sec)
sigmaz = taub*beta*clight/4. # RMS bunch length (m)
typelong = 'Gaussian' # Longitudinal distribution
eta = alphap-1./(gamma*gamma) # slip factor
Qs = 0.0019
omegas = Qs*omega0
print("eta={}, Qs={}, sigmaz={}".format(eta,Qs,sigmaz))
# Longitudinal distribution decomposition
g, a, b = longdistribution_decomp(taub, typelong=typelong)

# DELPHI results retrieval

tuneshiftQp, tuneshiftm0Qp = DELPHI_wrapper(imp_mod, Mscan, Qpscan, dampscan, Nbscan,
                                           [omegas], [dphase], omega0, Qx, Qy, gamma, eta, a, b, taub, g,
                                           planes, nevery=nevery, particle='proton', flagnorm=flagnorm,
                                           flagdamperimp=flagdamperimp, d=d, freqd=freqd, kmax=kmax, kmaxplot=kmaxplot,
                                           crit=crit, abseps=abseps, flagm0=flagm0, flageigenvect=flageigenvect,
                                           lxplusbatch=lxplusbatchDEL, comment="{}".format(machine),
                                           queue=queue, dire=result_folder+'/', flagQpscan_outside=True)


np.testing.assert_array_equal(tuneshiftQp.shape,(2, 1, 41, 2, 80, 1, 1, 200),err_msg="Incorrect output")
np.testing.assert_array_equal(tuneshiftm0Qp.shape,(2, 1, 41, 2, 80, 1, 1),err_msg="Incorrect output")

assert not np.any(np.isnan(tuneshiftQp)), "{} nan found".format(len(np.any(np.isnan(tuneshiftQp))))
assert not np.any(np.isnan(tuneshiftm0Qp)), "{} nan found (m=0)".format(len(np.any(np.isnan(tuneshiftm0Qp))))
assert not np.all(tuneshiftQp==0.), "only zeros found"
assert not np.all(tuneshiftm0Qp==0.), "only zeros found (m=0)"

with open(os.path.join(result_folder,'Qs.txt'),'w') as f:
    f.write("{}\n".format(Qs))

np.save(os.path.join(result_folder,'TuneshiftQp.npy'), tuneshiftQp)
np.save(os.path.join(result_folder,'Tuneshiftm0Qp.npy'), tuneshiftm0Qp)

strpart=['Re','Im']

for iplane, plane in enumerate(planes):
    for iM, M in enumerate(Mscan):
        for idamp,damp in enumerate(dampscan):
            for Qp in Qpscan:
                for ir, r in enumerate(['real','imag']):

                    # Output files name for data vs Qp
                    fileoutdataNb = os.path.join(result_folder,
                        'data_vs_Nb_'+machine+'_'+Estr+'_'+str(M)+'b_d'+float_to_str(damp)+'_Qp'+float_to_str(Qp)+'_converged'+strnorm[flagnorm]+'_'+plane)
                    fileoutdataNbm0 = os.path.join(result_folder,
                        'data_vs_Nb_m0_'+machine+'_'+Estr+'_'+str(M)+'b_d'+float_to_str(damp)+'_Qp'+float_to_str(Qp)+'_converged'+strnorm[flagnorm]+'_'+plane)
                    fileoutdata_all = os.path.join(result_folder,
                        'data_vs_Nb_all_'+machine+'_'+Estr+'_'+str(M)+'b_d'+float_to_str(damp)+'_Qp'+float_to_str(Qp)+'_converged'+strnorm[flagnorm]+'_'+plane)

                    ts = getattr(tuneshiftQp[iplane,iM,np.where(Qpscan==Qp),idamp,:,0,0,0], r)
                    data = np.hstack((Nbscan.reshape((-1,1)), ts.reshape((-1,1))))
                    write_ncol_file(fileoutdataNb+'_'+r+'.dat', data, header="Nb\t"+strpart[ir]+"_tuneshift")

                    tsm0 = getattr(tuneshiftm0Qp[iplane,iM,np.where(Qpscan==Qp),idamp,:,0,0], r)
                    data = np.hstack((Nbscan.reshape((-1,1)), ts.reshape((-1,1))))
                    write_ncol_file(fileoutdataNbm0+'_'+r+'.dat', data, header="Nb\t"+strpart[ir]+"_tuneshiftm0")

                    all_unstable_modes = getattr(tuneshiftQp[iplane,iM,np.where(Qpscan==Qp),idamp,:,0,0,:], r)
                    data = np.hstack((Nbscan.reshape((-1,1)), all_unstable_modes.reshape((-1,kmaxplot))))
                    write_ncol_file(fileoutdata_all+'_'+r+'.dat', data, header="Nb\t"+strpart[ir]+"_tuneshift")

with open(os.path.join(result_folder,'Qs.txt')) as f:
    Qs_ = float(f.read())

assert Qs==Qs_, "Qs.txt file is incorrect"
EOFcheck

chmod +x "$PATHTEST"/check_HTCondor.py

"$PATHTEST"/check_HTCondor.py -dAgg > "$PATHTEST"/check_HTCondor.out

ENDTIME_REAL=$( date '+%s.%03N' )
RUNTIME_REAL=$( echo "0${ENDTIME_REAL} - 0${STARTTIME}" | bc -l )

if [ `cat $DELPHI_RESULTS_PATH/data_vs_Nb_*.dat|wc -l` -ne '79704' ]; then
    echo "ERROR: data_vs_Nb files are incomplete" >&2
    exit 1
fi

printf "Done\n**********************\n"

echo 'Test succeeded' >&2
echo "test_complete_test_condor_time_runtime_value_for_our_high_level_driver: ${RUNTIME}"
echo "test_runtime_real: ${RUNTIME_REAL}"
echo "test_runtimes_perjob: ${RUNTIMES_flat}"
# cleanup
rm -rf "${PATHTEST}" >& /dev/null ||:
exit 0

#!/bin/bash

# Clean environement
# [ -z "$CLEANED" ] && exec /bin/env -i CLEANED=1 /bin/bash "$0" "$@"


###
# Bash script for ImpedanceWake2D installation
# The repository is cloned from GitLab
# Then install GMP, MPFR and ALGLIB and compile the code
# Then we run the code on four examples and check the results
###

###
# For more infos on AFS phaseout, see G.Iadarola presentation: https://indico.cern.ch/event/812709/
# and script example: https://github.com/giadarol/afs_phaseout_ecloud_test/blob/master/test_pyecloud.sh
###

###
# 2019-05-02: script created by D.Amorim (damorim@cern.ch) and N. Mounet (nicolas.mounet@cern.ch)
# 2019-05-03: v2 version: use the install & test scripts inside the git repository
###

# The script will stop on the first error 
trap 'echo -ne "Error in $0:${LINENO}\nPWD=${PWD}\n\t${LINENO}\t" >&2; sed -n -e "${LINENO}p" "$0" >&2' ERR
set -e


##############################
# Path definition
##############################

# Folder in which the tests will be performed 
if [ -z "$1" ]
  then
    echo "No test directory supplied."
    exit 1
fi
mkdir -p "$1/HSC"
PATHTEST=$(mktemp --directory "$1/HSC/006_iw2d_XXXXXX")
export PATHTEST
echo "Starting on ${HOSTNAME} with eos-$(rpm -qf /usr/bin/eosxd --queryformat '%{V}-%{R}') at $(date) using ${PATHTEST}"

COMPILATION_OUTPUT="compilation.outerr"

cd "$PATHTEST"
     
#####################################################
# Clone the repository using kerberos authentication
# WARNING: the repository should be made public for the purpose of the test
#####################################################
git clone --quiet https://:@gitlab.cern.ch:8443/IRIS/IW2D.git
 
##############################
# Installation and compilation
##############################
     
printf "\n\n**********************\nInstalling the code\n"
     
"$PATHTEST"/IW2D/Install_scripts/install.sh > $PATHTEST/$COMPILATION_OUTPUT 2>&1
     
printf "Done\n**********************\n"
     
##############################
# Run and check
##############################
     
"$PATHTEST"/IW2D/Tests/run_and_check_IW2D.sh

# TO DO - add test specific check
## not reached if error occurs above
OK="true"

### generic
if [ -n "${OK}" ]; then
  echo 'Test succeeded' >&2
  # cleanup
  rm -rf "${PATHTEST}" >& /dev/null ||:
  exit 0
else
  echo 'Test failed' >&2
  echo "Test dir left in place: ${PATHTEST}"
  exit 1
fi

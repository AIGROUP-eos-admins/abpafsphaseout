#!/bin/bash
source /cvmfs/sft.cern.ch/lcg/views/LCG_93python3/x86_64-centos7-gcc7-opt/setup.sh
source /usr/share/Modules/init/sh
module load mpi/mvapich2/2.3
export MV2_ENABLE_AFFINITY=0
export OM_NUM_THREADS=3
mpirun -np 3 ./combi $1

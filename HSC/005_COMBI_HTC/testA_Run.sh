#!/usr/bin/bash

if [ -z "$1" ]
  then
    echo "No test directory supplied."
    exit 1
fi
PATHTEST=$1/HSC/afs_phaseout_temptests_combi

set -e
mkdir -p $PATHTEST
cp -r ./* $PATHTEST/
cd $PATHTEST
mkdir output
mkdir bin
source /cvmfs/sft.cern.ch/lcg/views/LCG_93python3/x86_64-centos7-gcc7-opt/setup.sh
module load mpi/mvapich2/2.3
make
condor_submit EOSTest.job

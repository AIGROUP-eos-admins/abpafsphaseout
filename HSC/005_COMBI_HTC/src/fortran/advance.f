C
C Increment XXX1 and Decrement XXX2
C
C

      SUBROUTINE ADVANCE(XXX1,XXX2,NPOS)

      IMPLICIT NONE
      INTEGER NPOS
      INTEGER XXX1(NPOS), XXX2(NPOS)
      
      INTEGER XXX1LAST,XXX2FIRST,IPOS

C Beam 1 increases position

      XXX1LAST=XXX1(NPOS)
      DO IPOS=NPOS,2,-1
         XXX1(IPOS)=XXX1(IPOS-1)
      ENDDO
      XXX1(1)=XXX1LAST

C Beam2 decreases position
      XXX2FIRST=XXX2(1)
      DO IPOS=1,NPOS-1
         XXX2(IPOS)=XXX2(IPOS+1)
      ENDDO
      XXX2(NPOS)=XXX2FIRST

      RETURN
      END

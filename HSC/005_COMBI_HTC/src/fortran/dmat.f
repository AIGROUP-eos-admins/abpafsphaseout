      SUBROUTINE DMAT(X,A,Y,N)
C***********************************************************************
C                                                                      *
C        Multiply vector REAL*8 with (6*6) matrix                      *
C        Output in second vector                                       *
C        Author:      W. Herr                                          *
C                                                                      *
C***********************************************************************
      IMPLICIT NONE
      REAL*8   X(6), Y(6)
      REAL*8   A(6,6)
      INTEGER  N

      INTEGER I,J

c     write(*,*) 'in dmat: x: ',x

      DO  2  I = 1,N
          Y(I) = 0.0
          DO  1  J = 1,N
              Y(I) = Y(I) + X(J)*A(I,J)
 1        CONTINUE
 2    CONTINUE

c     write(*,*) 'in dmat: y: ',y
      RETURN
      END

       SUBROUTINE RDFILL(filfila,filfilaSize,filfilb,filfilbSize,
     &     xxx1,xxx2,npos,NBCH1,NBCH2)
      
      IMPLICIT NONE
      
      INTEGER NBU,NBCH1,NBCH2
      INTEGER TRAIN1(3564)
      INTEGER TRAIN2(3564)
      INTEGER IP(8)
      INTEGER, dimension (:),allocatable :: posb1, posb2
      INTEGER xxx1(npos),xxx2(npos)


      INTEGER numgrp1,i4101,ipoi1
      INTEGER numgrp2,i4201,ipoi2

      CHARACTER*1600 INLINEA
      CHARACTER*1600 INLINEB
      INTEGER filfilaSize,filfilbSize
      character(len=filfilaSize) filfila
      character(len=filfilbSize) filfilb

      integer kn1(200), km1(200)
      integer kn2(200), km2(200)     
      integer npos
      
      integer i,j,k,kk,INP,nb,nbun1,nbun2,nb1,nb2,nbch
      write(6,*),'fill1: ', filfila
      write(6,*),'fill2: ', filfilb

C ---  READ IN THE BUNCH TRAIN STRUCTURE FROM 2 DIFFERENT FILES ONE PER BEAM
C
      nbu = 1
      numgrp1 = 8
      i4101 = 1
      ipoi1 = 1
      nbch1 = 0
      numgrp2 = 8
      i4201 = 1
      ipoi2 = 1
      nbch2 = 0

C------ Reading the filling scheme for Beam 1

      OPEN(52,FILE=filfila,STATUS='OLD',ACTION='READ')

 4100 read(52,5001,end=4101),INLINEA                  
      if((INLINEA(1:2).eq.'#N').or.(INLINEA(1:3).eq.'# n')) then
           read(52,*) numgrp1
           write(6,*) 'Number of groups beam1: ',numgrp1
           go to  4100
      endif
 5001 FORMAT(A1600)
      read(inlinea,*),(kn1(I),km1(I),I=1,numgrp1)     
      write(6,*),(kn1(I),km1(I),I=1,numgrp1)     

      do  4105  I = 1,numgrp1
          do  4104 j = ipoi1,ipoi1+kn1(i)-1
              train1(j) = km1(i)
              if(train1(J).eq.1) nbch1 = nbch1 + 1
 4104     continue
          ipoi1 = ipoi1+kn1(i)
 4105 continue

      i4101 = i4101 + 1
      go to  4100
 4101 continue

      i4101 = i4101 - 1
      write(*,*) I4101
      write(*,*) 'Number of bunch positions found beam1: ',ipoi1 - 1
      write(*,*) 'Number of bunches found beam1: ',nbch1
      nbun1 = ipoi1 - 1


C------ Reading the filling scheme for Beam 2

      CLOSE(UNIT=52)
      OPEN(UNIT=53,FILE=filfilb,STATUS='OLD',ACTION='READ')

 4200 read(53,5201,end=4201),INLINEB                  
      if((INLINEB(1:2).eq.'#N').or.(INLINEB(1:3).eq.'# n')) then
           read(53,*) numgrp2
           write(*,*) 'Number of groups beam2: ',numgrp2
           go to  4200
      endif
 5201 FORMAT(A1600)

      read(inlineb,*),(kn2(I),km2(I),I=1,numgrp2)     

      write(6,*),(kn2(I),km2(I),I=1,numgrp2)     
       do  4205  I = 1,numgrp2
          do  4204 j = ipoi2,ipoi2+kn2(i)-1
              train2(j) = km2(i)
              if(train2(J).eq.1) nbch2 = nbch2 + 1
 4204     continue
          ipoi2 = ipoi2+kn2(i)
 4205 continue

      i4201 = i4201 + 1
      go to  4200

 4201 continue

      CLOSE(UNIT=53)

      i4201 = i4201 - 1

      write(6,*) I4201
      write(6,*) 'Number of bunch positions found beam2: ',ipoi2 - 1
      write(6,*) 'Number of bunches found beam2: ',nbch2
      nbun2 = ipoi2 - 1

C----Number of positions must be the same for the 2 beams

      if(nbun1.ne.nbun2) then
           write(6,*) 'Wrong beam description ',nbun1, nbun2
           write(6,*) 'Therefore I stop ...'
           stop 2927
      endif

C --- number of possible collision points is 2 times nbun1
      npos = 2*nbun1
      nbch1  = nbch1
      nbch2  = nbch2
      nb    = nbch

C
C --- set the positions of the IPs
C
      do  2921  INP = 1,8
          IP(INP) = ((npos/8.)*(INP-1)) + 1
 2921 continue

      if(train1(nbu).ne.1) then
           write(6,*) 'There is no bunch at place ',nbu
           write(6,*) 'Therefore I stop ...'
           stop 2928
      endif

C 
C --- fill bunch positions
C

C      CALL XIZERO(xxx,14256)

      CALL XIZERO(xxx1,7128)
      CALL XIZERO(xxx2,7128)

      allocate(posb1(npos))
      allocate(posb2(npos))

      nb1 = 0
      nb2 = 0
      do  1101 I = 1,nbun1
          if(train1(i).eq.1) then
              nb1 = nb1 + 1
              posb1(nb1) = npos - 2*i + 1 + 2
              if(posb1(nb1).gt.npos) posb1(nb1) = posb1(nb1) - npos
              xxx1(posb1(nb1)) = nb1
          endif
          if(train2(i).eq.1) then
              nb2 = nb2 + 1
              posb2(nb2) = 2*(i - 1) + 1
              xxx2(posb2(nb2)) = nb2
          endif
 1101 continue
                
      write(6,*) 'Second bunch count: ',nb1,nb2
      if(nbu.gt.nb1) then
           write(6,*) 'Bunch number ',nbu,' does not exist'
           stop 881
      endif
      if(nb1.ne.nbch1) then
           write(6,*) 'Something wrong in bunch count !'
           stop 882
      endif
      if(nb2.ne.nbch2) then
           write(6,*) 'Something wrong in bunch count !'
           stop 883
      endif

      write(6,*) (posb1(k),k=1,nb1)
      write(6,*) (posb2(k),k=1,nb2)

      deallocate(posb1)
      deallocate(posb2)

      do  kk = 1,npos
          if((xxx1(kk).ne.0).or.(xxx2(kk).ne.0)) then
             write(6,8101) kk,xxx1(kk),xxx2(kk)
c              write(6,*) kk,xxx1(kk),xxx2(kk)
          endif
      enddo

      go to   777

C 9999 write(6,*) 'error opening file fill.in'

 8101 format(1X,'POS: ',I6,5X,2(I5,2X))
      stop 9999
 
777   RETURN
      END

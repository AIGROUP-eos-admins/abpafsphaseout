#include "const.h"
      SUBROUTINE whitenoise(b1,Np,xampl,yampl)
      integer Np,I
      double precision b1(NCOORD,Np)
      double precision xampl,yampl,xkick,ykick
      double precision rnd
      call RANDOM_NUMBER(rnd)
      xkick = (2*rnd-1)*xampl
      call RANDOM_NUMBER(rnd)
      ykick = (2*rnd-1)*yampl
      if(xkick.ne.0.0.and.ykick.ne.0.0) then
        do I=1,Np
          b1(2,I) = b1(2,I)+xkick
          b1(4,I) = b1(4,I)+ykick
        enddo
      else if(xkick.ne.0.0)then
        do I=1,Np
          b1(2,I) = b1(2,I) + xkick
        enddo
      else if(ykick.ne.0.0)then
        do I=1,Np
          b1(4,I) = b1(4,I) + ykick
        enddo
      endif
      return
      end subroutine whitenoise
      
      SUBROUTINE quadwhitenoise(b1,Np,xampl,yampl)
      integer Np,I
      double precision b1(NCOORD,Np)
      double precision xampl,yampl,xkick,ykick

      call RANDOM_NUMBER(rnd)
      xkick = (2*rnd-1)*xampl
      call RANDOM_NUMBER(rnd)
      ykick = (2*rnd-1)*yampl
      if(xkick.ne.0.0.and.ykick.ne.0.0) then
        do I=1,Np
          b1(2,I) = b1(2,I)+xkick*b1(1,I)
          b1(4,I) = b1(4,I)+ykick*b1(3,I)
        enddo
      else if(xkick.ne.0.0)then
        do I=1,Np
          b1(2,I) = b1(2,I) + xkick*b1(1,I)
        enddo
      else if(ykick.ne.0.0)then
        do I=1,Np
          b1(4,I) = b1(4,I) + ykick*b1(3,I)
        enddo
      endif
      return
      end subroutine quadwhitenoise

      SUBROUTINE gaussiannoise(b1,Np,ampl,mean,var)
      integer Np,I
      double precision b1(NCOORD,Np)
      double precision ampl(2),mean(2),stddev(2)
      double precision xkick,ykick,tmpy1,tmpy2
      parameter (pi = acos(0.0))

      do while(tmpx1==0)
        call RANDOM_NUMBER(rnd)
        xkick = rnd
      enddo
      call RANDOM_NUMBER(rnd)
      tmpx2 = rnd
      do while(tmpx1==0)
        call RANDOM_NUMBER(rnd)
        ykick = rnd
      enddo
      call RANDOM_NUMBER(rnd)
      tmpy2 = rnd

      xkick=ampl(1)*(mean(1) + var(1)*
     &    sqrt(-2 * log(xkick)) * cos(2 * pi * tmpx2))
      ykick=ampl(2)*(mean(2)+var(2)*
     &    sqrt(-2 * log(ykick)) * cos(2 * pi * tmpy2))

      if(xkick.ne.0.0.and.ykick.ne.0.0) then
        do I=1,Np
          b1(2,I) = b1(2,I)+xkick
          b1(4,I) = b1(4,I)+ykick
        enddo
      else if(xkick.ne.0.0)then
        do I=1,Np
          b1(2,I) = b1(2,I) + xkick
        enddo
      else if(ykick.ne.0.0)then
        do I=1,Np
          b1(4,I) = b1(4,I) + ykick
        enddo
      endif
      return
      end subroutine gaussiannoise
      
      SUBROUTINE sinext(b1,Np,ampl,freq,phase,t)
      integer Np,I
      double precision b1(NCOORD,Np)
      double precision ampl(2),freq(2),phase(2),t
      double precision xkick,ykick

      xkick = ampl(1)*sin(freq(1)*t+phase(1))
      ykick = ampl(2)*sin(freq(2)*t+phase(2))
      do I = 1,Np
        b1(2,I) = b1(2,I) + xkick
        b1(4,I) = b1(4,I) + ykick
      enddo
      end subroutine sinext
      
      SUBROUTINE sinnoise(b1,Np,ampl,freq,corr,t)
      integer Np,I
      double precision b1(NCOORD,Np)
      double precision ampl(2),freq(2),t,corr
      double precision xkick,ykick
      
      COMMON/COMBI_NOISE/ phase1,phase2
      double precision phase1,phase2

      parameter (pi = acos(0.0))

      if (mod(t,corr).eq.0) then
        call RANDOM_NUMBER(rnd)
        phase1 = rnd*2.0*pi
        call RANDOM_NUMBER(rnd)
        phase2 = rnd*2.0*pi
      endif
      xkick = ampl(1)*sin(freq(1)*t+phase1)
      ykick = ampl(2)*sin(freq(2)*t+phase2)
C      OPEN(UNIT=1,STATUS='UNKNOWN',ACCESS='APPEND')
C      write(1,*) xkick,ykick
C      CLOSE(UNIT=1)

      do I = 1,Np
        b1(2,I) = b1(2,I) + xkick
        b1(4,I) = b1(4,I) + ykick
      enddo
      end subroutine sinnoise

      subroutine transincnoise(b1,np,amplx,amply)
C      transverse incoherent noise
      implicit none
      integer Np,I
      double precision b1(NCOORD,Np)
      double precision amplx,amply,rnd1,rnd2
      do I = 1,Np
        call gaussrand2(rnd1,rnd2)
        b1(2,I) = b1(2,I) + rnd1*amplx
        b1(4,I) = b1(4,I) + rnd2*amply
      enddo
      end subroutine transincnoise

      SUBROUTINE ccamplnoise(b1,Np,xampl,yampl)
      integer Np,I
      double precision b1(NCOORD,Np)
      double precision xampl,yampl,xkick,ykick
      double precision rnd,bunLen
      call RANDOM_NUMBER(rnd)
      xkick = (2*rnd-1)*xampl
      call RANDOM_NUMBER(rnd)
      ykick = (2*rnd-1)*yampl
      if(xkick.ne.0.0.and.ykick.ne.0.0) then
        do I=1,Np
          b1(2,I) = b1(2,I)+xkick*b1(5,I)
          b1(4,I) = b1(4,I)+ykick*b1(5,I)
        enddo
      else if(xkick.ne.0.0)then
        do I=1,Np
          b1(2,I) = b1(2,I) + xkick*b1(5,I)
        enddo
      else if(ykick.ne.0.0)then
        do I=1,Np
          b1(4,I) = b1(4,I) + ykick*b1(5,I)
        enddo
      endif
      return
      end subroutine ccamplnoise
        
      

import numpy as np
import matplotlib.pyplot as plt
from parseOutput import *
import numpy.linalg as linalg
import random as rnd
import fit as fit
import os,sys
sys.path.append('/home/xbuffat/workspace/AFS/src/Sussix');
from Sussix import sussix, sussix_inp

def normalize(data):
    avg = np.average(data[1000:]);
    sig = np.std(data[1000:]);
    for i in range(len(data)):
        data[i] = (data[i]-avg)/sig;

def getYlims(freq,fft,xlims):
    myMin = 1E100;
    myMax = -1E100;
    for i in range(len(freq)):
        if freq[i] >= xlims[0] and freq[i] < xlims[1]:
            if fft[i] > myMax:
                myMax = fft[i];
            if fft[i] < myMin:
                myMin = fft[i];
    return [myMin,myMax];

def doSVDanalysis(inputdir,outputdir=None,nTurn=1E4,offset=0,maxNbMode=10,nBunch = 36):
    if outputdir == None:
        outputdir = inputdir;
    bunches = np.arange(1,nBunch+1);
    tbtMatrix = None;
    indexOffset = 0;
    for beam in [1,2]:
        if beam == 2:
            indexOffset = 2*len(bunches);
        for bunch in np.arange(len(bunches)):
            #if beam == 2 and bunches[bunch] == 2:
            #    continue;
            bpData = parseBeamParameter(inputdir+"B"+str(beam)+"b"+str(bunches[bunch])+".bparam");
            normalize(bpData[0]);
            normalize(bpData[2]);
            if tbtMatrix == None:
                if nTurn >= len(bpData[0]):
                    nTurn = len(bpData[0])-1;
                tbtMatrix = np.zeros((4*len(bunches),nTurn));
                #print np.shape(tbtMatrix);
            for j in range(np.shape(tbtMatrix)[1]):
                tbtMatrix[indexOffset+2*bunch][j] = bpData[0][offset+j];
                tbtMatrix[indexOffset+2*bunch+1][j] = bpData[2][offset+j];
                
    #tbtMatrix lines alternate H-V data first all B1 the all B1
    #tbtMatrix columns are corresponding turn by turn data
                
    print 'Data loaded ',np.shape(tbtMatrix),',SVD';
    u,s,v = linalg.svd(tbtMatrix);
    print 'SVD done,plot';
    cmap = plt.get_cmap('jet');
    for i in range(np.shape(u)[0]) if np.shape(u)[0] < maxNbMode else range(maxNbMode):
        #print [u[i][k] for k in range(np.shape(u)[1])];
        #print s[i];
    
        plt.figure(i);
        vector = [v[i][k] for k in range(np.shape(v)[1])];
        freq = t2f(np.arange(np.shape(v)[1]));
        fft = c2db(rfft(vector));
        plt.plot(freq,fft,color=cmap(1.0-float(i)/(np.shape(u)[0]-1)));
        xlims = [0.295,0.33];
        plt.xlim(xlims);
        plt.ylim(getYlims(freq,fft,xlims));
        plt.xlabel('Spectrum');plt.ylabel('Ampl [a.u.]');
        plt.savefig(outputdir+str(i)+'_spectrum.png');
        plt.close();
        myFile = open(outputdir+str(i)+'_spectrum.csv','w');
        myFile.write(str(freq[0]));
        for j in range(1,len(freq)):
            myFile.write(','+str(freq[j]));
        myFile.write('\n'+str(fft[0]));
        for j in range(1,len(fft)):
            myFile.write(','+str(fft[j]));
        myFile.close();
        
        sussix_inp(ir=1,turns=len(bpData[0]),tunex=0.315,tuney=0.315,istun=1,narm=100);
        out = sussix(vector,vector);
        sussixFile = open(outputdir+str(i)+'_sussix.csv','w');
        for j in range(len(out.ax)):
            sussixFile.write(str(out.ox[j])+','+str(out.ax[j])+'\n');
        sussixFile.close();
        
        
        plt.figure(i+100);
        plt.plot(np.arange(np.shape(v)[1]),[v[i][k] for k in range(np.shape(v)[1])],color=cmap(1.0-float(i)/(np.shape(u)[0]-1)));
        xampl = amplitude([v[i][k] for k in range(np.shape(v)[1])],span=10);
        [A,b] = fit.expFitLin(np.array(xampl[1]),np.array(xampl[0]));
        plt.plot([xampl[0][k] for k in range(len(xampl[0]))],[A*np.exp(b*xampl[0][k]) for k in range(len(xampl[0]))],'k');
        print i,b,1/b;
        plt.xlabel('Turn');plt.ylabel('Ampl [a.u.]');plt.title('Rise time : '+str(int(1/b))+' turn');
        plt.savefig(outputdir+str(i)+'_ampl.png');
        plt.close();
        riseTimeFile = open(outputdir+str(i)+'_riseTime.csv','w');
        riseTimeFile.write(str(1.0/b));
        riseTimeFile.close();
        
        fig = plt.figure(i+200);
        fig.add_subplot(211);
        plt.scatter(bunches , [u[k][i] for k in range(0,np.shape(u)[1]/2,2)],marker='x',color='b');
        plt.scatter(bunches, [u[k][i] for k in range(np.shape(u)[1]/2,np.shape(u)[1],2)],marker='x',color='r');
        plt.plot(bunches,[0.0 for k in range(len(bunches))],color='k');
        plt.xlim([bunches[0],bunches[-1]]);plt.ylim([-1,1]);
        plt.ylabel('Horizontal');
        fig.add_subplot(212);
        plt.scatter(bunches , [u[k][i] for k in range(1,np.shape(u)[1]/2,2)],marker='x',color='b');
        plt.scatter(bunches, [u[k][i] for k in range(np.shape(u)[1]/2+1,np.shape(u)[1],2)],marker='x',color='r');
        plt.plot(bunches,[0.0 for k in range(len(bunches))],color='k');
        plt.xlim([bunches[0],bunches[-1]]);plt.ylim([-1,1]);
        plt.xlabel('Bunch number');plt.ylabel('Veritcal');
        plt.savefig(outputdir+str(i)+'_bunch.png');
        plt.close();
        
        fig = plt.figure(i+300);
        fig.add_subplot(211);
        plt.scatter(bunches , [u[k][i] for k in range(0,np.shape(u)[1]/2,2)],marker='x',color='b');
        plt.scatter([1+bunches[-1]-bunches[k] for k in range(len(bunches))], [u[k][i] for k in range(np.shape(u)[1]/2,np.shape(u)[1],2)],marker='x',color='r');
        plt.plot(bunches,[0.0 for k in range(len(bunches))],color='k');
        plt.xlim([bunches[0],bunches[-1]]);plt.ylim([-1,1]);
        plt.ylabel('Horizontal');
        plt.title('B2 reversed');
        fig.add_subplot(212);
        plt.scatter(bunches , [u[k][i] for k in range(1,np.shape(u)[1]/2,2)],marker='x',color='b');
        plt.scatter([1+bunches[-1]-bunches[k] for k in range(len(bunches))], [u[k][i] for k in range(np.shape(u)[1]/2+1,np.shape(u)[1],2)],marker='x',color='r');
        plt.plot(bunches,[0.0 for k in range(len(bunches))],color='k');
        plt.xlim([bunches[0],bunches[-1]]);plt.ylim([-1,1]);
        plt.xlabel('Bunch number');plt.ylabel('Veritcal');
        plt.savefig(outputdir+str(i)+'_bunch_B2R.png');
        plt.close();
        
    plt.figure(1000);
    plt.plot(np.arange(len(s)),s);
    plt.xlabel('Rank');plt.ylabel('Eigenvalue');
    plt.savefig(outputdir+'eigenvalues.png');    

def do1PlaneSVDanalysis(inputdir,outputdir=None,nTurn=1E4,offset=0,maxNbMode=10,nBunch = 36,dataIndex = 0):
    if outputdir == None:
        outputdir = inputdir;
    bunches = np.arange(1,nBunch+1);
    tbtMatrix = None;
    indexOffset = 0;
    for beam in [1,2]:
        if beam == 2:
            indexOffset = len(bunches);
        for bunch in np.arange(len(bunches)):
            #if beam == 2 and bunches[bunch] == 2:
            #    continue;
            bpData = parseBeamParameter(inputdir+"B"+str(beam)+"b"+str(bunches[bunch])+".bparam");
            normalize(bpData[dataIndex]);
            if tbtMatrix == None:
                if nTurn >= len(bpData[dataIndex]):
                    nTurn = len(bpData[dataIndex])-1;
                tbtMatrix = np.zeros((2*len(bunches),nTurn));
                #print np.shape(tbtMatrix);
            for j in range(np.shape(tbtMatrix)[1]):
                tbtMatrix[indexOffset+bunch][j] = bpData[dataIndex][offset+j];
                
    #tbtMatrix lines alternate H-V data first all B1 the all B1
    #tbtMatrix columns are corresponding turn by turn data
                
    print 'Data loaded ',np.shape(tbtMatrix),',SVD';
    u,s,v = linalg.svd(tbtMatrix);
    print 'SVD done,plot';
    cmap = plt.get_cmap('jet');
    for i in range(np.shape(u)[0]) if np.shape(u)[0] < maxNbMode else range(maxNbMode):
        #print [u[i][k] for k in range(np.shape(u)[1])];
        #print s[i];
    
        plt.figure(i);
        vector = [v[i][k] for k in range(np.shape(v)[1])];
        freq = t2f(np.arange(np.shape(v)[1]));
        fft = c2db(rfft(vector));
        plt.plot(freq,fft,color=cmap(1.0-float(i)/(np.shape(u)[0]-1)));
        plt.xlim([0.295,0.33]);
        plt.xlabel('Spectrum');plt.ylabel('Ampl [a.u.]');
        plt.savefig(outputdir+str(i)+'_spectrum.png');
        plt.close();
        myFile = open(outputdir+str(i)+'_spectrum.csv','w');
        myFile.write(str(freq[0]));
        for j in range(1,len(freq)):
            myFile.write(','+str(freq[j]));
        myFile.write('\n'+str(fft[0]));
        for j in range(1,len(fft)):
            myFile.write(','+str(fft[j]));
        myFile.close();
        
        sussix_inp(ir=1,turns=len(bpData[dataIndex]),tunex=0.315,tuney=0.315,istun=1,narm=100);
        out = sussix(vector,vector);
        sussixFile = open(outputdir+str(i)+'_sussix.csv','w');
        for j in range(len(out.ax)):
            sussixFile.write(str(out.ox[j])+','+str(out.ax[j])+'\n');
        sussixFile.close();
        
        
        plt.figure(i+100);
        plt.plot(np.arange(np.shape(v)[1]),[v[i][k] for k in range(np.shape(v)[1])],color=cmap(1.0-float(i)/(np.shape(u)[0]-1)));
        xampl = amplitude([v[i][k] for k in range(np.shape(v)[1])],span=10);
        [A,b] = fit.expFitLin(np.array(xampl[1]),np.array(xampl[0]));
        plt.plot([xampl[0][k] for k in range(len(xampl[0]))],[A*np.exp(b*xampl[0][k]) for k in range(len(xampl[0]))],'k');
        print i,b,1/b;
        plt.xlabel('Turn');plt.ylabel('Ampl [a.u.]');plt.title('Rise time : '+str(int(1/b))+' turn');
        plt.savefig(outputdir+str(i)+'_ampl.png');
        plt.close();
        riseTimeFile = open(outputdir+str(i)+'_riseTime.csv','w');
        riseTimeFile.write(str(1.0/b));
        riseTimeFile.close();
        
        
    plt.figure(1000);
    plt.plot(np.arange(len(s)),s);
    plt.xlabel('Rank');plt.ylabel('Eigenvalue');
    plt.savefig(outputdir+'eigenvalues.png');

if __name__=="__main__":
    print 'loading data';
    analysis = 1;
    inputdir = '../../output/';
    
    outputdir = inputdir + 'SVD/';
    try:
        os.mkdir(outputdir);
    except OSError as err:
        print 'Cannot create directory ',outputdir,' : ',err.args;
    
    if analysis == 1:
        doSVDanalysis(inputdir,outputdir,nBunch=4);
        #do1PlaneSVDanalysis(inputdir,outputdir,nBunch=1,dataIndex=2);

    plt.show();

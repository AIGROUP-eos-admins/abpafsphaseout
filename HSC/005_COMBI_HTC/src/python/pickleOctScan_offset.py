import pickle,os
import numpy as np
import matplotlib.pyplot as plt
from parseOutput import *
import matplotlib as mpl

if __name__=="__main__":
    mpl.rcParams['font.size'] = 30.0
    IOcts = [-200.0,-100.0,-50.0,0.0,25.0,50.0,75.0,100.0]
    seps = [1.0,1.2,1.4,1.6,1.8,2.0,2.2,2.4,2.6]
    #IOcts = [0.0,25.0,50.0,75.0,100.0]
    #seps = [1.0,1.2,1.4,1.6,1.8,2.0]
    chroma = 15.0
    dGain = 0.02
    runCommand = ''
    quadFlag = '_noQuad'
    zFact = 1.5

    maxXs = np.zeros((len(IOcts),len(seps)),dtype=float)
    maxEmitX = np.zeros_like(maxXs)
    maxYs = np.zeros_like(maxXs)
    maxEmitY = np.zeros_like(maxXs)

    for iIOct,IOct in enumerate(IOcts):
        for isep,sep in enumerate(seps):
            print(IOct,sep)
            simKey = 'octScan'+quadFlag+'_chomra'+str(chroma)+'_dGain'+str(dGain)+'_Ioct'+str(IOct)+'_sep'+str(sep)+'_ZFact'+str(zFact)
            outputDir = '/hpcscratch/user/xbuffat/COMBI/output/octScan/offsetScan/'+simKey+'/'
            fileName = outputDir+"B1b1.bparam"
            if not os.path.exists(fileName):
                print(fileName,'does not exist')
                continue
            bpData = parseBeamParameter(fileName)

            if False:
                plt.figure(0)
                ampl = amplitude(bpData[0],span=10,step=100)
                #(a,b) = polyfit(ampl[0],ampl[1],1)
                lines = plt.plot(ampl[0],ampl[1],'-',label='H, '+str(IOct))
                color = lines[-1].get_color()

                ampl = amplitude(bpData[2],span=10,step=100)
                #(a,b) = polyfit(ampl[0],ampl[1],1)
                plt.plot(ampl[0],ampl[1],'--',label='V, '+str(IOct),color=color)

                plt.figure(1)
                plt.plot(np.arange(len(bpData[8])),bpData[8],label='H, '+str(IOct),color=color)

                plt.plot(np.arange(len(bpData[9])),bpData[9],'--',label='V, '+str(IOct),color=color)

            maxXs[iIOct,isep] = np.max(np.abs(bpData[0]))
            maxYs[iIOct,isep] = np.max(np.abs(bpData[1]))
            maxEmitX[iIOct,isep] = np.max(np.abs(bpData[8]))
            maxEmitY[iIOct,isep] = np.max(np.abs(bpData[9]))

    myFile = open('octScan_offset'+quadFlag+'_chomra'+str(chroma)+'_dGain'+str(dGain)+'_ZFact'+str(zFact)+'.pkl','wb')
    pickle.dump([np.array(IOcts),np.array(seps),maxXs,maxEmitX,maxYs,maxEmitY],myFile)
    myFile.close()

    if False:
        plt.figure(0)
        plt.xlabel('Turn')
        plt.ylabel('Amplitude')
        plt.grid()
        plt.legend(loc=0)

        plt.figure(1)
        plt.xlabel('Turn')
        plt.ylabel('Emittance')
        plt.grid()

        plt.show()

from parseSlicePosition import parse
import numpy as np
import matplotlib.pyplot as plt
import numpy.linalg as linalg
import os,pickle

def getTimeVectors(u,s,v,tbtMatrix):
    basis = u.dot(np.diag(s));
    basis = np.linalg.inv(basis);
    return basis.dot(tbtMatrix);

def expFitLin(y,x = None):
    if x is None: x = np.arange(len(y));
    [a,b] = np.polyfit(x,np.log(y),1);
    return [np.exp(b),a];

#turnFactor is the number of turn betweent two dump of the longitudinal distribution
#redFact allows to reduce the array size by considering a fraction of the sample
def doSVDAnalysis(inputdir,beam,bunch,showPlots = True,outputdir = None,plane='H',turnFactor=100,redFact=1,doPickle=False,nMode = 5):
    bunchID = 'B'+str(beam)+'b'+str(bunch);
    print('loading data for '+bunchID+' '+plane[0]);
    xFileName = inputdir+bunchID+'_'+plane[0]+'.spos';
    pxFileName = inputdir+bunchID+'_'+plane[1]+'.spos';
    cFileName = inputdir+bunchID+'_Q.spos'
    if (not os.path.exists(xFileName)) or (not os.path.exists(pxFileName)) or (not os.path.exists(cFileName)):
        print('No data available for '+bunchID);
        return False;
    dataX = parse(xFileName,redFact=redFact);
    nTurn = np.shape(dataX)[0]
    offset = 0
    dataX = np.array(dataX)[offset:nTurn,:]
    dataPX = np.array(parse(pxFileName,redFact=redFact))[offset:nTurn,:]
    data = np.transpose(dataX + 1j*dataPX)
    #data = np.transpose(np.multiply(dataX,dataPX))
    print(np.shape(data));
    print('SVD')
    u,s,v = linalg.svd(data,full_matrices=False);
    v = np.diag(s).dot(v)
    #phaseData = np.transpose(np.arctan2(dataPX,dataX))
    #phaseU = getSliceBasis(u,s,v,phaseData)
    #phaseModeAmpl = getTimeVectors(u,s,v,amplData)
    #print(np.shape(v),np.shape(phaseModeAmpl))
    print('Plot')
    print(s)
    amplColorMap = plt.get_cmap('YlOrRd');
    phaseColorMap = plt.get_cmap('hsv');
    for i in range(nMode):
        vect = np.array([u[k][i] for k in range(len(u))])#,color=phaseColorMap(float(i)/nMode));
        nSlice = 30
        nRing = 30
        vect = vect.reshape(nSlice,nRing)
        vectForFFT = np.zeros_like(vect)
        for k in range(np.shape(vectForFFT)[0]):
            for l in range(np.shape(vectForFFT)[1]):
                vectForFFT[k][l]=vect[k][l]
        plt.figure(500+i)
        for k in range(np.shape(vectForFFT)[0]):
            plt.plot(np.arange(np.shape(vectForFFT)[1])/np.shape(vectForFFT)[1],np.unwrap(np.angle(vectForFFT[k,:]))/(2.0*np.pi))
        plt.xlabel('Azimuthal angle [2$\pi$]')
        plt.ylabel(r'Transverse phase [2$\pi$]')
        plt.grid()
        if not showPlots:
            plt.savefig(outputdir+bunchID+'_'+plane[0]+'_projTheta'+str(i)+'.png');
            plt.close();
        plt.figure(600+i)
        for k in range(np.shape(vectForFFT)[1]):
            plt.plot(np.arange(np.shape(vectForFFT)[0]),np.abs(vectForFFT[:,k]))
        plt.xlabel('Radius')
        plt.ylabel('Transverse amplitude')
        plt.grid()
        if not showPlots:
            plt.savefig(outputdir+bunchID+'_'+plane[0]+'_projR'+str(i)+'.png');
            plt.close();
        #########################
        plt.figure(200+i)
        fft = np.fft.fft2(vectForFFT)
        #for k in range(np.shape(fft)[0]):
        #    for l in range(np.shape(fft)[1]):
        #        fft[k][l]=fft[k][l]*(k+0.5)
        plt.contourf(np.arange(np.shape(vect)[1]),np.arange(np.shape(vect)[0]),np.transpose(np.log10(np.abs(fft))),20)
        plt.xlabel('R')
        plt.ylabel(r'$\theta$')
        plt.grid()
        plt.xlim([0,np.shape(vect)[1]/2])
        plt.ylim([0,np.shape(vect)[0]/2])
        if not showPlots:
            plt.savefig(outputdir+bunchID+'_'+plane[0]+'_fft'+str(i)+'.png');
            plt.close();
        #########################
        fig = plt.figure(100+i)
        vect = np.hstack([vect,vect[:,0:1]])
        X = np.arange(nSlice)
        Y = 2.0*np.pi*np.arange(nRing+1)/nRing
        print(np.shape(Y),np.shape(vect))
        ax = fig.add_subplot(111, projection='polar')
        p=ax.contourf(Y,X,np.angle(vect),np.arange(-np.pi,np.pi+1E-6,2.0*np.pi/20),cmap=phaseColorMap) # TODO unwrap ?
        plt.colorbar(p,ax=ax)
        if not showPlots:
            plt.savefig(outputdir+bunchID+'_'+plane[0]+'_modePhase'+str(i)+'.png');
            plt.close();
        fig = plt.figure(300+i)
        ax = fig.add_subplot(111, projection='polar')
        p=ax.contourf(Y,X,np.abs(vect),20,cmap=amplColorMap)
        plt.colorbar(p,ax=ax)
        if not showPlots:
            plt.savefig(outputdir+bunchID+'_'+plane[0]+'_modeAmpl'+str(i)+'.png');
            plt.close();
        fig = plt.figure(400+i)
        ax = fig.add_subplot(111, projection='polar')
        p=ax.contourf(Y,X,np.real(vect),20,cmap=amplColorMap)
        plt.colorbar(p,ax=ax)
        if not showPlots:
            plt.savefig(outputdir+bunchID+'_'+plane[0]+'_modeX'+str(i)+'.png');
            plt.close();

        plt.figure(1);
        cmap = plt.get_cmap('jet')
        x = np.arange(len(v[i]))/len(v[i])
        y = np.log10(np.abs(np.fft.fft([np.real(v[i][k]) for k in range(len(v[i]))])))
        plt.plot(x,y,color=cmap(float(i)/nMode),label='{0:d}:{1:.4f}'.format(i,x[np.argmax(y[:int(len(y)/2)])]));
        plt.xlim([0,0.5])
        plt.figure(2);
        x = np.arange(len(v[i]))
        y = [np.abs(v[i][k])*s[i] for k in range(len(v[i]))]
        [a,b] = expFitLin(y,x)
        print(a,b)
        plt.plot(x,y,color=cmap(float(i)/nMode),label='{0:d}:{1:.1f}'.format(i,1.0/b));
        plt.plot(x,a*np.exp(b*x),'--k')
        

    plt.figure(1)
    plt.xlabel('Turn');
    plt.ylabel(plane[0]+' freq');
    plt.legend(loc='upper left')
    plt.figure(2)
    plt.xlabel('Turn');
    plt.ylabel(plane[0]+' amplitude');
    plt.legend(loc='upper left')
    
    if doPickle:
        myFile = open(outputdir+'s.pkl','w');
        pickle.dump(s,myFile);
        myFile.close();
    
    if not showPlots:
        plt.figure(1);
        plt.savefig(outputdir+bunchID+'_'+plane[0]+'_freq.png');
        plt.close();
        plt.figure(2);
        plt.savefig(outputdir+bunchID+'_'+plane[0]+'_amplTime.png');
        plt.close();
    return True;

if __name__ == '__main__':
    showPlots = False;
    redFact = 1;
    nMode = 4;
    inputdir = "../../output/testRes/1.2E11_0_8E-2/";
        
    outputdir = inputdir + 'SVD/PizzaSlice/';
    try:
        os.makedirs(outputdir);
    except OSError as err:
        print('Cannot create directory ',outputdir,' : ',err.args);

    beam= 1
    bunch = 1
    for plane in [['x','X']]:
        ok = doSVDAnalysis(inputdir,beam,bunch,showPlots = showPlots,outputdir=outputdir,plane=plane,redFact=redFact,doPickle=False,nMode=nMode);
    print('done');
    if showPlots:
        plt.show()

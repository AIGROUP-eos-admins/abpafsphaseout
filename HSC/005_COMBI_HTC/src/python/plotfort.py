import csv
import numpy as np
from pylab import rfft
import matplotlib.pyplot as plt

def t2f(t):
    n=len(t)/2+1
    fs=1/(t[1]-t[0])
    return np.arange(0.,n)*fs/len(t)

def c2db(c):
    """complex signal to decibel"""
    return 20.*np.log10(abs(c))

def parse(fileName,ncolumn=3):
    data = [];
    for i in np.arange(ncolumn):
        data.append([]);
    reader = csv.reader(open(fileName, 'rb'), delimiter=' ');
    for row in reader:
        count = 0;
        for i in np.arange(len(row)):
            if count<ncolumn:
                if len(row[i])!=0:
                    data[count].append(float(row[i]));
                    count=count+1;
    return data;
    
def autocorr(x):
    return np.correlate(x,x);
    
if __name__ == '__main__':
    data = parse('../../fort.1');
#    data = parse('/home/xbuffat/stc/fort.1');
    plt.figure(0);
    plt.plot(np.arange(len(data[0])),data[0]);
    plt.plot(np.arange(len(data[1])),data[1]);
    plt.xlabel('line');plt.ylabel('value');plt.title('raw');
    plt.figure(1);
    plt.plot(t2f(np.arange(len(data[0]))),c2db(rfft(data[0])));
    plt.plot(t2f(np.arange(len(data[1]))),c2db(rfft(data[1])));
    plt.xlabel('Freq');plt.ylabel('ampl');plt.title('FFT');
    plt.figure(2);
    plt.scatter(data[0],data[1]);
    plt.xlabel('x'),plt.ylabel('y');plt.title('y(x)');
    
    print autocorr(data[0]);
    print autocorr(data[1]);
    print np.correlate(data[0],data[1]);
    
    plt.show();

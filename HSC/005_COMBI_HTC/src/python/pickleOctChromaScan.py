import pickle
import numpy as np
import matplotlib.pyplot as plt
from parseOutput import *
import matplotlib as mpl

if __name__=="__main__":
    mpl.rcParams['font.size'] = 30.0
    chromas = [0.0,5.0,10.0,15.0]
    IOcts = [100.0,150.0,175.0,200.0,225.0,250.0,275.0,300.0]
    dGain = 0.02
    runCommand = ''
    quadFlag = ''
    maxXs = np.zeros((len(IOcts),len(chromas)),dtype=float)
    maxEmitX = np.zeros_like(maxXs)
    maxYs = np.zeros_like(maxXs)
    maxEmitY = np.zeros_like(maxXs)
    for ichroma,chroma in enumerate(chromas):
        for iIOct,IOct in enumerate(IOcts):
            print(chroma,IOct)
            simKey = 'octScan_noBB'+quadFlag+'_chomra'+str(chroma)+'_dGain'+str(dGain)+'_Ioct'+str(IOct)
            outputDir = '/hpcscratch/user/xbuffat/COMBI/output/octScan/noBB/'+simKey+'/'
            bpData = parseBeamParameter(outputDir+"B1b1.bparam")

            if False:
                plt.figure(10+ichroma)
                ampl = amplitude(bpData[0],span=10,step=100)
                #(a,b) = polyfit(ampl[0],ampl[1],1)
                lines = plt.plot(ampl[0],ampl[1],'-',label='H, '+str(IOct))
                color = lines[-1].get_color()

                ampl = amplitude(bpData[2],span=10,step=100)
                #(a,b) = polyfit(ampl[0],ampl[1],1)
                plt.plot(ampl[0],ampl[1],'--',label='V, '+str(IOct),color=color)

                plt.figure(100+ichroma)
                plt.plot(np.arange(len(bpData[8])),bpData[8],label='H, '+str(IOct),color=color)
                plt.plot(np.arange(len(bpData[9])),bpData[9],'--',label='V, '+str(IOct),color=color)

            maxXs[iIOct,ichroma] = np.max(np.abs(bpData[0]))
            maxYs[iIOct,ichroma] = np.max(np.abs(bpData[1]))
            maxEmitX[iIOct,ichroma] = np.max(np.abs(bpData[8]))
            maxEmitY[iIOct,ichroma] = np.max(np.abs(bpData[9]))

        plt.figure(10+ichroma)
        plt.xlabel('Turn')
        plt.ylabel('Amplitude')
        plt.grid()
        plt.legend(loc=0)

        plt.figure(100+ichroma)
        plt.xlabel('Turn')
        plt.ylabel('Emittance')
        plt.grid()

    plt.figure(0)
    print(chroma,maxEmitX)
    plt.plot(IOcts,maxXs,'-x',label=str(chroma))

    myFile = open('octChromaScan_noBB'+quadFlag+'_dGain'+str(dGain)+'.pkl','wb')
    pickle.dump([np.array(IOcts),np.array(chromas),maxXs,maxEmitX,maxYs,maxEmitY],myFile)
    myFile.close()
    

    plt.figure(0)
    plt.xlabel('Octupole current [A]')
    plt.ylabel('Max. ampl')
    plt.grid()
    plt.legend(loc=0)

    plt.show()

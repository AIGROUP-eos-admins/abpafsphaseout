import numpy as np
import matplotlib.pyplot as plt
from parseOutput import *
import matplotlib as mpl

def getProfile(data,bins=100,fitPlot=True):
    events,edges = np.histogram(data,bins=bins);
    bin = np.arange(len(events),dtype=float);
    for i in np.arange(len(events)):
        bin[i] = (edges[i]+edges[i+1])/2.0;
    [x,fx],mu,sigma,myMax = fit.gaussianFit(events,bin);
    centeredData = [];
    for i in range(len(data)):
        centeredData.append(data[i]-mu);
    events,edges = np.histogram(centeredData,bins=bins);
#    plt.plot(bin,events,label=label,color=color,linestyle=linestyle);
#    if fitPlot:
#        plt.plot(x,fx,label=str(mu)+','+str(sigma),color='b',linestyle=linestyle);
#    plt.ylabel('count');
#    plt.legend();
    return mu,sigma,myMax,edges,events

if __name__=="__main__":
    mpl.rcParams['font.size'] = 30.0;
    xBins = 100;
    pxBins = 100;
    inputdir ="../../output/";
    xProfiles = [];
    pxProfiles = [];
    xWidth = [];
    pxWidth = [];
    turns = range(200,25000,200);
    for turn in turns:
        beamData = parseBeam(inputdir+"B1b1_t"+str(turn)+".beam");
        mux,sigmax,maxx,xBins,xProfile = getProfile(beamData[0],bins=xBins,fitPlot=False);
        xWidth.append(sigmax);
        xProfiles.append(xProfile);
        mupx,sigmapx,maxpx,pxBins,pxProfile = getProfile(beamData[1],bins=pxBins,fitPlot=False);
        pxWidth.append(sigmapx);
        pxProfiles.append(pxProfile);

    plt.figure(0);
    plt.imshow(np.flipud(xProfiles), aspect='auto', extent=[xBins[0],xBins[len(xBins)-1],turns[0],turns[len(turns)-1]]);
    plt.plot([-5.0*xWidth[0],-5.0*xWidth[0]],[turns[0],turns[len(turns)-1]],'-k');
    plt.plot([5.0*xWidth[0],5.0*xWidth[0]],[turns[0],turns[len(turns)-1]],'-k');
    
    emit0 = xWidth[0]*pxWidth[0];
    print emit0;
    plt.figure(1);
    plt.plot(turns,[xWidth[k]*pxWidth[k]/emit0 for k in range(len(xWidth))]);
    
    plt.show();
        

import numpy as np
import matplotlib.pyplot as plt
from parseOutput import *

if __name__=="__main__":
    analysis = 2;
    inputdir ='../../output/';
    bunches = [1,5];
    if analysis == 1:
        allRates = [];
        for beam in [1]:
            rates = [np.zeros(len(bunches)),np.zeros(len(bunches)),np.zeros(len(bunches)),np.zeros(len(bunches))];
            init = [np.zeros(len(bunches)),np.zeros(len(bunches))];
            lines = [];
            labels = [];
            for bunch in np.arange(len(bunches)):
                bpData = parseBeamParameter(inputdir+"B"+str(beam)+"b"+str(bunches[bunch])+".bparam");
                startFit = 1000;
                endFit = len(bpData[0]);
                if beam == 1:
                    fig = plt.figure(0);
                else:
                    fig = plt.figure(1);
                fig.add_subplot(211);
                lines.append(plt.plot(np.arange(len(bpData[8])),bpData[8],label=str(bunches[bunch])));
                labels.append(str(bunches[bunch]));
                (ax,bx) = polyfit(np.arange(startFit,endFit),[bpData[8][i] for i in np.arange(startFit,endFit)],1);
                axerr = getFitError(ax,bx,np.arange(startFit,endFit),[bpData[8][i] for i in np.arange(startFit,endFit)]);
                plt.plot(np.arange(startFit,endFit),polyval([ax,bx],np.arange(startFit,endFit)));
                fig.add_subplot(212);
                plt.plot(np.arange(len(bpData[9])),bpData[9],label=str(bunches[bunch]));
                (ay,by) = polyfit(np.arange(startFit,endFit),[bpData[9][i] for i in np.arange(startFit,endFit)],1);
                ayerr = getFitError(ay,by,np.arange(startFit,endFit),[bpData[9][i] for i in np.arange(startFit,endFit)]);
                plt.plot(np.arange(startFit,endFit),polyval([ay,by],np.arange(startFit,endFit)));
                rates[0][bunch] = ax;rates[1][bunch] = axerr;rates[2][bunch] = ay;rates[3][bunch] = ayerr;
                for i in np.arange(4):
                    rates[i][bunch] = 11245.0*100.0*rates[i][bunch];
                
                init[0][bunch] = np.average(bpData[8]);
                init[1][bunch] = np.average(bpData[9]);
	            
            if beam == 1:
                fig = plt.figure(0);
            else:
                fig = plt.figure(1);
            plt.title('B'+str(beam));
            plt.figlegend(lines,labels,'center right');
            ax = fig.add_subplot(211);
            plt.xlabel("turn");plt.ylabel("Horizontal emittance");
            fig.add_subplot(212);
            plt.xlabel("turn");plt.ylabel("Vertical emittance");

            if beam == 1:
                fig = plt.figure(2);
            else:
                fig = plt.figure(3);
            plt.title('B'+str(beam));
            plt.bar([bunches[k]-0.3 for k in np.arange(len(bunches))],rates[0],width=0.3,yerr=rates[1],color='b',label='horz');
            plt.bar([bunches[k] for k in np.arange(len(bunches))],rates[2],width=0.3,yerr=rates[3],color='r',label='vert');
    #        plt.twinx();
    #        plt.plot(bunches,init[0]);
    #        plt.plot(bunches,init[1]);
            plt.xlabel("bunch number");plt.ylabel("Emittance growth rate");plt.legend(loc=0);
            allRates.append(rates);
        plt.figure(4);
        plt.bar([bunches[k]-0.3 for k in np.arange(len(bunches))],[allRates[0][0][k]+allRates[0][2][k] for k in np.arange(len(rates[0]))],width=0.2,yerr=[abs(allRates[0][1][k])+abs(allRates[0][3][k]) for k in np.arange(len(allRates[0][0]))],color='b',label='tot B1');
        plt.bar([bunches[k]-0.1 for k in np.arange(len(bunches))],[allRates[1][0][k]+allRates[1][2][k] for k in np.arange(len(allRates[1][0]))],width=0.2,yerr=[abs(allRates[1][1][k])+abs(allRates[1][3][k]) for k in np.arange(len(allRates[1][0]))],color='r',label='tot B2');
        plt.bar([bunches[k]+0.1 for k in np.arange(len(bunches))],[allRates[0][0][k]+allRates[0][2][k]+allRates[1][0][k]+allRates[1][2][k] for k in np.arange(len(allRates[0][0]))],width=0.2,yerr=[abs(allRates[0][1][k])+abs(allRates[0][3][k])+abs(allRates[1][1][k])+abs(allRates[1][3][k]) for k in np.arange(len(allRates[0][0]))],color='g',label='tot');
        plt.xlabel('bunch number');plt.ylabel('Emittance growth rate');plt.legend();
    if analysis == 2:
        beam = 2;
        startFFT = 1000;
        cmap = plt.get_cmap('jet');
        for bunch in np.arange(len(bunches)):
            print bunch;
            bpData = parseBeamParameter(inputdir+"B"+str(beam)+"b"+str(bunches[bunch])+".bparam");
            plt.figure(0);
            plt.plot(t2f(np.arange(len(bpData[0])-startFFT)),c2db(rfft([bpData[0][i] for i in np.arange(startFFT,len(bpData[0]))])),label=str(bunch),color=cmap(float(bunch)/np.max(bunches)));
            plt.figure(1);
            plt.plot(t2f(np.arange(len(bpData[2])-startFFT)),c2db(rfft([bpData[2][i] for i in np.arange(startFFT,len(bpData[2]))])),label=str(bunch),color=cmap(float(bunch)/np.max(bunches)));
	plt.show();
        

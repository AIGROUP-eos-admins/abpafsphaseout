import pickle
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

if __name__=="__main__":
    chroma = 15.0
    dGain = 0.02
    runCommand = ''
    quadFlag = '_noQuad'
    zFact = 1.5

    myFile = open('octScan_offset'+quadFlag+'_chomra'+str(chroma)+'_dGain'+str(dGain)+'_ZFact'+str(zFact)+'.pkl','rb')
    [IOcts,seps,maxXs,maxEmitX,maxYs,maxEmitY] = pickle.load(myFile,encoding='latin-1')
    myFile.close()

    print(IOcts)

    thresPos = np.zeros_like(seps)
    thresPosLowerErr = np.zeros_like(seps)
    thresPosUpperErr = np.zeros_like(seps)
    thresNeg = np.zeros_like(seps)
    thresNegLowerErr = np.zeros_like(seps)
    thresNegUpperErr = np.zeros_like(seps)
    for i in range(len(thresPos)):
        plt.figure(0)
        plt.plot(IOcts,maxXs[:,i],'-x',label=str(seps[i]))
        plt.figure(2)
        plt.plot(IOcts,maxEmitX[:,i],'-x',label=str(seps[i]))

        tmpIOcts = IOcts[3:]
        
        stable = np.argwhere(np.logical_and(maxXs[3:,i]<1E-6,maxEmitX[3:,i]<1.2))
        print(stable)
        if len(stable) == 0:
            thresPos[i] = tmpIOcts[-1]
            thresPosLowerErr[i] = 0
            thresPosUpperErr[i] = 1E3
        else:
            iStable = stable[0,0]
            if iStable == 0 :
                thresPos[i] = tmpIOcts[0]
                thresPosLowerErr[i] = 0.0
                thresPosUpperErr[i] = tmpIOcts[0]
            else:
                thresPos[i] = (tmpIOcts[iStable-1]+tmpIOcts[iStable])/2
                thresPosLowerErr[i] = tmpIOcts[iStable-1]-thresPos[i]
                thresPosUpperErr[i] = thresPos[i]-tmpIOcts[iStable]
                print(seps[i],iStable,thresPos[i],thresPosLowerErr[i],thresPosUpperErr[i])

        tmpIOcts = IOcts[3:-1]
        
        stable = np.argwhere(np.logical_and(maxXs[3:-1,i]<1E-6,maxEmitX[3:-1,i]<1.2))
        print(stable)
        if len(stable) == 0:
            thresNeg[i] = tmpIOcts[-1]
            thresNegLowerErr[i] = 0
            thresNegUpperErr[i] = 1E3
        else:
            iStable = stable[0,0]
            if iStable == 0 :
                thresNeg[i] = tmpIOcts[0]
                thresNegLowerErr[i] = 0.0
                thresNegUpperErr[i] = tmpIOcts[0]
            else:
                thresNeg[i] = (tmpIOcts[iStable-1]+tmpIOcts[iStable])/2
                thresNegLowerErr[i] = tmpIOcts[iStable-1]-thresNeg[i]
                thresNegUpperErr[i] = thresNeg[i]-tmpIOcts[iStable]
                print(seps[i],iStable,thresNeg[i],thresNegLowerErr[i],thresNegUpperErr[i])

    plt.figure(1)
    plt.errorbar(seps,thresPos,yerr=[thresPosLowerErr,thresPosUpperErr])
    plt.errorbar(seps,thresNeg,yerr=[thresNegLowerErr,thresNegUpperErr])


    plt.figure(0)
    plt.grid()
    plt.legend(loc=0)

    plt.figure(2)
    plt.grid()
    plt.legend(loc=0)
    plt.show()

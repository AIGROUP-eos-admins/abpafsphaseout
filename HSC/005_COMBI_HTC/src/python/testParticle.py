import numpy as np
import matplotlib.pyplot as plt
import csv
import plotRoutines as pltRoutines
from pylab import rfft
from Sussix import sussix,sussixBS,sussix_inp

def parseBeam(fileName):
    data = [];
    reader = csv.reader(open(fileName, 'rb'), delimiter=' ');
    ncolumn = 0;
    for row in reader:
        if ncolumn == 0:
            ncolumn = len(row)-1;
            for i in np.arange(ncolumn):
                data.append([]);
        for i in np.arange(ncolumn):
            data[i].append(float(row[i]));
    return data;

def t2f(t):
    n=len(t)/2+1
    fs=1/(t[1]-t[0])
    return np.arange(0.,n)*fs/len(t)

def c2db(c):
    """complex signal to decibel"""
    return 20.*np.log10(abs(c))

def getTune(x):
    fft = c2db(rfft(x));
    f = t2f(np.arange(len(x)));
    mymax = -100000;
    retVal = -1;
    for i in np.arange(len(fft)):
        if fft[i]>mymax:
            mymax = fft[i];
            retVal = f[i];
    return retVal;
  
def getTunes(x,y):
    sussix_inp(ir=1,turns=len(x),tunex=0.31,tuney=0.32);
    sussixOut = sussix(x,y);
    return np.abs(sussixOut.tunexy);
    
def getTunesBS(x,px,y,py):
    sussix_inp(ir=1,turns=len(x),tunex=0.31,tuney=0.32);
#    sussix_inp(ir=0,turns=len(x),tunex=0.695,tuney=0.685);
    sussixOut = sussixBS(x,y,px,py);
    return np.abs(sussixOut.tunexy);
    
def getFootprintLines(xtunes,ytunes,size1,size2):
    linex = [];
    liney = [];
    for i in np.arange(0,size1-1,2):
        for j in np.arange(size2):
            linex.append(xtunes[i][j]);
            liney.append(ytunes[i][j]);
        for j in np.arange(size2-1,-1,-1):
            linex.append(xtunes[i+1][j]);
            liney.append(ytunes[i+1][j]);
    if size1%2 == 0:
        for j in np.arange(0,size2-1,2):
            for i in np.arange(size1-1,-1,-1):
                linex.append(xtunes[i][j]);
                liney.append(ytunes[i][j]);
            for i in np.arange(0,size1,1):
                linex.append(xtunes[i][j+1]);
                liney.append(ytunes[i][j+1]);
        if size2%2 != 0:
            for i in np.arange(size1-1,-1,-1):
                linex.append(xtunes[i][size2-1]);
                liney.append(ytunes[i][size2-1]);
            linex.append(xtunes[0][size2-2]);
            liney.append(ytunes[0][size2-2]);
    else:
        for j in np.arange(size2):
            linex.append(xtunes[size1-1][j]);
            liney.append(ytunes[size1-1][j]);
        for j in np.arange(size2-2,-1,-2):
            for i in np.arange(size1-1,-1,-1):
                linex.append(xtunes[i][j]);
                liney.append(ytunes[i][j]);
            for i in np.arange(0,size1,1):
                linex.append(xtunes[i][j-1]);
                liney.append(ytunes[i][j-1]);
        if size2%2 != 0:
            for i in np.arange(size1-1,-1,-1):
                linex.append(xtunes[i][0]);
                liney.append(ytunes[i][0]);
            linex.append(xtunes[0][1]);
            liney.append(ytunes[0][1]);
    return linex,liney;

if __name__ == "__main__":
    data = parseBeam("../../output/foot/B1b1.testpart");
#    data = parseBeam("/home/xbuffat/COMBI results/hfmm/testpart/5E-4Noise/1E6_1/B1b1.testpart");
    npart = len(data)/4;
    ampl=[0.0,6.0,7.0];
    angl=[0.0,1.570796325,2];
    analysis = 3;
    if analysis == 1:
        fact=1;
        k=0;
        for i in np.arange(ampl[2]):
            tmpampl = ampl[0]+i*(ampl[1]-ampl[0])/(ampl[2]-1);
            for j in np.arange(angl[2]):
                fig = plt.figure(j);
                fig.add_subplot(211);
                plt.scatter([data[k*4][z] for z in np.arange(0,len(data[k*4]),fact)],[data[k*4+1][z] for z in np.arange(0,len(data[k*4+1]),fact)],s=2,color=plt.cm.jet(tmpampl/ampl[1]));
                plt.xlabel("X");plt.ylabel("PX");
                fig.add_subplot(212);
                plt.scatter([data[k*4+2][z] for z in np.arange(0,len(data[k*4+2]),fact)],[data[k*4+3][z]for z in np.arange(0,len(data[k*4+3]),fact)],s=2,color=plt.cm.jet(tmpampl/ampl[1]));
                plt.xlabel("Y");plt.ylabel("PY");
                k=k+1;
            for j in np.arange(angl[2]):
                tmpangl = angl[0]+j*(angl[1]-angl[0])/(angl[2]-1);
                fig = plt.figure(j);
                plt.title("angle "+str(tmpangl));
    if analysis == 2:
        xtunes = [];
        ytunes = [];
        k = 0;
        for i in np.arange(angl[2]):
           xtunes.append(np.zeros(ampl[2]));
           ytunes.append(np.zeros(ampl[2]));
        for i in np.arange(int(ampl[2])):
            for j in np.arange(int(angl[2])):
                xtunes[j][i] = getTune(data[4*k]);
                ytunes[j][i] = getTune(data[4*k+2]);
                k=k+1;
        fig = plt.figure(100);
        for i in np.arange(angl[2],dtype=int):
  #      for i in [9]:
            tmpangl = angl[0]+i*(angl[1]-angl[0])/(angl[2]-1);
            fig.add_subplot(211);
            plt.scatter([ampl[0]+j*(ampl[1]-ampl[0])/(ampl[2]-1) for j in np.arange(int(ampl[2]))],xtunes[i],label=str(tmpangl),color=plt.cm.jet(tmpangl/angl[1]));
            fig.add_subplot(212);
            plt.scatter([ampl[0]+j*(ampl[1]-ampl[0])/(ampl[2]-1) for j in np.arange(int(ampl[2]))],ytunes[i],label=str(tmpangl),color=plt.cm.jet(tmpangl/angl[1]));
            #plt.legend();

    if analysis == 3:
        xtunes = [];
        ytunes = [];
        k = 0;
        xtunestest = [];
        ytunestest = [];
        startFFT = 0;
        endFFT = len(data[0]);
        for i in np.arange(ampl[2]):
           xtunes.append(np.zeros(angl[2]));
           ytunes.append(np.zeros(angl[2]));
        for i in np.arange(int(ampl[2])):
            for j in np.arange(int(angl[2])):
                tunes = getTunesBS([data[4*k][w] for w in np.arange(startFFT,endFFT)],[data[4*k+1][w] for w in np.arange(startFFT,endFFT)],[data[4*k+2][w] for w in np.arange(startFFT,endFFT)],[data[4*k+3][w] for w in np.arange(startFFT,endFFT)]);
#                tunes = getTunes([data[4*k][w] for w in np.arange(startFFT,endFFT)],[data[4*k+2][w] for w in np.arange(startFFT,endFFT)]);
                xtunes[i][j] = tunes[0];
                ytunes[i][j] = tunes[1];
#                xtunes[i][j] = getTune([data[4*k][w] for w in np.arange(startFFT,endFFT)]);
#                ytunes[i][j] = getTune([data[4*k+2][w] for w in np.arange(startFFT,endFFT)]);
                xtunestest.append(xtunes[i][j]);
                ytunestest.append(ytunes[i][j]);
                k=k+1;
        fig = plt.figure(200);
        linex,liney = getFootprintLines(xtunes,ytunes,int(ampl[2]),int(angl[2]));
        plt.plot(linex,liney);
        plt.scatter(xtunestest,ytunestest);
        
    plt.show();

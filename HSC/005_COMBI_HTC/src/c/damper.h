
#ifndef __DAMPERHEADER__
#define __DAMPERHEADER__

void perfectDamperKick(double* beam, int npart, double* bparam, double Hgain, double Vgain);
void thresholdedDamperKick(double* beam, int npart, double* bparam, double Hgain, double Vgain,double Hthreshold,double Vthreshold);
void singleTurnADT(double* beam,int npart,double* sliceMoments,int nSlice,double omega,double Hgain, double Vgain);
void singleTurnADT_debug(double* beam,int npart,double* sliceMoments,int nSlice,double omega,double Hgain, double Vgain,double* bparam,int ibeam);

#endif

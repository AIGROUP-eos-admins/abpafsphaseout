#include "extern.h"
#include "const.h"
#include "writer.h"
#include "mpi.h"
#include "defs.h"
#include "zkick.h"
#include "slice.h"
#include "parser.h"
#include "passageRecord.h"
#include "damper.h"
#include "collimator.h"
#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <string.h>
#include <math.h>
#include "action.h"
#include <sys/time.h>
#include <sys/resource.h>
#include <errno.h>
#include "omp.h"
//#include "FPPS/NonLinearMesh.h"
//#include "FPPS/ChangeCoord_Frac.h"
//#include "FPPS/ChargeDistribution.h"
//#include "FPPS/FastPolarPoissonSolver.h"
//#include "FPPS/ElectricFieldSolver.h"
//#include "FPPS/PolarBeamRepresentation.h"

# define NXG 100
# define NYG 100
# define NGRID ((2*NXG+1)*(2*NYG+1))

extern int TAG_INIT;
extern int TAG_ACTION;
extern int TAG_ARGS;
extern int TAG_PASSAGERECORD;
extern int TAG_PASSAGEARRAY;
extern int TAG_COMPLETION;
extern int TAG_SLAVETOSLAVE;
extern int TAG_SLAVETOSLAVE2;
extern int TAG_SLAVETOSLAVE3;

//this is kept there for performance considerations
float CHG[NGRID], POTG[NGRID],EXG[NGRID],EYG[NGRID];

void doSlaveJob(int myid,ParserOutput parserOutput) {

    MPI_Status status;
    MPI_Request request;
    
#ifdef RTSCHEDULE
    pid_t myPID = 0; // 0 means calling process/thread
    int realTimePolicy = SCHED_RR;
    sched_param* realTimeHigh = (sched_param*)malloc(sizeof(sched_param));
    realTimeHigh->sched_priority = sched_get_priority_max(realTimePolicy);
    sched_param* realTimeLow = (sched_param*)malloc(sizeof(sched_param));
    realTimeLow->sched_priority = sched_get_priority_min(realTimePolicy);
    printf("Process %d: RT limits min %d max %d\n",myid,realTimeLow->sched_priority,realTimeHigh->sched_priority);
    #pragma omp parallel
    {
        errno = 0;
        sched_setscheduler(myPID,realTimePolicy,realTimeHigh);
        int testRealTimePolicy = sched_getscheduler(myPID);
        printf("Process %d/%d : RT policy provided %d (requested %d, error %d)\n",myid,omp_get_thread_num(),testRealTimePolicy,realTimePolicy,errno);
    }
    cpu_set_t mask;
    sched_getaffinity(myPID, sizeof(mask),&mask);
#endif

    // Receive INIT data first
    int masterRank = 0;
    initdata init;
#ifdef TDEBUG
    printf("Process %d:  Waiting for INIT from MASTER\n",myid);
    fflush(stdout);
#endif
    MPI_Recv(&init, sizeof(init), MPI_BYTE, masterRank,TAG_INIT, MPI_COMM_WORLD, &status);
    // fire unused slave
    if(init.abort)
    {
        printf("Process %i INFO : abort\n",myid);
        fflush(stdout);
        return;
    }
#ifdef TDEBUG
    printf("...Process %d: From MASTER\n",myid);
    fflush(stdout);
#endif

    // Store in local variables:
    int ibeam = init.ibeam;
    int ibunch = init.ibunch;
    int numberOfBunch = init.numberOfBunch;
    double myLongitudinalPosition = init.longitudinalPosition;
    double revolutionTime = parserOutput.circumference*1E9/SPEEDOFLIGHT; //in ns
    int npartTest = 0;
    if(parserOutput.testParticle) {
        npartTest = (int) parserOutput.testParticleAmplitude[2]*parserOutput.testParticleAngle[2];
    }
    int npartReal = parserOutput.npart;
    int npart = npartReal + npartTest;
    if(parserOutput.loadBeamFromFile) {
        char fileName[200];
        sprintf(fileName,parserOutput.beamFileFormat,ibeam,ibunch);
        if (!exists(fileName)) {
            printf("process %i, file %s does not exist, reinitializing\n",myid,fileName);
            parserOutput.loadBeamFromFile = false;
        }
    }

    printf("SLAVE %i: initializing, npart=%i (%luMB), outputDir=%s\n",myid, npart,npart*NCOORD*sizeof(double)/1000000,parserOutput.outputDir);
    fflush(stdout);

    double* beam = (double*)malloc(npart*NCOORD*sizeof(double));
    for(int i = 0;i<npart*NCOORD;++i) {
        beam[i] = 0;
    }

    double bparam[NPARAM];
    double b2param[NPARAM];
    double rms[NCOORD];
    if(ibeam==1) {
        rms[0] = sqrt(parserOutput.mass[0]*parserOutput.emittanceB1[0]*parserOutput.betaStar[0]/parserOutput.energy[0]);
        rms[1] = sqrt(parserOutput.mass[0]*parserOutput.emittanceB1[0]/parserOutput.betaStar[0]/parserOutput.energy[0]);
        rms[2] = sqrt(parserOutput.mass[0]*parserOutput.emittanceB1[1]*parserOutput.betaStar[1]/parserOutput.energy[0]);
        rms[3] = sqrt(parserOutput.mass[0]*parserOutput.emittanceB1[1]/parserOutput.betaStar[1]/parserOutput.energy[0]);
    } else {
        rms[0] = sqrt(parserOutput.mass[1]*parserOutput.emittanceB2[0]*parserOutput.betaStar[0]/parserOutput.energy[1]);
        rms[1] = sqrt(parserOutput.mass[1]*parserOutput.emittanceB2[0]/parserOutput.betaStar[0]/parserOutput.energy[1]);
        rms[2] = sqrt(parserOutput.mass[1]*parserOutput.emittanceB2[1]*parserOutput.betaStar[1]/parserOutput.energy[1]);
        rms[3] = sqrt(parserOutput.mass[1]*parserOutput.emittanceB2[1]/parserOutput.betaStar[1]/parserOutput.energy[1]);
    }
        
    rms[4] = parserOutput.bunchLength;
    rms[5] = parserOutput.relativeMomentumSpread;
    rms[6] = 0; //Relative intensity
    printf("process %i, rms beam sizes : %e,%e,%e,%e,%e,%e\n",myid,rms[0],rms[1],rms[2],rms[3],rms[4],rms[6]);
    double lratio = rms[4]/rms[5]; // Normalisation for longitudinal phase space rotation
    double sLimInf = -3.0*rms[4];
    double sLimSup = 3.0*rms[4];

/////////////////////////////////////////////

    // in case multiple kick per turn, this would have to be moved to a 'zkick instance'
    PassageRecords passageRecords;
    passageRecords.array = (PassageRecord*) malloc(numberOfBunch*parserOutput.wakeLength*sizeof(PassageRecord));
    passageRecords.maxLength = numberOfBunch*parserOutput.wakeLength;
    passageRecords.length = 0;
    passageRecords.position = 0;
    PassageRecords tmpPassageRecords;
    PassageRecord* tmpPassageRecordsArray = (PassageRecord*) malloc(numberOfBunch*sizeof(PassageRecord));

    double particleCharge = 0.0;
    initconst_(&ibeam,&ibunch,&parserOutput.energy[0],&parserOutput.mass[0],&parserOutput.charge[0]);
    if(parserOutput.loadBeamFromFile) {
        char fileName[200];
        sprintf(fileName,parserOutput.beamFileFormat,ibeam,ibunch);
        printf("process %i, parsing file %s\n",myid,fileName);
        fflush(stdout);
        bool ok = parseBeamFile(beam,npart,fileName);
        if(!ok) {
            printf("process %i, error while parsing file %s, request abort\n",myid,fileName);
            init.abort = true;
        }
        if(ibeam == 1) {
            particleCharge = parserOutput.charge[0] >= 0.0 ? parserOutput.bunchIntensity[0]/npartReal : -1.0*parserOutput.bunchIntensity[0]/npartReal;
        } else {
            particleCharge = parserOutput.charge[1] >= 0.0 ? parserOutput.bunchIntensity[1]/npartReal : -1.0*parserOutput.bunchIntensity[1]/npartReal;
        }
    } else {

        if(ibeam == 1) {
            bfillpar_(beam,&npartReal,bparam,rms,&parserOutput.bunchIntensity[0], &parserOutput.xfact, &parserOutput.kick);
            particleCharge = parserOutput.charge[0] >= 0.0 ? parserOutput.bunchIntensity[0]/npartReal : -1.0*parserOutput.bunchIntensity[0]/npartReal;
        } else if(ibeam == 2) {
            //parserOutput.kick = -parserOutput.kick;
            double kick = 0.0;
            particleCharge = parserOutput.charge[1] >= 0.0 ? parserOutput.bunchIntensity[1]/npartReal : -1.0*parserOutput.bunchIntensity[1]/npartReal;
            bfillpar_(beam,&npartReal,bparam,rms,&parserOutput.bunchIntensity[1],&parserOutput.xfact,&kick);//, &parserOutput.kick);
        }
        if(npartTest>0) {
            bfilltestpar_(beam,&npart,&npartReal,rms,parserOutput.testParticleAmplitude,parserOutput.testParticleAngle);
        }
    }

    double particleChargeInC = 1.60217662E-19*particleCharge;
    
    printf("process %i, macro particle charge : %E\n",myid,particleCharge);
    beampar_(beam,&npartReal, bparam,rms,&particleCharge);
    printf("process %i, bunch initialized with parameters : \n",myid);
    printBeamParameter(bparam);
    fflush(stdout);
    
    int synchrotronRadiationTableSize;
    double* synchrotronRadiationTable;
    if(parserOutput.syncRadTableFileName != NULL) {
        if(!parseSynchrotronTable(parserOutput.syncRadTableFileName,&synchrotronRadiationTable,&synchrotronRadiationTableSize)) {
            printf("process %i, error while parsing file %s, request abort\n",myid,parserOutput.syncRadTableFileName);
            init.abort = true;
        }
    }
    
    // check 'dump beam to file' input parameters
    if(parserOutput.dumpBeamToFile) {
        if(parserOutput.dumpBeamToFileFreq>0) {
            if(parserOutput.dumpBeamNPart>=0 && parserOutput.dumpBeamNPart <= npart) {
                if(parserOutput.dumpBeamNPart==0) {
                    parserOutput.dumpBeamNPart = npart;
                }
                char fileName[200];
                sprintf(fileName,parserOutput.dumpBeamFileFormat,ibeam,ibunch,0);
                printf("Process %d will dump %d particles of the beam to a file every %d turns\n",myid,parserOutput.dumpBeamNPart,parserOutput.dumpBeamToFileFreq);
            } else {
                printf("Process %d : %d is not a valid number of particle to dump into a file. Requesting abort\n",myid,parserOutput.dumpBeamNPart);
                init.abort = true;
            }
        } else {
            printf("Process %d : %d is not a valid frequency to dump the beam to a file. Requesting abort\n",myid,parserOutput.dumpBeamToFileFreq);
            init.abort = true;
        }
    }
    
    // Return seed to master
#ifdef TDEBUG
    printf("Process %d:  Returning init ok to MASTER\n",myid);
    fflush(stdout);
#endif
    MPI_Send(&init, sizeof(init), MPI_BYTE, masterRank,TAG_INIT, MPI_COMM_WORLD);
    // wait for confirmation from MASTER
    MPI_Recv(&init, sizeof(init), MPI_BYTE, masterRank,TAG_INIT, MPI_COMM_WORLD, &status);
    // fire unused slave / error during init
    if(init.abort) {
        printf("Process %i INFO : abort\n",myid);
        fflush(stdout);
        return;
    }
#ifdef RSLEEP
    //            unsigned int iseed = secure_rand_base();
    unsigned int iseedsr = 123456789/myid;
#ifdef TDEBUG
    printf("Process %d:  SEEDING rand() with %d\n", myid, iseedsr);
    fflush(stdout);
#endif
    srand(iseedsr);
#endif
    actiondata adata;

//Do one turn where the LR dipole kick are computed (lrdpkick) and set as action.args///
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    char* HOcollSched;
    int nHO = 0;
    HOcollSched = (char*) malloc(10000*sizeof(char));
    sprintf(HOcollSched,"none");
    char* LRcollSched;
    LRcollSched = (char*) malloc(10000*sizeof(char));
    sprintf(LRcollSched,"none");
    int nLR = 0;
    int maxNbSlices = 0;
    int flagUseAction2 = 0;
    int flagUseFPPS = 0;
    while(1) {
        double* args = NULL;
#ifdef TDEBUG
        printf("Process %d:  Waiting for MASTER directive (Turn 0)\n",myid);
        fflush(stdout);
#endif
        MPI_Recv(&adata, sizeof(adata), MPI_BYTE, masterRank,TAG_ACTION, MPI_COMM_WORLD, &status);
#ifdef TDEBUG        
        int count1, ndata;
        MPI_Get_count(&status, MPI_BYTE, &count1);

        ndata = count1/sizeof(adata);
        printf("...Process %d: From MASTER length %d act %d part %d (Turn 0)\n",myid, ndata, adata.actcode, adata.partner);
        fflush(stdout);
#endif
        if(adata.nargs>0) {
#ifdef TDEBUG
            printf("Process %d:  arguments are expected for action code %i, waiting on MASTER (Turn 0)\n",myid,adata.actcode);
            fflush(stdout);
#endif
            args = (double*) malloc(adata.nargs*sizeof(double));
            MPI_Recv(args, adata.nargs*sizeof(double), MPI_BYTE, masterRank,TAG_ARGS, MPI_COMM_WORLD, &status);
#ifdef TDEBUG
            char arguments[1000];
            sprintf(arguments,"%E",args[0]);
            for(int i = 1;i<adata.nargs;++i) {
                sprintf(arguments,"%s,%E",arguments,args[i]);
            }
            printf("Process %d:  arguments received : %s (Turn 0)\n ",myid,arguments);
            fflush(stdout);
#endif
        }

#ifdef RSLEEP
        if (rand() > RAND_MAX/2) {
#ifdef TDEBUG
            printf("...Process %d: SLEEPING for 5 sec. (Turn 0)\n", myid);
            fflush(stdout);
#endif
            sleep(5);
        }
#endif
        if((adata.actcode == 2 || adata.actcode == 4 || adata.actcode == 5) && adata.partner > 0) {
            if(nHO==0){
                sprintf(HOcollSched,"%i",adata.partner);
            }else{
                sprintf(HOcollSched,"%s, %i",HOcollSched,adata.partner);
            }
            ++nHO;
        }

        if(adata.actcode == 2 && adata.partner > 0) {
            flagUseAction2 = 1;
            beampar_(beam,&npartReal,bparam,rms, &particleCharge);

#ifdef TDEBUG
            printf("Process %d:  Sending PARTNER %d proc %d (Turn 0)\n",myid, adata.partner, adata.partnerCPU);
            fflush(stdout);
#endif
            MPI_Send(bparam, NPARAM*sizeof(double), MPI_BYTE, adata.partnerCPU,TAG_SLAVETOSLAVE, MPI_COMM_WORLD);
            MPI_Recv(b2param, NPARAM*sizeof(double), MPI_BYTE, adata.partnerCPU,TAG_SLAVETOSLAVE, MPI_COMM_WORLD, &status);
#ifdef TDEBUG
            MPI_Get_count(&status, MPI_BYTE, &count1);
            ndata = count1;
            printf("...Process %d: from PARTNER %d proc %d len %d (Turn 0)\n", myid, adata.partner, adata.partnerCPU, ndata);
            fflush(stdout);
#endif
            if(ibeam ==1){
                lrdpkick2d_(b2param,&args[1],&args[2],rms,&args[4],&args[5]);
            } else {
                lrdpkick2d_(b2param,&args[1],&args[2],rms,&args[6],&args[7]);
            }
        } else if (adata.actcode == 4 && adata.partner > 0){
            flagUseFPPS = 1;
        }else if (adata.actcode == 10) {
            if(adata.partner > 0) {
                if(nLR==0){
                    sprintf(LRcollSched,"%i",adata.partner);
                }else{
                    sprintf(LRcollSched,"%s, %i",LRcollSched,adata.partner);
                }
                ++nLR;
                beampar_(beam,&npartReal,bparam,rms, &particleCharge);
#ifdef TDEBUG
                printf("Process %d:  Sending PARTNER %d proc %d (Turn 0)\n",myid, adata.partner, adata.partnerCPU);
                fflush(stdout);
#endif
                MPI_Send(bparam, NPARAM*sizeof(double), MPI_BYTE, adata.partnerCPU,TAG_SLAVETOSLAVE, MPI_COMM_WORLD);
                MPI_Recv(b2param, NPARAM*sizeof(double), MPI_BYTE, adata.partnerCPU,TAG_SLAVETOSLAVE, MPI_COMM_WORLD, &status);
#ifdef TDEBUG
                MPI_Get_count(&status, MPI_BYTE, &count1);
                ndata = count1;
                printf("...Process %d: from PARTNER %d proc %d len %d (Turn 0)\n",myid, adata.partner, adata.partnerCPU, ndata);
                fflush(stdout);
#endif
                int plan = (int) args[0];
                if(plan==1) {
                    lrdpkick_(b2param,&plan,&args[1],&rms[0],&args[3]);            
                }else {
                    lrdpkick_(b2param,&plan,&args[1],&rms[3],&args[4]);
                }            
            }
        }else if (adata.actcode == 12) {
            if(args[0] > maxNbSlices) {
                maxNbSlices = args[0];
            }
        }else if (adata.actcode == 5) {
            if(args[2] > maxNbSlices) {
                maxNbSlices = args[2];
            }
        }else if (adata.actcode == 14 && args[0] == 2) {
            if(args[5] > maxNbSlices) {
                maxNbSlices = args[5];
            }
        }else if (adata.actcode == ABORTACTION) {      // EXIT message from MASTER
#ifdef TDEBUG
            printf("Process %d: end loop (Turn 0)\n",myid);
            fflush(stdout);
#endif
            break;
        }

#ifdef TDEBUG
        printf("Process %d:  Sending COMPLETION to MASTER (Turn 0)\n",myid);
        fflush(stdout);
#endif
        MPI_Send(&adata, sizeof(adata), MPI_BYTE, masterRank,TAG_COMPLETION, MPI_COMM_WORLD);
        if(adata.nargs>0) {
            MPI_Send(args, adata.nargs*sizeof(double),MPI_BYTE,masterRank,TAG_ARGS,MPI_COMM_WORLD);
        }

        free(args);
    }

    if(parserOutput.dumpBunchCollisionSchedule) {
        char* collSchedFileName = (char*) malloc(200*sizeof(char));
        sprintf(collSchedFileName,parserOutput.bunchCollisionScheduleFileFormat,ibeam,ibunch);
        char* collSched = (char*) malloc(20000*sizeof(char));
        sprintf(collSched,"B%ib%.3i; %i HO: %s; %i LR: %s;",ibeam,ibunch,nHO,HOcollSched,nLR,LRcollSched);
        writeString(parserOutput.outputDir,collSchedFileName,collSched);
        free(collSchedFileName);
        free(collSched);
    }
    free(HOcollSched);
    free(LRcollSched);
//end computation of LR dipole kick

    printf("Process %d: Max number of slices : %d\n",myid,maxNbSlices);
#ifdef TDEBUG
        printf("Process %d: computation of LR kick done, allocating memory\n",myid);
        fflush(stdout);
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Load wakes and allocate memory for impedance computation
    wake* wakes = (wake*) malloc(parserOutput.nWakeFiles*sizeof(wake));
    for(int i = 0;i<parserOutput.nWakeFiles;++i) {
        //parseWake(parserOutput.wakeFiles[i],&wakes[i]);
        parseWake(parserOutput.wakeFiles[i],&wakes[i],parserOutput.zBeta[0]/parserOutput.betaStar[0],parserOutput.zBeta[1]/parserOutput.betaStar[1]);
    }
    //double* zWorkspace1 = (double*) malloc(npart*sizeof(double));
    double*** pizzaSliceMoment1 = (double***) malloc(maxNbSlices*sizeof(double**));
    for(int i=0;i<maxNbSlices;++i){
        pizzaSliceMoment1[i] = (double**) malloc(maxNbSlices*sizeof(double*));
        for(int j=0;j<maxNbSlices;++j){
            pizzaSliceMoment1[i][j] = (double*) malloc(8*sizeof(double));
        }
    }
    double* sliceMoment1 = (double*) malloc(maxNbSlices*5*sizeof(double));
    double* b2sliceMoment1 = (double*) malloc(maxNbSlices*5*sizeof(double));
    double* sliceMoment2 = (double*) malloc(maxNbSlices*11*sizeof(double));
    double* b2sliceMoment2 = (double*) malloc(maxNbSlices*11*sizeof(double));
    double* zWorkspace3 = (double*) malloc(maxNbSlices*4*sizeof(double));
    //int* zWorkspace2 = (int*) malloc(npart*maxNbSlices*sizeof(int));
    //int* zWorkspace3 = (int*) malloc(maxNbSlices*sizeof(int));
    PassageRecord* tmpPassageRecord = (PassageRecord*) malloc(sizeof(PassageRecord));
    double zKickScale = 1.0/(parserOutput.energy[0]*1E9);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//This is temporary, as no beta function is implemented yet/////////////////////////////////////////////////////////
    double betaStarH[] = {parserOutput.betaStar[0],parserOutput.betaStar[0]};
    double betaStarV[] = {parserOutput.betaStar[1],parserOutput.betaStar[1]};
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    double* beamDistrWorkspace = (double*) malloc(1E6 * sizeof(double));
//initialisation of the HFMM ///////////////////////////////////////////////////////////////////////////////////
#ifndef GAUSS
    // Arrays for FMM charge coordinates
    int stdSize = 2*(npart+(2*NXG+1)*(2*NYG+1));
    float* XCHARGE = (float*) malloc(flagUseAction2*stdSize*sizeof(float));
    float* YCHARGE = (float*) malloc(flagUseAction2*stdSize*sizeof(float));
    // Array for FMM charge values (COMPLEX type in Fortran)
    float* QA = (float*) malloc(flagUseAction2*2*stdSize*sizeof(float));
    // Array for packed charges & count, for sending/receiving
    float* CHARGE = (float*) malloc(flagUseAction2*3*stdSize*sizeof(float));
    float* CHARGE2 = (float*) malloc(flagUseAction2*3*stdSize*sizeof(float));
    // Index array for HFMM grid
    int* INDX = (int*) malloc(flagUseAction2*npart*sizeof(float));
    // Arrays for calculated fields at particle locations
    float* EX = (float*) malloc(flagUseAction2*npart*sizeof(float));
    float* EY = (float*) malloc(flagUseAction2*npart*sizeof(float));
    // Workspace for the hfmm
    int wkspSize = 32*(npart+NGRID)+1000;
    float* WKSP = (float*) malloc(flagUseAction2*wkspSize*sizeof(float));
    float* hfmmField = (float*) malloc(flagUseAction2*2*stdSize*sizeof(float));
    float* hfmmPoten = (float*) malloc(flagUseAction2*stdSize*sizeof(float));
    int hfmmInfoSize = 4;
    int* hfmmInfo = (int*) malloc(flagUseAction2*hfmmInfoSize*sizeof(int));
    int hfmmErrorSize = 3;
    int* hfmmError = (int*) malloc(flagUseAction2*hfmmErrorSize*sizeof(int));
    //printf("process %i, field solver workspace size : %d\n",myid,wkspSize);

    // Initialize FMM parameters
    int fmmout = 1;
    int fmmbox = 50;
    float fmmtol = 0.05;
    float fmmcut = 1.e-12;
    fmmsetup_(&fmmout, &fmmbox, &fmmtol, &fmmcut);

    // FMM options
    int NAT = 0;
    int NXGR = NXG; int NYGR = NYG;
    float DXGR = 0.1*rms[0];
    float DYGR = 0.1*rms[2];
    int FMMTEST = 0;
    int FMMGRID = 1;
    int CLOUD = 1;
    float CCONST = 1.;
#endif
#ifdef TDEBUG
        printf("Process %d: memory allocation done\n",myid);
        fflush(stdout);
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//initialisation of the FPPS solver for head-on simulations///////////////////////////////////////////////////////

    double FPPSKickCoef = parserOutput.charge[0]*1E-9/parserOutput.energy[0];

    int FPPSnTheta = 0;
    int FPPSnR = 0;
    if(flagUseFPPS) {
        FPPSnTheta = parserOutput.nTheta;
        FPPSnR = parserOutput.mR;
        printf("Using FPPS with nTheta %d nR %d\n",FPPSnTheta,FPPSnR);
    } 
    
#ifdef TDEBUG
        printf("Process %d: instanciating FPPS %d,%d\n",myid,FPPSnTheta,FPPSnR);
        fflush(stdout);
#endif
/*
    PolarBeamRepresentation polarBeamRepresentation(beam,npart);
    ChangeCoord_Frac g(0.5*(rms[0]+rms[2])/2.); //change of coordinates. Parameter could be reset after computing sigma
    NonLinearMesh chargeDistributionMesh(FPPSnTheta,FPPSnR,1.0,&g);
    NonLinearMesh partnerChargeDistributionMesh(FPPSnTheta,FPPSnR,1.0,&g);
    NonLinearMesh radialField(FPPSnTheta,FPPSnR,1.0,&g);
    NonLinearMesh polarField(FPPSnTheta,FPPSnR,1.0,&g);

    //double rmax = 6.0*rms[0];
    //Mesh chargeDistributionMesh(FPPSnTheta,FPPSnR,rmax);
    //Mesh partnerChargeDistributionMesh(FPPSnTheta,FPPSnR,rmax);
    //Mesh radialField(FPPSnTheta,FPPSnR,rmax);
    //FMesh polarField(FPPSnTheta,FPPSnR,rmax);

    ChargeDistribution myChargeDistribution(&chargeDistributionMesh);
    ChargeDistribution partnerChargeDistribution(&partnerChargeDistributionMesh);
    FastPolarPoissonSolver fastPolarPoissonSolver(&partnerChargeDistribution);
    ElectricFieldSolver electricFieldSolver(&fastPolarPoissonSolver,&radialField,&polarField);
*/

/*    
    FastPolarPoissonSolver_FD2_Scaled solver(FPPSnTheta,FPPSnR);
    solver.setMesh(msc);
    solver.setChangeCoord(g);

    Charge_Distribution_Fixed_Scaled chd(FPPSnTheta,FPPSnR);
    chd.setMesh(msc);
    chd.setChangeCoord(g);
    chd.init();
    
    Electric_Field_Solver_Scaled elecfield; //Empty electricfield, filled by the potential after computation
    elecfield.setEr(Function_on_a_mesh(FPPSnTheta,FPPSnR,1.0));
    elecfield.setEtheta(Function_on_a_mesh(FPPSnTheta,FPPSnR,1.0));
    elecfield.setChangeCoord(g);
    elecfield.setMesh(msc);
*/
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef NICE
    int niceHigh = -19;
    int niceLow = 19;
    pid_t myPID = 0; // 0 means calling process/thread
#endif
//    char fileName[200];
//    sprintf(fileName,parserOutput.dumpBeamFileFormat,ibeam,ibunch,0);
//    writeBeam(parserOutput.outputDir,fileName,beam,npart);
    //initialization done, starting tracking
#ifdef TDEBUG
        printf("Process %d: initialisation done, start tracking\n",myid);
        fflush(stdout);
#endif
    while (1) {
        double* args = NULL;
#ifdef TDEBUG
        printf("Process %d:  Waiting for MASTER directive (track)\n",myid);
        fflush(stdout);
#endif
        MPI_Recv(&adata, sizeof(adata), MPI_BYTE, masterRank,TAG_ACTION, MPI_COMM_WORLD, &status);
#ifdef NICE
        setpriority(PRIO_PROCESS,myPID,niceHigh);
#endif
#ifdef RTSCHEDULE	    
	    sched_setparam(myPID,realTimeHigh);
#endif

#ifdef TDEBUG        
        int count1, ndata;
        MPI_Get_count(&status, MPI_BYTE, &count1);

        printf("...Process %d: From MASTER act %d part %d\n",myid, adata.actcode, adata.partner);
        fflush(stdout);
#endif
        if(adata.nargs>0) {
#ifdef TDEBUG
            printf("Process %d:  arguments are expected for action code %i, waiting on MASTER\n",myid,adata.actcode);
            fflush(stdout);
#endif
            args = (double*) malloc(adata.nargs*sizeof(double));             
            MPI_Recv(args, adata.nargs*sizeof(double), MPI_BYTE, masterRank,TAG_ARGS, MPI_COMM_WORLD, &status);
#ifdef TDEBUG
            char arguments[1000];
            sprintf(arguments,"%f",args[0]);
            for(int i = 1;i<adata.nargs;++i) {
                sprintf(arguments,"%s,%f",arguments,args[i]);
            }
            printf("Process %d:  arguments received : %s\n",myid,arguments);
            fflush(stdout);
#endif
        }

        //add previous turn passage records (including other bunches's)
        if(adata.actcode == 12) {
            MPI_Recv(&tmpPassageRecords, sizeof(PassageRecords), MPI_BYTE, masterRank,TAG_PASSAGERECORD, MPI_COMM_WORLD, &status);
            MPI_Recv(tmpPassageRecordsArray, tmpPassageRecords.maxLength*sizeof(PassageRecord), MPI_BYTE, masterRank,TAG_PASSAGEARRAY, MPI_COMM_WORLD, &status);
            tmpPassageRecords.array = tmpPassageRecordsArray;
            
            add(&passageRecords,&tmpPassageRecords);
        }

#ifdef RSLEEP
        if (rand() > RAND_MAX/2) {
#ifdef TDEBUG
            printf("...Process %d: SLEEPING for 5 sec.\n", myid);
            fflush(stdout);
#endif
            sleep(5);
        }
#endif
        // performing action
        if (adata.actcode == 1) {
            //longitudinal motion DEPRECATED use action 3
            if(ibeam==1){
                rf_(beam,&npart, &args[0],&lratio);
            } else {
                rf_(beam,&npart, &args[1],&lratio);
            }
        } else if (adata.actcode == 2) {
            // Head-on collision (or just pi/2 phase advance to collision point if no opposite bunch)
#ifdef TIMING
                ////////////////////////////////////////////////////////
                timeval a2Start;
                gettimeofday(&a2Start,NULL);
                ////////////////////////////////////////////////////////
#endif
            //transpi2_(beam,&npart,betaStarH,betaStarV);
            if (adata.partner > 0 && args[0] != 0.0) {
                //double hOffset = args[1]*rms[0]/2;
                //double vOffset = args[2]*rms[2]/2;
                //if(ibeam == 2) {
                //    hOffset = -hOffset;
                //    vOffset = -vOffset;
                //} 
                //offset_(beam,&npart,&hOffset,&vOffset);
#ifdef GAUSS
                beampar_(beam,&npartReal, bparam,rms, &particleCharge);
#ifdef TDEBUG
                printf("Process %d:  Sending PARTNER %d proc %d len %d\n",myid, adata.partner, adata.partnerCPU, NPARAM*sizeof(double));
#endif
                MPI_Send(bparam, NPARAM*sizeof(double), MPI_BYTE, adata.partnerCPU,TAG_SLAVETOSLAVE, MPI_COMM_WORLD);
                MPI_Recv(b2param, NPARAM*sizeof(double), MPI_BYTE, adata.partnerCPU,TAG_SLAVETOSLAVE, MPI_COMM_WORLD, &status);
                pkickgauss_(beam,&npart,b2param);
#endif
#ifndef GAUSS
                double scaledParticleCharge = args[0]*particleCharge;
                // prepare HFMM
#ifdef TIMING
            ////////////////////////////////////////////////////////
            timeval hfmmprepStart;
            gettimeofday(&hfmmprepStart,NULL);
            ////////////////////////////////////////////////////////
#endif
                hfmmprep_(CHG,&NXGR,&NYGR,&DXGR,&DYGR,&FMMTEST,
                        beam,&npart,&FMMGRID,&CLOUD,
                        INDX,XCHARGE,YCHARGE,QA,&NAT,
                        CHARGE,&scaledParticleCharge);
#ifdef TIMING
                /////////////////////////////////////////////////////////////////////
                timeval hfmmprepEnd;
                gettimeofday(&hfmmprepEnd,NULL);
                double hfmmprepDuration = hfmmprepEnd.tv_sec-hfmmprepStart.tv_sec+(hfmmprepEnd.tv_usec-hfmmprepStart.tv_usec)/1E6;
                printf("Process %d : time for hfmmprep : %f s \n",myid,hfmmprepDuration);
                /////////////////////////////////////////////////////////////////////
#endif

#ifdef TDEBUG
                printf("Process %d:  Sending PARTNER %d proc %d \n",myid, adata.partner, adata.partnerCPU);
                fflush(stdout);
#endif
                // send / recieve number of grid points/particle
                MPI_Isend(&NAT,sizeof(int),MPI_BYTE,adata.partnerCPU,TAG_SLAVETOSLAVE,MPI_COMM_WORLD,&request);
                int NAT2 = 0;
                MPI_Recv(&NAT2,sizeof(int),MPI_BYTE,adata.partnerCPU,TAG_SLAVETOSLAVE,MPI_COMM_WORLD,&status);

                // Send / recieve particles
                MPI_Isend(CHARGE, 3*NAT*sizeof(float), MPI_BYTE, adata.partnerCPU,TAG_SLAVETOSLAVE, MPI_COMM_WORLD,&request);
                MPI_Recv(CHARGE2, 3*NAT2*sizeof(float), MPI_BYTE, adata.partnerCPU,TAG_SLAVETOSLAVE, MPI_COMM_WORLD, &status);
#ifdef TDEBUG
                MPI_Get_count(&status, MPI_BYTE, &count1);
                ndata = count1;
                printf("...Process %d: from PARTNER %d proc %d len %d\n",myid, adata.partner, adata.partnerCPU, ndata);
                fflush(stdout);
#endif
#ifdef TIMING
            ////////////////////////////////////////////////////////
            timeval hfmmcalcStart;
            gettimeofday(&hfmmcalcStart,NULL);
            ////////////////////////////////////////////////////////
#endif
                hfmmcalc_(CHG,EXG,EYG,POTG,&NXGR,&NYGR,
                        &DXGR,&DYGR,&FMMTEST,
                        beam,&npart,INDX,
                        XCHARGE,YCHARGE,QA,CHARGE2,
                        &NAT,&NAT2,
                        EX,EY,&FMMGRID,&CLOUD,&CCONST,
                        &myid,
                        &wkspSize,WKSP,hfmmPoten,hfmmField,
                        hfmmInfo,&hfmmInfoSize,hfmmError,&hfmmErrorSize);
#ifdef TIMING
                /////////////////////////////////////////////////////////////////////
                timeval hfmmcalcEnd;
                gettimeofday(&hfmmcalcEnd,NULL);
                double hfmmcalcDuration = hfmmcalcEnd.tv_sec-hfmmcalcStart.tv_sec+(hfmmcalcEnd.tv_usec-hfmmcalcStart.tv_usec)/1E6;
                printf("Process %d : time for hfmmcalc : %f s \n",myid,hfmmcalcDuration);
                /////////////////////////////////////////////////////////////////////
#endif
                if(hfmmError[0] != 0) {
                    printf("Process %d : error during hfmmcalc (%d,%d,%d), requesting abort \n",myid,hfmmError[0],hfmmError[1],hfmmError[2]);
                    adata.actcode = -1;
                }
#ifdef TDEBUG
                char testString[200];
                sprintf(testString,"process %i, HFMM info : ",myid);
                for(int i = 0;i<hfmmInfoSize;++i) {
                    sprintf(testString,"%s,%d",testString,hfmmInfo[i]);
                }
                printf("%s\n",testString);
                fflush(stdout);
#endif
#ifdef TIMING
            ////////////////////////////////////////////////////////
            timeval pkickStart;
            gettimeofday(&pkickStart,NULL);
            ////////////////////////////////////////////////////////
#endif
                if(ibeam == 1) {
                    //printf("process %i, HO dp kicks B1 : %E, %E\n",myid,args[4],args[5]);
                    pkick_(beam,&npart,&particleCharge, EX, EY,&args[4],&args[5]);
                } else {
                    //printf("process %i, HO dp kicks B2 : %E, %E\n",myid,args[6],args[7]);
                    pkick_(beam,&npart,&particleCharge, EX, EY,&args[6],&args[7]);
                }
                //hOffset = -hOffset;
                //vOffset = -vOffset;
                //offset_(beam,&npart,&hOffset,&vOffset);
#ifdef TIMING
                /////////////////////////////////////////////////////////////////////
                timeval pkickEnd;
                gettimeofday(&pkickEnd,NULL);
                double pkickDuration = pkickEnd.tv_sec-pkickStart.tv_sec+(pkickEnd.tv_usec-pkickStart.tv_usec)/1E6;
                printf("Process %d : time for pkick : %f s \n",myid,pkickDuration);
                /////////////////////////////////////////////////////////////////////
#endif
#endif
            }
            //transpi2_(beam,&npart,betaStarH,betaStarV);
#ifdef TIMING
            /////////////////////////////////////////////////////////////////////
            timeval a2End;
            gettimeofday(&a2End,NULL);
            double duration = a2End.tv_sec-a2Start.tv_sec+(a2End.tv_usec-a2Start.tv_usec)/1E6;
            printf("Process %d : time for head on : %f s\n",myid,duration);
            //////////////////////////////////////////////////////////////////////
#endif
        } else if(adata.actcode == 4){
            if(args[0] != 0) {
                transpi2_(beam,&npart,betaStarH,betaStarV);
            }
            if (adata.partner > 0) {

#ifdef TIMING
                ////////////////////////////////////////////////////////
                timeval projectStart;
                gettimeofday(&projectStart,NULL);
                ////////////////////////////////////////////////////////
#endif
                //polarBeamRepresentation.update();

#ifdef TIMING
                ////////////////////////////////////////////////////////
                timeval fillStart;
                gettimeofday(&fillStart,NULL);
                double duration = fillStart.tv_sec-projectStart.tv_sec+(fillStart.tv_usec-projectStart.tv_usec)/1E6;
                printf("Process %d : time for polar projection : %f s \n",myid,duration);
                ////////////////////////////////////////////////////////
#endif
               //if(ibeam == 2){
               //     double hOffset = 0.5*rms[0];
               //   double vOffset = 0.0;
               //     offset_(beam,&npart,&hOffset,&vOffset);
               // }
               // myChargeDistribution.fill(&polarBeamRepresentation,particleChargeInC);
               // if(ibeam == 2){
               //     double hOffset = -0.5*rms[0];
               //   double vOffset = 0.0;
               //     offset_(beam,&npart,&hOffset,&vOffset);
               // }
#ifdef TIMING
                ////////////////////////////////////////////////////////
                timeval MPIStart;
                gettimeofday(&MPIStart,NULL);
                duration = MPIStart.tv_sec-fillStart.tv_sec+(MPIStart.tv_usec-fillStart.tv_usec)/1E6;
                printf("Process %d : time for FPPS fill : %f s \n",myid,duration);
                ////////////////////////////////////////////////////////
#endif
                //myChargeDistribution.getMesh()->sendMPI(adata.partnerCPU,TAG_SLAVETOSLAVE, &request);
                //partnerChargeDistribution.getMesh()->recieveMPI(adata.partnerCPU,TAG_SLAVETOSLAVE, &status);
#ifdef TIMING
                ////////////////////////////////////////////////////////
                timeval solveStart;
                gettimeofday(&solveStart,NULL);
                duration = solveStart.tv_sec-MPIStart.tv_sec+(solveStart.tv_usec-MPIStart.tv_usec)/1E6;
                printf("Process %d : time for FPPS MPI exchange : %f s \n",myid,duration);
                ////////////////////////////////////////////////////////
#endif
                //electricFieldSolver.solve(myid);
#ifdef TIMING
                ////////////////////////////////////////////////////////
                timeval applyStart;
                gettimeofday(&applyStart,NULL);
                duration = applyStart.tv_sec-solveStart.tv_sec+(applyStart.tv_usec-solveStart.tv_usec)/1E6;
                printf("Process %d : time for FPPS solve : %f s \n",myid,duration);
                ////////////////////////////////////////////////////////
#endif
                //electricFieldSolver.applykicks(&polarBeamRepresentation,FPPSKickCoef);
#ifdef TIMING
                ////////////////////////////////////////////////////////
                timeval applyEnd;
                gettimeofday(&applyEnd,NULL);
                duration = applyEnd.tv_sec-applyStart.tv_sec+(applyEnd.tv_usec-applyStart.tv_usec)/1E6;
                printf("Process %d : time for FPPS apply : %f s \n",myid,duration);
                duration = applyEnd.tv_sec-projectStart.tv_sec+(applyEnd.tv_usec-projectStart.tv_usec)/1E6;
                printf("Process %d : time for FPPS total : %f s \n",myid,duration);
                ////////////////////////////////////////////////////////
#endif
            }
            if(args[0] != 0) {
                transpi2_(beam,&npart,betaStarH,betaStarV);
            }
        } else if (adata.actcode == 5) {
	        if(adata.partner > 0) {

                beampar_(beam,&npartReal,bparam,rms,&particleCharge);
                MPI_Send(bparam, NPARAM*sizeof(double), MPI_BYTE, adata.partnerCPU,TAG_SLAVETOSLAVE, MPI_COMM_WORLD);
                MPI_Recv(b2param, NPARAM*sizeof(double), MPI_BYTE, adata.partnerCPU,TAG_SLAVETOSLAVE, MPI_COMM_WORLD, &status);

                int nbslices = (int) args[2];

                computeSlicesFirstOrderMoment(beam,npart,particleCharge,nbslices,sliceMoment1,sLimInf,sLimSup);
                MPI_Send(sliceMoment1, nbslices*5*sizeof(double), MPI_BYTE, adata.partnerCPU,TAG_SLAVETOSLAVE3, MPI_COMM_WORLD);
                MPI_Recv(b2sliceMoment1, nbslices*5*sizeof(double), MPI_BYTE, adata.partnerCPU,TAG_SLAVETOSLAVE3, MPI_COMM_WORLD, &status);

                computeSlicesSecondOrderMoment(beam,npart,particleCharge,nbslices,sliceMoment2,sLimInf,sLimSup);
                MPI_Send(sliceMoment2, nbslices*11*sizeof(double), MPI_BYTE, adata.partnerCPU,TAG_SLAVETOSLAVE2, MPI_COMM_WORLD);
                MPI_Recv(b2sliceMoment2, nbslices*11*sizeof(double), MPI_BYTE, adata.partnerCPU,TAG_SLAVETOSLAVE2, MPI_COMM_WORLD, &status);

                sbc_(beam,&npart,b2param,&args[0],&nbslices,&args[1],b2sliceMoment1,b2sliceMoment2);
            }
        }else if (abs(adata.actcode) == 10) {
            // Long-range collision
            int plan = (int) args[0];
            if (adata.partner > 0) {
#ifdef TIMING
                ////////////////////////////////////////////////////////
                timeval a10Start;
                gettimeofday(&a10Start,NULL);
                ////////////////////////////////////////////////////////
                ////////////////////////////////////////////////////////
                timeval bparamStart;
                gettimeofday(&bparamStart,NULL);
                ////////////////////////////////////////////////////
#endif
                beampar_(beam,&npartReal, bparam,rms, &particleCharge);
#ifdef TIMING
                /////////////////////////////////////////////////////////////////////
                timeval bparamEnd;
                gettimeofday(&bparamEnd,NULL);
                double bparamDuration = bparamEnd.tv_sec-bparamStart.tv_sec+(bparamEnd.tv_usec-bparamStart.tv_usec)/1E6;
                /////////////////////////////////////////////////////////////////////
#endif
#ifdef TDEBUG
                printf("Process %d:  Sending PARTNER %d proc %d\n",myid, adata.partner, adata.partnerCPU);
                fflush(stdout);
#endif
                MPI_Send(bparam, NPARAM*sizeof(double), MPI_BYTE, adata.partnerCPU,TAG_SLAVETOSLAVE, MPI_COMM_WORLD);
                MPI_Recv(b2param, NPARAM*sizeof(double), MPI_BYTE, adata.partnerCPU,TAG_SLAVETOSLAVE, MPI_COMM_WORLD, &status);
#ifdef TDEBUG
                MPI_Get_count(&status, MPI_BYTE, &count1);
                ndata = count1;
                printf("...Process %d: from PARTNER %d proc %d len %d\n",myid, adata.partner, adata.partnerCPU, ndata);
                fflush(stdout);
#endif
//                int usekick = 1; //TODO set to 1 to use precomputed kicks
//		double kick = 0.0;
//                if(ibeam==1 && plan == 1){
//                    pkicklr_(beam,&npart,bparam, b2param,&plan,&args[1],&rms[0],&usekick,&kick);
//                }else if(ibeam==1 && plan == 2){
//                    pkicklr_(beam,&npart,bparam, b2param,&plan,&args[1],&rms[2],&usekick,&kick);
//                }else if(ibeam==2 && plan == 1){
//                    pkicklr_(beam,&npart,bparam, b2param,&plan,&args[1],&rms[0],&usekick,&kick);
//                }else if(ibeam==2 && plan == 2){
//                    pkicklr_(beam,&npart,bparam, b2param,&plan,&args[1],&rms[2],&usekick,&kick);
//                }

                int usekick = 0; //TODO set to 1 to use precomputed kicks
                if(ibeam==1 && plan == 1){
                    pkicklr_(beam,&npart,bparam, b2param,&plan,&args[1],&rms[0],&usekick,&args[3]);
                }else if(ibeam==1 && plan == 2){
                    pkicklr_(beam,&npart,bparam, b2param,&plan,&args[1],&rms[2],&usekick,&args[3]);
                }else if(ibeam==2 && plan == 1){
                    pkicklr_(beam,&npart,bparam, b2param,&plan,&args[1],&rms[0],&usekick,&args[4]);
                }else if(ibeam==2 && plan == 2){
                    pkicklr_(beam,&npart,bparam, b2param,&plan,&args[1],&rms[2],&usekick,&args[4]);
                }
#ifdef TIMING
                /////////////////////////////////////////////////////////////////////
                timeval a10End;
                gettimeofday(&a10End,NULL);
                double duration = a10End.tv_sec-a10Start.tv_sec+(a10End.tv_usec-a10Start.tv_usec)/1E6;
                printf("Process %d : time for long range : %f s (%f s for bparam)\n",myid,duration,bparamDuration);
                //////////////////////////////////////////////////////////////////////
#endif
            }
        } else if (abs(adata.actcode) == 100) {
            // incoherent lumped long-range collision (-> interaction does not depend on the other beam's property but only on the action arguments)
            int plan = (int) args[0];
#ifdef TIMING
            ////////////////////////////////////////////////////////
            timeval a10Start;
            gettimeofday(&a10Start,NULL);
            ////////////////////////////////////////////////////////
#endif
            inckicklr_(beam,&npart,&args[2],&plan,&args[1],rms);
#ifdef TIMING
            /////////////////////////////////////////////////////////////////////
            timeval a10End;
            gettimeofday(&a10End,NULL);
            double duration = a10End.tv_sec-a10Start.tv_sec+(a10End.tv_usec-a10Start.tv_usec)/1E6;
            printf("Process %d : time for incoherent long range : %f s \n",myid,duration);
            //////////////////////////////////////////////////////////////////////
#endif
        } else if (adata.actcode == 3) {
            // Linear transport
#ifdef TIMING
            ////////////////////////////////////////////////////////
            timeval a3Start;
            gettimeofday(&a3Start,NULL);
            ////////////////////////////////////////////////////////
#endif
#ifdef TDEBUG
            if(ibeam == 1) {
                printf("...Process %d: PHASE ADVANCE dqx %f dqy %f\n",myid, args[0], args[1]);
            } else {
                printf("...Process %d: PHASE ADVANCE dqx %f dqy %f\n",myid, args[2], args[3]);
            }
            fflush(stdout);
#endif
            //double coupl = 0.01; //use to introduce linear transverse coupling
            if(ibeam == 1) {
                transfer_(beam,&npart,&args[0], &args[1], &args[2],betaStarH,betaStarV,&lratio,&args[6],&args[7]);
                //transfercoupl_(beam,&npart,&args[0], &args[1],betaStarH,betaStarV,&coupl);
            } else {
                transfer_(beam,&npart,&args[3], &args[4], &args[5],betaStarH,betaStarV,&lratio,&args[8],&args[9]);
                //transfercoupl_(beam,&npart,&args[2], &args[3],betaStarH,betaStarV,&coupl);
            }
#ifdef TDEBUG
            printf("...Process %d: PHASE ADVANCE done\n",myid);
            fflush(stdout);
#endif
#ifdef TIMING
            /////////////////////////////////////////////////////////////////////
            timeval a3End;
            gettimeofday(&a3End,NULL);
            double duration = a3End.tv_sec-a3Start.tv_sec+(a3End.tv_usec-a3Start.tv_usec)/1E6;
            printf("Process %d : time for transport : %f s\n",myid,duration);
            //////////////////////////////////////////////////////////////////////
#endif
        }  else if (adata.actcode == 31) {
            // tune inversion emittance exchange
#ifdef TIMING
            ////////////////////////////////////////////////////////
            timeval a3Start;
            gettimeofday(&a3Start,NULL);
            ////////////////////////////////////////////////////////
#endif
#ifdef TDEBUG
            if(ibeam == 1) {
                printf("...Process %d: PHASE ADVANCE dqx %f dqy %f\n",myid, args[0], args[1]);
            } else {
                printf("...Process %d: PHASE ADVANCE dqx %f dqy %f\n",myid, args[2], args[3]);
            }
            fflush(stdout);
#endif

	    double scanStat = 1.0;
            double rampTime = args[10];
            double exchangeTime = args[11];
            if (adata.turn < rampTime){
	        scanStat = 0.0;
            }else if (adata.turn < rampTime+exchangeTime){
                scanStat = ((double)adata.turn-rampTime)/exchangeTime;
            }
            if(scanStat>1){scanStat = 1.0;}

            //double coupl = 0.01; //use to introduce linear transverse coupling
            if(ibeam == 1) {
            	double qx = args[0] + (args[1]-args[0])*scanStat;
            	double qy = args[1] + (args[0]-args[1])*scanStat;

                transfer_(beam,&npart,&qx, &qy, &args[2],betaStarH,betaStarV,&lratio,&args[6],&args[7]);
            } else {
            	double qx = args[3] + (args[4]-args[3])*scanStat;
            	double qy = args[4] + (args[3]-args[4])*scanStat;

                transfer_(beam,&npart,&qx, &qy, &args[5],betaStarH,betaStarV,&lratio,&args[8],&args[9]);
            }
            // skew quad ramp up and and down
            double k=args[12];
            if (adata.turn < rampTime){
                k*=(double)adata.turn/rampTime;
            } else if(adata.turn > rampTime+exchangeTime){
                k*=(1.0-((double)adata.turn-(rampTime+exchangeTime))/rampTime);
            }
            if(adata.turn > 2*rampTime+exchangeTime) {
                k = 0;
	    }
	    double angle = 0.785375;
            quad_(beam,&npart,&k,&angle);

#ifdef TDEBUG
            printf("...Process %d: PHASE ADVANCE done\n",myid);
            fflush(stdout);
#endif
#ifdef TIMING
            /////////////////////////////////////////////////////////////////////
            timeval a3End;
            gettimeofday(&a3End,NULL);
            double duration = a3End.tv_sec-a3Start.tv_sec+(a3End.tv_usec-a3Start.tv_usec)/1E6;
            printf("Process %d : time for transport : %f s\n",myid,duration);
            //////////////////////////////////////////////////////////////////////
#endif
        } else if (adata.actcode == 11) {
            // collimator
            double horz[2];
            horz[0] = args[0]*rms[0];
            horz[1] = args[1]*rms[0];
            double vert[2];
            vert[0] = args[2]*rms[2];
            vert[1] = args[3]*rms[2];
            // Warning, ugly Quick fix  : TODO move to input + writer class ?////////
            char fileName[200];
            sprintf(fileName,"B%ib%i.loss",ibeam,ibunch);
	        char completeFileName[strlen(parserOutput.outputDir)+1+strlen(fileName)];
	        strcpy(completeFileName,parserOutput.outputDir);
	        strcat(completeFileName,"/");
	        strcat(completeFileName,fileName);
            //////////////////////////////////////////
            collimator(beam,npartReal,horz,vert,completeFileName,adata.turn);
        }  else if (adata.actcode == 6) {
            if (adata.nargs == 2 || (adata.nargs == 3 && args[2]==ibeam)){
		double k=args[0];
                quad_(beam,&npart,&k,&args[1]);
            }
        }else if (adata.actcode == 7) {
            // noise source
            if(args[0]==1) {
                // white noise (constant dipolar kick over the bunch length)
                double xampl = args[1]*rms[1];
                double yampl = args[2]*rms[3];
                whitenoise_(beam,&npart,&xampl,&yampl);
            }else if(args[0]==2){
                // sinusoidal excitation with finite correlation
                double ampl[2] = {args[1]*rms[1],args[2]*rms[3]};
                double freq[2] = {2*M_PI*args[3],2*M_PI*args[4]};
                double phase[2] = {args[5],args[6]};
                double t = adata.turn+myLongitudinalPosition/revolutionTime;
                //printf("B%ib%i : %f\n",ibeam,ibunch,t);
                sinnoise_(beam,&npart,ampl,freq,phase,&t);
            }
            else if(args[0]==3){
                  // white noise (constant dipolar kick over the bunch length)
                  double xampl = args[1]*rms[1]/rms[0];
                  double yampl = args[2]*rms[3]/rms[2];
                  quadwhitenoise_(beam,&npart,&xampl,&yampl);
            } else if(args[0]==4){
                // incoherent gaussian noise in the transverse plane
                double xampl = args[1]*rms[1];
                double yampl = args[2]*rms[3];
                transincnoise_(beam,&npart,&xampl,&yampl);
            }
 	    else if(args[0]==5){
                // linear CC ampl noise
                double xampl = args[1]*rms[1]/rms[4];
                double yampl = args[2]*rms[3]/rms[4];
                ccamplnoise_(beam,&npart,&xampl,&yampl);
            }
        } else if (adata.actcode == 6) {
        }else if (adata.actcode == 8) {
            // Sweeping sinusidal excitation on one beam. (NEVER USED)
            if(ibeam == 1) {
                double stepLength = 2000.0;
                int nstep = (int) (adata.turn/stepLength);
                if(nstep%2 == 0) {
                    double ampl[2] = {args[0]*rms[1],args[1]*rms[3]};
                    double freq = args[2]+nstep*args[3];
                    double freqs[2] = {freq,freq};
                    double phase[2] = {0,0};
                    double t = adata.turn;
                    sinnoise_(beam,&npart,ampl,freqs,phase,&t);
                }
            }
            ///////////////////////////////////////////////////////////////////////////
        } else if(adata.actcode == 12) {
            // Impedance
#ifdef TIMING
            ////////////////////////////////////////////////////////
            timeval zStart;
            gettimeofday(&zStart,NULL);
            ////////////////////////////////////////////////////////
#endif
            beampar_(beam,&npartReal, bparam,rms, &particleCharge);
            updateZ(&passageRecords,revolutionTime,adata.turn,myLongitudinalPosition);
            wake* wake = ibeam==1 ? &wakes[(int)args[2]] : &wakes[(int)args[4]];
            double scale = ibeam==1 ? args[1] : args[3];
#ifdef TDEBUG
            for(int i = 0;i<passageRecords.length;++i) {
                printf("Process %i : passage record %i , Z %E, turn %i / %i\n",myid,i,getElement(&passageRecords,i).Z,getElement(&passageRecords,i).turn,adata.turn);
            }
#endif
            computeSlicesFirstOrderMoment(beam,npart,particleCharge,(int)args[0],sliceMoment1,sLimInf,sLimSup);
            zkick_equiDistant(beam,npartReal,particleCharge,zKickScale*scale,(int)args[0],wake,sliceMoment1,zWorkspace3,&passageRecords,sLimInf,sLimSup); //TODO npartTest should be in as well            
            set(tmpPassageRecord,adata.turn,myLongitudinalPosition,bparam[0],bparam[2],bparam[10]);
#ifdef TIMING
            /////////////////////////////////////////////////////////////////////
            timeval zEnd;
            gettimeofday(&zEnd,NULL);
            double duration = zEnd.tv_sec-zStart.tv_sec+(zEnd.tv_usec-zStart.tv_usec)/1E6;
            printf("Process %d : time for impedance kick : %f s\n",myid,duration);
            //////////////////////////////////////////////////////////////////////
#endif
        } else if(adata.actcode == 13) {
            // non-linear transport (linear detuning)
#ifdef TIMING
            ////////////////////////////////////////////////////////
            timeval zStart;
            gettimeofday(&zStart,NULL);
            ////////////////////////////////////////////////////////
#endif
            transferoct_(beam,&npart,rms,&args[0],&args[1],&args[2],&args[3],&parserOutput.betaStar[0],&parserOutput.betaStar[1]);
#ifdef TIMING
            /////////////////////////////////////////////////////////////////////
            timeval zEnd;
            gettimeofday(&zEnd,NULL);
            double duration = zEnd.tv_sec-zStart.tv_sec+(zEnd.tv_usec-zStart.tv_usec)/1E6;
            printf("Process %d : time for non linear transfer : %f s\n",myid,duration);
            //////////////////////////////////////////////////////////////////////
#endif
        } else if(adata.actcode == 14) {
            // trasnverse damper
            if(args[0] == 0) {
                // perfect damper
                beampar_(beam,&npartReal, bparam,rms, &particleCharge);
                if(ibeam == 1) {
                    perfectDamperKick(beam,npart,bparam,args[1],args[2]);
                } else {

                    perfectDamperKick(beam,npart,bparam,args[3],args[4]);
                }
            } else if (args[0] == 1) {
                beampar_(beam,&npartReal, bparam,rms, &particleCharge);
                if(ibeam == 1) {
                    thresholdedDamperKick(beam,npart,bparam,args[1],args[2],args[5],args[6]);
                } else {
                    thresholdedDamperKick(beam,npart,bparam,args[3],args[4],args[7],args[8]);
                }
            } else if (args[0] == 2) {
                beampar_(beam,&npartReal, bparam,rms, &particleCharge);
                computeSlicesFirstOrderMoment(beam,npart,particleCharge,(int)args[5],sliceMoment1,sLimInf,sLimSup);
                if(ibeam == 1) {
                    singleTurnADT(beam,npart,sliceMoment1,(int)args[5],args[6],args[1],args[2]);
                    //singleTurnADT_debug(beam,npart,sliceMoment1,(int)args[5],args[6],args[1],args[2],bparam,ibeam);
                } else {
                    singleTurnADT(beam,npart,sliceMoment1,(int)args[5],args[6],args[3],args[4]);
                    //singleTurnADT_debug(beam,npart,sliceMoment1,(int)args[5],args[6],args[3],args[4],bparam,ibeam);
                }
            }
        }else if (adata.actcode == 15) {
            if (args[0] == 1) {
                if(ibeam==1){
                    synchrad_(beam,&npart,&parserOutput.energy[0],&parserOutput.mass[0],&args[1],&parserOutput.circumference,&parserOutput.momentumCompaction,&parserOutput.averageBetaH,&parserOutput.averageBetaV,&parserOutput.betaStar[0],&parserOutput.betaStar[1],&args[2],synchrotronRadiationTable,&synchrotronRadiationTableSize);
                } else {
                    synchrad_(beam,&npart,&parserOutput.energy[1],&parserOutput.mass[1],&args[1],&parserOutput.circumference,&parserOutput.momentumCompaction,&parserOutput.averageBetaH,&parserOutput.averageBetaV,&parserOutput.betaStar[2],&parserOutput.betaStar[3],&args[2],synchrotronRadiationTable,&synchrotronRadiationTableSize);
                }
            } else if(args[0] == 2) {
                if(ibeam==1){
                    //synchrad_gauss_(beam,&npart,&parserOutput.energy[0],&parserOutput.mass[0],&args[1],&parserOutput.circumference,&parserOutput.momentumCompaction,&parserOutput.averageBetaH,&parserOutput.averageBetaV,&parserOutput.betaStar[0],&parserOutput.betaStar[1],&args[2]);
                    synchradgaussintegral_(beam,&npart,&parserOutput.energy[0],&parserOutput.mass[0],&args[1],&args[2],&args[3],&parserOutput.betaStar[0],&args[4]);
                } else {
                    synchradgaussintegral_(beam,&npart,&parserOutput.energy[1],&parserOutput.mass[1],&args[1],&args[2],&args[3],&parserOutput.betaStar[0],&args[4]);
                }
            } else if(args[0] == 3) {
                if(ibeam==1){
                    double phEmit= args[1]*parserOutput.mass[0]/parserOutput.energy[0];
                    synchradgauss_(beam,&npart,&phEmit,&args[2],&parserOutput.betaStar[0],&args[3]);
                } else {
                    double phEmit= args[1]*parserOutput.mass[1]/parserOutput.energy[1];
                    synchradgauss_(beam,&npart,&phEmit,&args[2],&parserOutput.betaStar[0],&args[3]);
                }
            }
        } else if (adata.actcode == ABORTACTION) {
            // EXIT message from MASTER
            if(parserOutput.dumpBeamToFile) {
                char fileName[200];
                sprintf(fileName,parserOutput.dumpBeamFileFormat,ibeam,ibunch,adata.turn);
                writeBeam(parserOutput.outputDir,fileName,beam,npart);
            }
            break;
        }

        // Write out beam parameters if requested
        if (adata.writeToFile)
        {
#ifdef TDEBUG
            printf("...Process %d: writing to file\n",myid);
            fflush(stdout);
#endif
            beampar_(beam,&npartReal, bparam,rms, &particleCharge);
            char fileName[200];

            sprintf(fileName,parserOutput.dumpParameterFileFormat,ibeam,ibunch);

            writeBeamParameter(parserOutput.outputDir,fileName,bparam);
            if(parserOutput.testParticle) {
                int dumpFreq = 10000;
                int mod = (adata.turn-1)%dumpFreq;
                int span = 2048;
                if(mod<span) {
                    writeTestParticle(parserOutput.outputDir,parserOutput.testParticleFileFormat,ibeam,ibunch,beam,bparam,npartReal,npartTest);
                }
            }
            //////dump beam to file at fixed frequency. Use with care/////
            //if(parserOutput.dumpBeamToFile) {
            //    if(adata.turn%parserOutput.dumpBeamToFileFreq==0) {
                    //printf("...Process %d: writing to file in\n",myid);
                    //fflush(stdout);
            //        char fileName[200];
            //        sprintf(fileName,parserOutput.dumpBeamFileFormat,ibeam,ibunch,adata.turn);
            //        writeBeam(parserOutput.outputDir,fileName,beam,parserOutput.dumpBeamNPart);
            //    }
            //}
            ///////////////////////////////////////////////////////////////
            //////dump beam to file, for specific analysis/////
            //if(parserOutput.dumpBeamToFile) {
            //    int mod = adata.turn%parserOutput.dumpBeamToFileFreq;
            //    int span = 1024;
            //    if(mod==0){
            //        char fileName[200];
            //        sprintf(fileName,parserOutput.dumpBeamFileFormat,ibeam,ibunch,adata.turn);
            //        writeBeam(parserOutput.outputDir,fileName,beam,npart);
            //    }else if(mod<=span) {
                    //printf("...Process %d: writing to file in\n",myid);
                    //fflush(stdout);
            //        char fileName[200];
            //        sprintf(fileName,parserOutput.dumpBeamFileFormat,ibeam,ibunch,adata.turn);
            //        writeBeam(parserOutput.outputDir,fileName,beam,parserOutput.dumpBeamNPart);
            //    }
            //}
            ///////////////////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////
            //if(parserOutput.dumpSlicePos) {
            //    if(adata.turn%parserOutput.dumpSlicePosFreq==0) {
            //        computeSlicesFirstOrderMoment(beam,npart,particleCharge,parserOutput.dumpSlicePosNSlice,sliceMoment1,sLimInf,sLimSup);
            //        
            //        char fileName[200];
            //        sprintf(fileName,parserOutput.dumpSlicePosFileFormat,ibeam,ibunch,'H');
            //        writeSlicePositions(parserOutput.outputDir,fileName,sliceMoment1,parserOutput.dumpSlicePosNSlice,0);
            //        sprintf(fileName,parserOutput.dumpSlicePosFileFormat,ibeam,ibunch,'V');
            //        writeSlicePositions(parserOutput.outputDir,fileName,sliceMoment1,parserOutput.dumpSlicePosNSlice,1);
            //        sprintf(fileName,parserOutput.dumpSlicePosFileFormat,ibeam,ibunch,'S');
            //        writeSlicePositions(parserOutput.outputDir,fileName,sliceMoment1,parserOutput.dumpSlicePosNSlice,2);
            //        sprintf(fileName,parserOutput.dumpSlicePosFileFormat,ibeam,ibunch,'Q');
            //        writeSlicePositions(parserOutput.outputDir,fileName,sliceMoment1,parserOutput.dumpSlicePosNSlice,3);
            //    }
            //}

            if(parserOutput.dumpSlicePos) {
                if(adata.turn%parserOutput.dumpSlicePosFreq==0) {
                    computePizzaSlicesFirstOrderMoment(beam,npart,particleCharge,rms,parserOutput.dumpSlicePosNSlice,parserOutput.dumpSlicePosNSlice,pizzaSliceMoment1,3);
                    char fileName[200];
                    sprintf(fileName,parserOutput.dumpSlicePosFileFormat,ibeam,ibunch,'x');
                    writePizzaSlicePositions(parserOutput.outputDir,fileName,pizzaSliceMoment1,parserOutput.dumpSlicePosNSlice,parserOutput.dumpSlicePosNSlice,0);
                    sprintf(fileName,parserOutput.dumpSlicePosFileFormat,ibeam,ibunch,'X');
                    writePizzaSlicePositions(parserOutput.outputDir,fileName,pizzaSliceMoment1,parserOutput.dumpSlicePosNSlice,parserOutput.dumpSlicePosNSlice,1);
                    sprintf(fileName,parserOutput.dumpSlicePosFileFormat,ibeam,ibunch,'y');
                    writePizzaSlicePositions(parserOutput.outputDir,fileName,pizzaSliceMoment1,parserOutput.dumpSlicePosNSlice,parserOutput.dumpSlicePosNSlice,2);
                    sprintf(fileName,parserOutput.dumpSlicePosFileFormat,ibeam,ibunch,'Y');
                    writePizzaSlicePositions(parserOutput.outputDir,fileName,pizzaSliceMoment1,parserOutput.dumpSlicePosNSlice,parserOutput.dumpSlicePosNSlice,3);
                    sprintf(fileName,parserOutput.dumpSlicePosFileFormat,ibeam,ibunch,'s');
                    writePizzaSlicePositions(parserOutput.outputDir,fileName,pizzaSliceMoment1,parserOutput.dumpSlicePosNSlice,parserOutput.dumpSlicePosNSlice,4);
                    sprintf(fileName,parserOutput.dumpSlicePosFileFormat,ibeam,ibunch,'S');
                    writePizzaSlicePositions(parserOutput.outputDir,fileName,pizzaSliceMoment1,parserOutput.dumpSlicePosNSlice,parserOutput.dumpSlicePosNSlice,5);
                    sprintf(fileName,parserOutput.dumpSlicePosFileFormat,ibeam,ibunch,'Q');
                    writePizzaSlicePositions(parserOutput.outputDir,fileName,pizzaSliceMoment1,parserOutput.dumpSlicePosNSlice,parserOutput.dumpSlicePosNSlice,6);
                }
            }

            //////////////////////////////////////////////////////
            if(parserOutput.dumpBeamDistribution) {
                if(adata.turn%parserOutput.dumpBeamDistributionFreq==0) {
                    char fileName[200];
                    sprintf(fileName,parserOutput.dumpBeamDistributionFileFormat,ibeam,ibunch,adata.turn);
                    //TODO move hardcoded value to inputs
                    writeBeamDistribution(parserOutput.outputDir,fileName,beam,npart,rms,0.0,18.0,720,parserOutput.beamDistributionCoordinate,beamDistrWorkspace);
                }
            }
           if(parserOutput.dumpTransverseBeamDistribution) {
                if(adata.turn%parserOutput.dumpTransverseBeamDistributionFreq==0) {
                    char fileName[200];
                    sprintf(fileName,parserOutput.dumpTransverseBeamDistributionFileFormat,ibeam,ibunch,adata.turn);
                    //TODO move hardcoded value to inputs
                    writeTransverseBeamDistribution(parserOutput.outputDir,fileName,beam,npart,rms,0.0,18,360,beamDistrWorkspace);
                }
            }
            if(parserOutput.abortIfUnstable) { 
               if(bparam[0]/rms[0] > parserOutput.instabilityThreshold || (bparam[8]-1.0) > parserOutput.instabilityThreshold) {
                    printf("B%ib%i unstable in horizontal plane, requesting abort\n",ibeam,ibunch);
                    adata.actcode = -1;
                }
                if(bparam[2]/rms[2] > parserOutput.instabilityThreshold || (bparam[9]-1.0) > parserOutput.instabilityThreshold) {
                    printf("B%ib%i unstable in vertical plane, requesting abort\n",ibeam,ibunch);
                    adata.actcode = -1;
                }
            }
        }

        // SEND COMPLETION NOTE TO MASTER

#ifdef TDEBUG
        printf("Process %d:  Sending COMPLETION , actcode %i to MASTER\n",myid,adata.actcode);
        fflush(stdout);
#endif
        MPI_Send(&adata, sizeof(adata), MPI_BYTE, masterRank,TAG_COMPLETION, MPI_COMM_WORLD);
        if(adata.actcode == 12) {
            MPI_Send(tmpPassageRecord, sizeof(PassageRecord),MPI_BYTE,masterRank, TAG_PASSAGERECORD,MPI_COMM_WORLD);
        }
        //free(args);
#ifdef NICE
	    errno = 0;
	    //printf("Process %d, priority before %d\n",myid,getpriority(PRIO_PROCESS,myPID));
        setpriority(PRIO_PROCESS,myPID,niceLow);
        //printf("Process %d, priority after %d (error %d)\n",myid,getpriority(PRIO_PROCESS,myPID),errno);
#endif
#ifdef RTSCHEDULE
        //sched_param tmpParam;
        //int error = sched_getparam(myPID,&tmpParam);
        //printf("Process %d, RT priority before %d (error %d)\n",myid,tmpParam.sched_priority,error);
        //errno = 0;
        sched_setparam(myPID,realTimeLow);  
        //printf("Process %d, setting RT priority to %d (error %d (%d,%d,%d))\n",myid,realTimeLow->sched_priority,errno,EINVAL,EPERM,ESRCH);
        //error = sched_getparam(myPID,&tmpParam);
        //printf("Process %d, RT priority after %d (error %d)\n",myid,tmpParam.sched_priority,error);
#endif
    }  // while(1)

#ifdef TDEBUG
    printf("...Process %d:  Rec'd EXIT message from master\n", myid);
    fflush(stdout);
#endif

}





#include "zkick.h"
#include "const.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <omp.h>
#include "slice.h"

int binarySearch(double* array,const int& size,const double& value,int lower,int upper,int& nStep) {
    int pivot = (lower+upper)/2;
    if((upper-lower)<=1) {
        return lower;
    }
    if(value < array[pivot]) {
        upper = pivot;
        pivot = (lower+upper)/2;
        ++nStep;
        return binarySearch(array,size,value,lower,upper,nStep);
    }else if(value > array[pivot]) {
        lower = pivot;
        pivot = (lower+upper)/2;
        ++nStep;
        return binarySearch(array,size,value,lower,upper,nStep);
    } else {
        return pivot;
    }
}

void getWake(wake* wake, double z,double* dipWakes,double* quadWakes) {
    if(z<wake->z[0] || z>wake->z[wake->size-1]) {
        dipWakes[0] = 0.0;
        dipWakes[1] = 0.0;
        quadWakes[0] = 0.0;
        quadWakes[1] = 0.0;
    }else {
        int lower = 0;
        int upper = wake->size;
        int nStep = 0;
        int i = binarySearch(wake->z,wake->size,z,lower,upper,nStep);
        //printf("getwake nstep %i\n",nStep);
        dipWakes[0] = interp(z,wake->z[i],wake->z[i+1],wake->wxDip[i],wake->wxDip[i+1]);
        dipWakes[1] = interp(z,wake->z[i],wake->z[i+1],wake->wyDip[i],wake->wyDip[i+1]);
        quadWakes[0] = interp(z,wake->z[i],wake->z[i+1],wake->wxQuad[i],wake->wxQuad[i+1]);
        quadWakes[1] = interp(z,wake->z[i],wake->z[i+1],wake->wyQuad[i],wake->wyQuad[i+1]);
    }
}

double interp(double x,double x1,double x2,double fx1,double fx2) {
    return fx1+(x-x1)*(fx2-fx1)/(x2-x1);
}


// sliceWakes[iSlice] (xkick)
// sliceWakes[nSlice+iSlice] (ykick)
// sliceWakes[2*nSlice+iSlice] (xquadwake)
// sliceWakes[3*nSlice+iSlice] (yquadwake)
void zkick_equiDistant(double* beam,int npart,double particleCharge,double scale,int nSlice,wake* wake,double* sliceMoments,double* sliceWakes,PassageRecords* passageRecords,double limInf,double limSup) {
#ifdef ZTIMING
    timeval zstart;
    gettimeofday(&zstart,NULL);
#endif
    //communication array
    double dipWakes[2];
    double quadWakes[2];
    //compute wake from previous bunches (including itself)
    double oldXkick = 0.0;
    double oldYkick = 0.0;
    double oldXQuadWake = 0.0;
    double oldYQuadWake = 0.0;
#ifdef ZTIMING    
    timeval start;
    timeval end;
    gettimeofday(&start,NULL);
#endif
    for(int i = 0;i < passageRecords->length;++i) {
        getWake(wake,getElement(passageRecords,i).Z,dipWakes,quadWakes);
        oldXkick += getElement(passageRecords,i).charge*dipWakes[0]*getElement(passageRecords,i).X;
        oldYkick += getElement(passageRecords,i).charge*dipWakes[1]*getElement(passageRecords,i).Y;
        oldXQuadWake += getElement(passageRecords,i).charge*quadWakes[0];
        oldYQuadWake += getElement(passageRecords,i).charge*quadWakes[1];
    }
#ifdef ZTIMING
    gettimeofday(&end,NULL);
    double duration = end.tv_sec-start.tv_sec+(end.tv_usec-start.tv_usec)/1E6;
    printf("Time to comptute wake from previous bunch : %f s\n",duration);
    gettimeofday(&start,NULL);
#endif

#ifdef ZTIMING
    gettimeofday(&end,NULL);
    duration = end.tv_sec-start.tv_sec+(end.tv_usec-start.tv_usec)/1E6;
    printf("Time to compute moments : %f s\n",duration);
    gettimeofday(&start,NULL);
#endif
    //computing wakes
    // reinitializing arrays
    for(int iSlice = 0;iSlice<nSlice;++iSlice) {
        sliceWakes[iSlice] = 0.0;
        sliceWakes[nSlice + iSlice] = 0.0;
        sliceWakes[2*nSlice + iSlice] = 0.0;
        sliceWakes[3*nSlice + iSlice] = 0.0;
    }
    for(int iSlice=0;iSlice<nSlice;++iSlice) {
        for(int j=0;j<iSlice;++j) {
            //printf("Process %i : computing wake for slice %i, from %i\n",myid,iSlice,j);
            //fflush(stdout);
            getWake(wake, sliceMoments[2*nSlice + j]-sliceMoments[2*nSlice + iSlice],dipWakes,quadWakes);
            //printf("Process %i : adding wake for slice %i from slice %i\n",myid,iSlice,j);
            //fflush(stdout);
            sliceWakes[iSlice] += sliceMoments[3*nSlice + j]*dipWakes[0]*sliceMoments[j];
            sliceWakes[nSlice + iSlice] += sliceMoments[3*nSlice + j]*dipWakes[1]*sliceMoments[nSlice + j];
            sliceWakes[2*nSlice + iSlice] += sliceMoments[3*nSlice + j]*quadWakes[0];
            sliceWakes[3*nSlice + iSlice] += sliceMoments[3*nSlice + j]*quadWakes[1];
            //printf("Process %i : wake for slice %i from slice %i ok\n",myid,iSlice,j);
        }
    }
#ifdef ZTIMING
    gettimeofday(&end,NULL);
    duration = end.tv_sec-start.tv_sec+(end.tv_usec-start.tv_usec)/1E6;
    printf("Time to compute wakes : %f s\n",duration);
    gettimeofday(&start,NULL);
#endif
    //applying kicks
    #pragma omp parallel for schedule(guided,1000) default(none) firstprivate(scale,npart,nSlice,limInf,limSup,oldXkick,oldXQuadWake,oldYkick,oldYQuadWake) shared(sliceWakes,beam)
    for(int i = 0;i<npart;i++) {
        int iSlice = getSliceNumber(beam[i*NCOORD+4],nSlice,limInf,limSup);
        beam[i*NCOORD+1] += scale*(sliceWakes[iSlice] + oldXkick + (sliceWakes[2*nSlice + iSlice]+oldXQuadWake)*beam[i*NCOORD]);
        beam[i*NCOORD+3] += scale*(sliceWakes[nSlice + iSlice] + oldYkick + (sliceWakes[2*nSlice + iSlice]+oldYQuadWake)*beam[i*NCOORD+2]);
    }
#ifdef ZTIMING
    gettimeofday(&end,NULL);
    duration = end.tv_sec-start.tv_sec+(end.tv_usec-start.tv_usec)/1E6;
    printf("Time to apply kicks: %f s\n",duration);
    timeval zend;
    gettimeofday(&zend,NULL);
    duration = zend.tv_sec-zstart.tv_sec+(zend.tv_usec-zstart.tv_usec)/1E6;
    printf("TOTAL TIME for impedance kic: %f s\n",duration);
#endif
}



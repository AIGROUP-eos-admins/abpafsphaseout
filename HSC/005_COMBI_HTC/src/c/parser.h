#ifndef __PARSERHEADER__
#define __PARSERHEADER__

#include "const.h"
#include "defs.h"
#include "action.h"

struct ParserOutput {
	bool valid;
	char* collisionFile;
	char* fillingFileA;
	char* fillingFileB;
	char* outputDir;
	int nturn;
	float wallTime;
	int npart;
	double kick;
	double xfact;
	double* bunchIntensity;
	double* emittanceB1;
	double* emittanceB2;
	double* betaStar;
	double* energy;
	double* mass;
	double* charge;
    bool abortIfUnstable;
    double instabilityThreshold;

	char* dumpParameterFileFormat;
	bool loadBeamFromFile;
	char* beamFileFormat;
	bool dumpBeamToFile;
	char* dumpBeamFileFormat;
	int dumpBeamToFileFreq;
	int firstTurn;

    bool dumpBeamDistribution;
    int beamDistributionCoordinate;
    char* dumpBeamDistributionFileFormat;
    int dumpBeamDistributionFreq;
    
    bool dumpTransverseBeamDistribution;
    char* dumpTransverseBeamDistributionFileFormat;
    int dumpTransverseBeamDistributionFreq;
    int dumpBeamNPart;
    
    bool dumpSlicePos;
    char* dumpSlicePosFileFormat;
    int dumpSlicePosNSlice;
    int dumpSlicePosFreq;
    
    bool dumpBunchCollisionSchedule;
    char* bunchCollisionScheduleFileFormat;

	bool testParticle;
	double* testParticleAmplitude;
	double* testParticleAngle;
	char* testParticleFileFormat;
	
	double circumference;
	double bunchSpacing;
	double bunchLength;
	double relativeMomentumSpread;
	
	int nWakeFiles;
	char** wakeFiles;
	int wakeLength;
	double* zBeta;
	
	double momentumCompaction;
	double omegaRF;
	double voltRF;
	double averageBetaH;
	double averageBetaV;
	char* syncRadTableFileName;

    unsigned int mR;
    unsigned int nTheta;
};

ParserOutput parse(int argc,char *argv[]);

bool exists(char* fileName);

bool parseBeamFile(double* beam,int npart, char* fileName);

bool parseCollisionFile(char* fileName,action* col,int npos);

bool parseWake(char* fileName,wake* wake);
bool parseWake(char* fileName,wake* wake,double betax, double betay);
bool parseSynchrotronTable(char* fileName,double** synchrotronRadiationTable ,int* synchrotronRadiationTableSize);

#endif

#include "extern.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "const.h"

# define NXG 100
# define NYG 100
# define NGRID ((2*NXG+1)*(2*NYG+1))

//TODO understand why there are some NAN (setup of hfmm must be incomplete)

float CHG[NGRID], POTG[NGRID],EXG[NGRID],EYG[NGRID];

int main(int argc, char *argv[])
{
    unsigned int currentTime = time(NULL);
    rdmin_(&currentTime);
    unsigned int seed;
    rdmout_(&seed);
    
    int ibeam = 1;
    int ibunch = 1;
    double energy = 4000;
    double mass = 0.938272;
    double charge = 1;
    double bunchIntensity = 1E11;
    double emit = 1.5E-6;
    double betastar = 0.6;
    int npart = 1E6;
    double xfact = 0.0;
    double xkick = 0.0;
    double xsep = 0.0;
    double ysep = 0.0;
    
    initconst_(&ibeam,&ibunch,&energy,&mass,&charge);
    double particleCharge = charge >= 0.0 ? bunchIntensity/npart : -1.0*bunchIntensity/npart;
    
    double rms[NCOORD];
    rms[0] = sqrt(mass*emit*betastar);
    rms[1] = sqrt(mass*emit/betastar);
    rms[2] = sqrt(mass*emit*betastar);
    rms[3] = sqrt(mass*emit/betastar);

    double* beam = (double*)malloc(npart*NCOORD*sizeof(double));
    for(int i = 0;i<npart*NCOORD;++i) {
        beam[i] = 0;
    }
    double bparam[NPARAM];
    bfillpar_(beam,&npart,bparam,rms,&bunchIntensity,&xfact, &xkick, &seed);
    beampar_(beam,&npart,bparam,rms, &particleCharge);
    //************init HFMM***************/
    
    
	// Arrays for FMM charge coordinates
    int stdSize = 2*(npart+(2*NXG+1)*(2*NYG+1));
	float* XCHARGEB1 = (float*) malloc(stdSize*sizeof(float));
	float* YCHARGEB1 = (float*) malloc(stdSize*sizeof(float));
    float* XCHARGEB2 = (float*) malloc(stdSize*sizeof(float));
	float* YCHARGEB2 = (float*) malloc(stdSize*sizeof(float));
	// Array for FMM charge values (COMPLEX type in Fortran)
	float* QAB1 = (float*) malloc(2*stdSize*sizeof(float));
	float* QAB2 = (float*) malloc(2*stdSize*sizeof(float));
	// Array for packed charges & count, for sending/receiving
	float* CHARGEB1 = (float*) malloc(3*stdSize*sizeof(float));
	float* CHARGEB2 = (float*) malloc(3*stdSize*sizeof(float));
	float* CHARGE2 = (float*) malloc(3*stdSize*sizeof(float));
	// Index array for HFMM grid
	int* INDXB1 = (int*) malloc(npart*sizeof(float));
	int* INDXB2 = (int*) malloc(npart*sizeof(float));
	// Arrays for calculated fields at particle locations
	float* EX = (float*) malloc(npart*sizeof(float));
	float* EY = (float*) malloc(npart*sizeof(float));
	// Workspace for the hfmm
	int wkspSize = 32*(npart+NGRID)+1000;
	float* WKSP = (float*) malloc(wkspSize*sizeof(float));
	int hfmmInfoSize = 4;
	int* hfmmInfo = (int*) malloc(hfmmInfoSize*sizeof(int));

	// Initialize FMM parameters
	int fmmout = 0;
	int fmmbox = 400;
	float fmmtol = 0.05;
	float fmmcut = 1.e-12;
	fmmsetup_(&fmmout, &fmmbox, &fmmtol, &fmmcut);

	// FMM options
	int NATB1 = 0;
	int NATB2 = 0;
	int NXGR = NXG; int NYGR = NYG;
	float DXGR = 0.2*rms[0]; float DYGR = 0.2*rms[2];
	int FMMTEST = 0;
	int FMMGRID = 1;
	int CLOUD = 1;
	float CCONST = 1.;
	
	//**********************************/
	//************BB kick***************/
    int myid = 0;
	hfmmprep_(CHG,&NXGR,&NYGR,&DXGR,&DYGR,&FMMTEST,
			beam,&npart,&FMMGRID,&CLOUD,
			INDXB1,XCHARGEB1,YCHARGEB1,QAB1,&NATB1,
			CHARGEB1,&particleCharge,&xsep,&ysep);
    
    int ntest=1E3;
    double nMin=-6;
    double nMax=6;
    double* testBeam = (double*)malloc(npart*NCOORD*sizeof(double));
    for(int i = 0;i<npart*NCOORD;++i) {
        testBeam[i] = 0;
    }
    //rdmout_(&seed);
    ibeam = 2;
    initconst_(&ibeam,&ibunch,&energy,&mass,&charge);
    bfillpar_(testBeam,&npart,bparam,rms,&bunchIntensity,&xfact, &xkick, &seed);
    
    
    for(int i=0;i<ntest*NCOORD;i+=NCOORD) {
            double n = ((double)i)/(NCOORD*ntest);
            testBeam[i] = (nMin+(nMax-nMin)*n)*rms[0];
            //printf("%i,%f,%.20E\n",i,n,testBeam[i]);
            //for(int j=1;j<NCOORD;++j){
            //    testBeam[i+j] = 0.0;
            //}
            testBeam[i+1] = 0;
            testBeam[i+2] = 0;
            testBeam[i+3] = 0;
            testBeam[i+6] = 0;
    }
    
    
	hfmmprep_(CHG,&NXGR,&NYGR,&DXGR,&DYGR,&FMMTEST,
			testBeam,&npart,&FMMGRID,&CLOUD,
			INDXB2,XCHARGEB2,YCHARGEB2,QAB2,&NATB2,
			CHARGEB2,&particleCharge,&xsep,&ysep);
	hfmmcalc_(CHG,EXG,EYG,POTG,&NXGR,&NYGR,
			&DXGR,&DYGR,&FMMTEST,
			testBeam,&npart,INDXB2,
			XCHARGEB2,YCHARGEB2,QAB2,CHARGEB1,
			&NATB2,&NATB1,
			EX,EY,&FMMGRID,&CLOUD,&CCONST,
			&myid,
			&wkspSize,WKSP,
			hfmmInfo,&hfmmInfoSize);
    double xHOkick = 0.0;
    double yHOkick = 0.0;
    pkick_(testBeam,&npart,&particleCharge, EX, EY,&xHOkick,&yHOkick);

     //**************************************/
    //************print beam***************/

    FILE* file = fopen("testHO_H.csv","w");
    for(int i=0;i<ntest*NCOORD;i+=NCOORD) {
            fprintf(file,"%.20E,%.20E\n",testBeam[i]/rms[0],testBeam[i+1]);
    }
    fprintf(file,"\n");
    fclose(file);
    
    /*
    for(int i=0;i<ntest*NCOORD;i+=NCOORD) {
        double n = ((double)i)/(NCOORD*ntest);
        testBeam[i] = 0.0;
        testBeam[i+1] = 0.0;
        testBeam[i+2] = (nMin+(nMax-nMin)*n)*rms[2];
        //printf("%i,%f,%.20E\n",i,n,testBeam[i+2]);
        for(int j=3;j<NCOORD;++j){
            testBeam[i+j] = 0.0;
        }
    }
    */
}

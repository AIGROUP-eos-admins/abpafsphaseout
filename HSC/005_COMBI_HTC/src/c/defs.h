#ifndef __DEFSHEADER__
#define __DEFSHEADER__

struct initdata {
   unsigned int iseed;
   int ibeam;
   int ibunch;
   double xfact;
   double kick;
   bool abort;
   int numberOfBunch;
   double longitudinalPosition;
};

struct fielddata {
   double x0;
   double y0;
};

struct wake {
   int size;
   double* z;
   double* wxDip;
   double* wyDip;
   double* wxQuad;
   double* wyQuad;
};

const double SPEEDOFLIGHT =  299792458;

// The following structs are for possible future use

//struct beampar {
//   double x0, xp0, y0, yp0;
//   double sigx, sigxp, sigy, sigyp;
//};
//struct beampar stats, statr;

//struct coords {
//   double x;
//   double xp;
//   double y;
//   double yp;
//};

//struct coords coordata[NPMAX + 1];
//double X[NPMAX + 1];
//double XP[NPMAX + 1];
//double Y[NPMAX + 1];
//double YP[NPMAX + 1];

#endif


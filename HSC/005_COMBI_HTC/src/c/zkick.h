#ifndef __ZKICKHEADER__
#define __ZKICKHEADER__


#include "defs.h"
#include "passageRecord.h"

void getWake(wake* wake, double z,double* kicks,double* quadWakes);
double interp(double x,double x1,double x2,double fx1,double fx2);
void zkick_equiDistant(double* beam,int npart,double particleCharge,double scale,int nSlice,wake* wake,double* sliceMoments,double* sliceWakes,PassageRecords* passageRecords,double limInf,double limSup);
int binarySearch(double* array,const int& size,const double& value,int lower,int upper,int& nStep);

#endif

#include "parser.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

bool getValue(ParserOutput*,char*,char*);
bool testArgs(action action);

/************************************************************************/
/*                                                                      */
/* output is invalid if                                                 */
/*	file in argument is not exist and in.in does not exist          */
/*	one of the lines is not structured like ID: VALUE1 VALUE2...	*/
/*	one of the ID is not recongnized                                */
/*                                                                      */
/* beamFileFormat example : B%ib%i.beam                                 */
/* use // at the begining of the line to comment                        */
/*                                                                      */
/************************************************************************/

ParserOutput parse(int argc,char *argv[]) {
	ParserOutput retVal;
	FILE* file;
    char* inputFileName;
	if(argc>1) {
        inputFileName = argv[1];
	} else {
        printf("WARNING : no input file provided, default in.in\n");
		inputFileName = (char*)"in.in";
	}
	file = fopen(inputFileName,"rb");
	if(file != NULL) {
		retVal.valid = true;
		// set default values
		char tmp[] = "B%ib%i.bparam";
		retVal.dumpParameterFileFormat = (char*) malloc(strlen(tmp)*sizeof(char));
		strcpy(retVal.dumpParameterFileFormat,tmp);
		retVal.loadBeamFromFile = false;
		retVal.firstTurn = 0;
		retVal.dumpBeamToFile = false;
		retVal.dumpBeamToFileFreq = -1;
        retVal.dumpBeamNPart = -1;
		retVal.wallTime = -1;
		retVal.bunchIntensity = (double*) malloc(2*sizeof(double));
		retVal.bunchIntensity[0] = 1.15E11;
		retVal.bunchIntensity[1] = 1.15E11;
		retVal.kick = 0.0;
		retVal.xfact = 0.0;
		retVal.emittanceB1 = (double*) malloc(3*sizeof(double));
		retVal.emittanceB1[0] = 3.75E-6;
		retVal.emittanceB1[1] = 3.75E-6;
		retVal.emittanceB1[2] = 2.5;
		retVal.emittanceB2 = (double*) malloc(3*sizeof(double));
		retVal.emittanceB2[0] = 3.75E-6;
		retVal.emittanceB2[1] = 3.75E-6;
		retVal.emittanceB2[2] = 2.5;
		retVal.betaStar = (double*) malloc(2*sizeof(double));
		retVal.betaStar[0] = 0.55;
		retVal.betaStar[1] = 0.55;
		retVal.energy = (double*) malloc(2*sizeof(double));
		retVal.energy[0] = 7000.0;
		retVal.energy[1] = 7000.0;
		retVal.mass = (double*) malloc(2*sizeof(double));
		retVal.mass[0] = 0.938272;
		retVal.mass[1] = 0.938272;
		retVal.charge = (double*) malloc(2*sizeof(double));
		retVal.charge[0] = 1.0;
		retVal.charge[1] = 1.0;
		retVal.testParticle = false;
		retVal.testParticleAmplitude = (double*) malloc(3*sizeof(double));
		retVal.testParticleAmplitude[0] = 0.0;
		retVal.testParticleAmplitude[1] = 12.0;
		retVal.testParticleAmplitude[1] = 12.0;
		retVal.testParticleAngle = (double*) malloc(3*sizeof(double));
		retVal.testParticleAngle[0] = 0.0;
		retVal.testParticleAngle[1] = 2.0*3.1415;
		retVal.testParticleAngle[2] = 60;
		retVal.dumpBunchCollisionSchedule = false;
		retVal.dumpBeamDistribution = false;
		retVal.dumpTransverseBeamDistribution = false;
		retVal.dumpSlicePos = false;
		retVal.dumpSlicePosNSlice = 0;
		retVal.circumference = 1.0;
		retVal.bunchLength = 1.0;
		retVal.bunchSpacing = 10.0;
		retVal.relativeMomentumSpread = 1E-4;
        retVal.abortIfUnstable = false;
        retVal.instabilityThreshold = 10;
        retVal.zBeta = (double*) malloc(2*sizeof(double));
		retVal.zBeta[0] = 0.55;
		retVal.zBeta[1] = 0.55;
		retVal.nWakeFiles = 0;
	    retVal.momentumCompaction = 0.0;
	    retVal.omegaRF = 0.0;
	    retVal.voltRF = 0.0;
	    retVal.averageBetaH = 0.0;
	    retVal.averageBetaV = 0.0;
	    retVal.syncRadTableFileName = NULL;
		retVal.mR=512;
		retVal.nTheta=512;

		char line[1000];
		while(fgets(line,sizeof(line),file) != NULL) {
			if(line[0] != '/' || line[1] != '/') {
				line[strlen(line)-1]='\0';
				char *id,*values;
				id = strtok (line,":");
				if (id != NULL) {
					values = strtok (NULL, ":");
					bool ok = getValue(&retVal,id,values);
					if(!ok) {
                        printf("ERROR : invalid values in input line : %s\n",line); //TODO forward message to caller
						retVal.valid = false;
						break;
					}
				} else {
                    printf("ERROR : invalid input line : %s\n",line); //TODO forward message to caller
					retVal.valid = false;
					break;
				}
			}
		}
		fclose(file);
	} else {
        printf("ERROR : Input file %s does not exist\n",inputFileName); //TODO forward message to caller
		retVal.valid = false;
	}
	return retVal;
}

bool getValue(ParserOutput* parserOutput,char* id,char* values) {
	bool retVal = true;	
	char *value = strtok(values," ");
	char* allValues[1000];
	int counter = 0;
	while(value != NULL) {
		allValues[counter] = value;
		value = strtok(NULL," ");
		++counter;
	}

	if(strcmp(id,"collision")==0) {
		parserOutput->collisionFile = (char*)malloc((strlen(allValues[0])+1)*sizeof(char));
		strcpy(parserOutput->collisionFile,allValues[0]);
	}else if(strcmp(id,"filling")==0) {
		parserOutput->fillingFileA = (char*)malloc((strlen(allValues[0])+1)*sizeof(char));
		strcpy(parserOutput->fillingFileA,allValues[0]);
		parserOutput->fillingFileB = (char*)malloc((strlen(allValues[1])+1)*sizeof(char));
		strcpy(parserOutput->fillingFileB,allValues[1]);
	}else if(strcmp(id,"number of turns")==0) {
		parserOutput->nturn = atof(allValues[0]);
	}else if(strcmp(id,"nb of macro particle")==0) {
		parserOutput->npart = atof(allValues[0]);
	}else if(strcmp(id,"output directory")==0) {
		parserOutput->outputDir = (char*)malloc((strlen(allValues[0])+1)*sizeof(char));
		strcpy(parserOutput->outputDir,allValues[0]);
	}else if(strcmp(id,"load beam from file")==0) {
		parserOutput->loadBeamFromFile = true;
		parserOutput->beamFileFormat = (char*) malloc((strlen(allValues[0])+1)*sizeof(char));
		strcpy(parserOutput->beamFileFormat,allValues[0]);
	}else if(strcmp(id,"first turn")==0) {
		parserOutput->firstTurn = atoi(allValues[0]);
	}else if(strcmp(id,"dump beam to file")==0) {
		parserOutput->dumpBeamToFile = true;
		parserOutput->dumpBeamFileFormat = (char*) malloc((strlen(allValues[0])+1)*sizeof(char));
		strcpy(parserOutput->dumpBeamFileFormat,allValues[0]);
		if(counter>1) {
			parserOutput->dumpBeamToFileFreq = atoi(allValues[1]);
		}else {
			parserOutput->dumpBeamToFileFreq = 1;
		}
        if(counter > 2){
            parserOutput->dumpBeamNPart = (int)atof(allValues[2]);
        }else {
            parserOutput->dumpBeamNPart = 0;
		}
	}else if(strcmp(id,"dump beam distribution")==0) {
		parserOutput->dumpBeamDistribution = true;
		parserOutput->beamDistributionCoordinate = atoi(allValues[0]);
		parserOutput->dumpBeamDistributionFileFormat = (char*) malloc((strlen(allValues[1])+1)*sizeof(char));
		strcpy(parserOutput->dumpBeamDistributionFileFormat,allValues[1]);
		parserOutput->dumpBeamDistributionFreq = atoi(allValues[2]);
	}else if(strcmp(id,"dump slices position")==0) {
		parserOutput->dumpSlicePos = true;
		parserOutput->dumpSlicePosFileFormat = (char*) malloc((strlen(allValues[0])+1)*sizeof(char));
		strcpy(parserOutput->dumpSlicePosFileFormat,allValues[0]);
		parserOutput->dumpSlicePosNSlice = atoi(allValues[1]);
		parserOutput->dumpSlicePosFreq = atoi(allValues[2]);
	}else if(strcmp(id,"dump transverse beam distribution")==0) {
		parserOutput->dumpTransverseBeamDistribution = true;
		parserOutput->dumpTransverseBeamDistributionFileFormat = (char*) malloc((strlen(allValues[0])+1)*sizeof(char));
		strcpy(parserOutput->dumpTransverseBeamDistributionFileFormat,allValues[0]);
		parserOutput->dumpTransverseBeamDistributionFreq = atoi(allValues[1]);
	}else if(strcmp(id,"walltime")==0) {
		parserOutput->wallTime = atof(allValues[0]); 
	}else if(strcmp(id,"dump beam parameter to file")==0) {
		free(parserOutput->dumpParameterFileFormat);
		parserOutput->dumpParameterFileFormat = (char*) malloc((strlen(allValues[0])+1)*sizeof(char));
		strcpy(parserOutput->dumpParameterFileFormat,allValues[0]);
	}else if(strcmp(id,"dump bunch collision schedule")==0) {
		parserOutput->dumpBunchCollisionSchedule= true;
		parserOutput->bunchCollisionScheduleFileFormat = (char*) malloc((strlen(allValues[0])+1)*sizeof(char));
		strcpy(parserOutput->bunchCollisionScheduleFileFormat,allValues[0]);
	}else if(strcmp(id,"bunch intensity")==0) {
		parserOutput->bunchIntensity[0] = atof(allValues[0]);
		if(counter>1) {
			parserOutput->bunchIntensity[1] = atof(allValues[1]);
		} else {
			parserOutput->bunchIntensity[1] = parserOutput->bunchIntensity[0]; 
		}
	}else if(strcmp(id,"kick")==0) {
		parserOutput->kick = atof(allValues[0]); 
	}else if(strcmp(id,"xfact")==0) {
		parserOutput->xfact = atof(allValues[0]); 
	}else if(strcmp(id,"emittance B1")==0) {
		parserOutput->emittanceB1[0] = atof(allValues[0])*1.0E-6;
		if(counter>1){
			parserOutput->emittanceB1[1] = atof(allValues[1])*1.0E-6;
			if(counter>2){
				parserOutput->emittanceB1[2] = atof(allValues[2]);
			}
		} else {
			parserOutput->emittanceB1[1] = parserOutput->emittanceB1[0];
		}
	}else if(strcmp(id,"emittance B2")==0) {
		parserOutput->emittanceB2[0] = atof(allValues[0])*1.0E-6;
		if(counter>1){
			parserOutput->emittanceB2[1] = atof(allValues[1])*1.0E-6;
			if(counter>2){
				parserOutput->emittanceB2[2] = atof(allValues[2]);
			}
		} else {
			parserOutput->emittanceB2[1] = parserOutput->emittanceB2[0];
		}
	}else if(strcmp(id,"beta*")==0) {
		parserOutput->betaStar[0] = atof(allValues[0]);
		if(counter>1) {
			parserOutput->betaStar[1] = atof(allValues[1]);
		} else {
			parserOutput->betaStar[1] = parserOutput->betaStar[0];
		}
	}else if(strcmp(id,"energy")==0) {
		parserOutput->energy[0] = atof(allValues[0]);
		if(counter>1) {
			parserOutput->energy[1] = atof(allValues[1]);
		} else {
			parserOutput->energy[1] = parserOutput->energy[0];
		} 
	}else if(strcmp(id,"mass")==0) {
		parserOutput->mass[0] = atof(allValues[0]); 
		if(counter>1) {
			parserOutput->mass[1] = atof(allValues[1]);
		} else {
			parserOutput->mass[1] = parserOutput->mass[0];
		} 
	}else if(strcmp(id,"charge")==0) {
		parserOutput->charge[0] = atof(allValues[0]); 
		if(counter>1) {
			parserOutput->charge[1] = atof(allValues[1]);
		} else {
			parserOutput->charge[1] = parserOutput->charge[0];
		}
	}else if(strcmp(id,"test particle file")==0) {
		parserOutput->testParticleFileFormat = (char*)malloc((strlen(allValues[0])+1)*sizeof(char));
		strcpy(parserOutput->testParticleFileFormat,allValues[0]);
		parserOutput->testParticle = true; 
	}else if(strcmp(id,"test particle amplitude")==0) {
		parserOutput->testParticleAmplitude[0] = atof(allValues[0]); 
		parserOutput->testParticleAmplitude[1] = atof(allValues[1]); 
		parserOutput->testParticleAmplitude[2] = atof(allValues[2]); 
	}else if(strcmp(id,"test particle angle")==0) {
		parserOutput->testParticleAngle[0] = atof(allValues[0]); 
		parserOutput->testParticleAngle[1] = atof(allValues[1]); 
		parserOutput->testParticleAngle[2] = atof(allValues[2]); 
	}else if(strcmp(id,"circumference")==0) {
		parserOutput->circumference = atof(allValues[0]);
	}else if(strcmp(id,"bunch spacing")==0) {
		parserOutput->bunchSpacing = atof(allValues[0]);
	}else if(strcmp(id,"bunch length")==0) {
		parserOutput->bunchLength = atof(allValues[0]);
	}else if(strcmp(id,"relative momentum spread")==0) {
		parserOutput->relativeMomentumSpread = atof(allValues[0]); 
	}else if(strcmp(id,"wake table files")==0) {
		parserOutput->nWakeFiles = counter;
		parserOutput->wakeFiles = (char**)malloc(counter*sizeof(char*));
		for(int k=0;k<counter;++k){
		    parserOutput->wakeFiles[k] = (char*) malloc((strlen(allValues[k])+1)*sizeof(char));
		    strcpy(parserOutput->wakeFiles[k],allValues[k]);
		}
	}else if(strcmp(id,"wake length")==0) {
		parserOutput->wakeLength = atoi(allValues[0]); 
	}else if(strcmp(id,"beta for impedance kick")==0) {
		parserOutput->zBeta[0] = atof(allValues[0]);
		if(counter>1) {
			parserOutput->zBeta[1] = atof(allValues[1]);
		} else {
			parserOutput->zBeta[1] = parserOutput->zBeta[0];
		}
	}else if(strcmp(id,"instability threshold")==0) {
        parserOutput->abortIfUnstable= true;
        parserOutput->instabilityThreshold = atof(allValues[0]);
    }else if(strcmp(id,"momentum compaction factor")==0) {
        parserOutput->momentumCompaction = atof(allValues[0]);
    }else if(strcmp(id,"omega RF")==0) {
        parserOutput->omegaRF = atof(allValues[0]);
    }else if(strcmp(id,"voltage RF")==0) {
        parserOutput->voltRF = atof(allValues[0]);
    }else if(strcmp(id,"average horizontal beta")==0) {
        parserOutput->averageBetaH = atof(allValues[0]);
    }else if(strcmp(id,"average vertical beta")==0) {
        parserOutput->averageBetaV = atof(allValues[0]);
    }else if(strcmp(id,"synchrotron radiation table")==0) {
		parserOutput->syncRadTableFileName = (char*)malloc((strlen(allValues[0])+1)*sizeof(char));
		strcpy(parserOutput->syncRadTableFileName,allValues[0]);
	}else if(strcmp(id,"FPPS radial grid size")==0) {
		parserOutput->mR = atof(allValues[0]); 
	}else if(strcmp(id,"FPPS angular grid size")==0) {
		parserOutput->nTheta = atof(allValues[0]); 
	}else {
	    printf("'%s' is not a valid input\n",id);
		retVal = false;
	}
	return retVal;
}

bool exists(char* fileName) {
    FILE* file = fopen(fileName,"rb");
    if(file == NULL) {
        return false;
    } else{
        fclose(file);
        return true;
    }
}

/* parse csv file
bool parseBeamFile(double* beam,int npart, char* fileName) {
	FILE* file = fopen(fileName,"rb");
	if(file == NULL) {
		printf("PARSING ERROR : No file %s\n",fileName);
		return false;
	}
	int lineNumber = 0;
	char line[1000];
	while(fgets(line,sizeof(line),file) != NULL) {
		if(lineNumber<npart) {
			int valueNumber = 0;
			char *value = strtok(line," ");
			while(value!=NULL && *value!='\n') {
				if(valueNumber<NCOORD) {
					beam[lineNumber*NCOORD+valueNumber] = strtod(value,NULL);
					value = strtok (NULL, " ");
				}
				++valueNumber;
			}
			if(valueNumber!=NCOORD) {
				printf("PARSING ERROR : Not right coord\n");
				fclose(file);
				return false;
			}
		}
		++lineNumber;
	}
	if(lineNumber != npart)	{
		printf("PARSING ERROR : not right nb of particles\n");
		fclose(file);
		return false;
	}
	fclose(file);
	return true;
}
*/

bool parseBeamFile(double* beam,int npart, char* fileName) {
	FILE* file = fopen(fileName,"rb");
	if(file == NULL) {
		printf("PARSING ERROR : No file %s\n",fileName);
		return false;
	}
	int size = fread(beam,sizeof(double),npart*NCOORD,file);
    fclose(file);
	if (size != npart*NCOORD) {
        printf("PARSING ERROR : not right nb of particles\n");
    } 
	return true;
}


bool parseCollisionFile(char* fileName,action* col,int npos) {
	FILE* file = fopen(fileName,"rb");
	if(file == NULL) {
		printf("PARSING ERROR : No file %s\n",fileName);
		return false;
	}

	for(int i = 0;i<npos;++i) {
		action act;
		act.actcode = 0;
		act.needsPartner = false;
		act.args = NULL;
		act.nargs = 0;
		col[i] = act;
	}

	char line[1000];
	while(fgets(line,sizeof(line),file) != NULL) {
		int valueNumber = 0;
		int index = -1;
		double args[100];
		int nargs = 0;
		char *value = strtok(line," ");
		while(value!=NULL && *value!='\n') {
			if(valueNumber == 0) {
				index = atoi(value);
			} else if(valueNumber == 1){
				col[index].actcode = atoi(value);
				if(col[index].actcode == 2 || 
						col[index].actcode == 10
                             || col[index].actcode==4) {
					col[index].needsPartner = true;
				}
			} else {
				args[nargs] = strtod(value,NULL);
				++nargs;
			}
			value = strtok (NULL, " ");
			++valueNumber;
		}
		if(nargs>0){
			col[index].nargs = nargs;
			col[index].args = (double*) malloc(nargs*sizeof(double));
			for(int i = 0;i<nargs;++i) {
				col[index].args[i] = args[i];
			}
		}
		if(index == -1) {
			printf("PARSING ERROR : syntax is not valid : %s\n",line);
			fclose(file);
			return false;
		}
		if(!testArgs(col[index])) {
			printf("PARSING ERROR : arguments incorrect for action code %i\n",col[index].actcode);
			fclose(file);
			return false;
		}
	}
	return true;
}

bool testArgs(action action) {
	if(action.actcode == 0) {
		if(action.nargs!=0) {
			return false;
		}
	}else if(action.actcode==2) {
		if(action.nargs!=3) {
			return false;
		}
	}else if(action.actcode==1) {
		if(action.nargs!=2) {
			return false;
		}
	}else if(action.actcode==3) {
		if(action.nargs!=10) {
			return false;
		}
	}else if(action.actcode==31) {
		if(action.nargs!=13) {
			return false;
		}
	}else if(action.actcode==4) {
		if(action.nargs!=1) {
			return false;
		}
	}else if(action.actcode==5) {
		if(action.nargs!=3) {
			return false;
		}
	}else if(action.actcode==6) {
		if(action.nargs!=2 && action.nargs!=3) {
			return false;
		}
	}else if(action.actcode==7) {
		if(action.nargs<1) {
			return false;
		} else if (action.args[0]==1){
			if(action.nargs!=3) {
				return false;
			}
		} else if (action.args[0]==2){
			if(action.nargs!=7) {
				return false;
			}
		} else if (action.args[0]==3){
			if(action.nargs!=3) {
				return false;
			}
		} else if (action.args[0]==4){
			if(action.nargs!=3) {
				return false;
			}
		} else if (action.args[0]==5){
			if(action.nargs!=3) {
				return false;
			}
		} else {
			return false;
		}
	}else if(action.actcode==8) {
	    if(action.nargs!=4) {
	        return false;
	    }
	}else if(action.actcode==10) {
		if(action.nargs!=2 && action.nargs!=3) {
			return false;
		}
		if(action.args[0]!=1 && action.args[0]!=2) {
			return false;
		}
	}else if(action.actcode==100) {
		if(action.nargs!=3) {
			return false;
		}
	}else if(action.actcode==11) {
		if(action.nargs!=6) {
			return false;
		}
	}else if(action.actcode==12) {
		if(action.nargs!=5) {
			return false;
		}
	}else if(action.actcode==13) {
	    if(action.nargs!=4) {
	        return false;
	    }
	}else if(action.actcode==14) {
	    if(action.nargs > 0) {
	        if(action.args[0] == 0) {
	            if(action.nargs!=5) {
	                return false;
                }
            } else if(action.args[0] == 1) {
                if(action.nargs!=9) {
                    return false;
                }
            } else if (action.args[0] == 2){
                if(action.nargs!=7){
                    return false;
                }
            }
            //add other damper type
            else {
                return false;
            }
	    } else {
	        return false;
	    }
	}else if(action.actcode==15) {
	    if(action.nargs > 0) {
	        if(action.args[0] == 1){
	            if(action.nargs!=3) {
	                return false;
	            }
	        } else if(action.args[0] == 2){
	            if(action.nargs!=5) {
	                return false;
	            }
	        } else if(action.args[0] == 3){
	            if(action.nargs!=4) {
	                return false;
	            }
	        } else {
	            return false;
	        }
	    } else {
	        return false;
	    }
	} else {
		return false;
	}
	return true;
}

bool parseWake(char* fileName,wake* wake) {
    return parseWake(fileName,wake,1.0,1.0);
}

bool parseWake(char* fileName,wake* wake,double bRatiox, double bRatioy) {
    double scaling = 1.60217646E-4; //table are provided in [V/pC*mm], this scales to [V/m]
	FILE* file = fopen(fileName,"rb");
	if(file == NULL) {
		printf("PARSING ERROR : No wake file %s\n",fileName);
		return false;
	}
	int nLine = 0;
	char line[1000];
	while(fgets(line,sizeof(line),file) != NULL) {
		++nLine;
    }
    fclose(file);
    file = fopen(fileName,"rb");

	wake->z = (double*) malloc(nLine*sizeof(double));
	wake->wxDip = (double*) malloc(nLine*sizeof(double));
	wake->wyDip = (double*) malloc(nLine*sizeof(double));
	wake->wxQuad = (double*) malloc(nLine*sizeof(double));
	wake->wyQuad = (double*) malloc(nLine*sizeof(double));

    int i = 0;
	while(fgets(line,sizeof(line),file) != NULL) {
		char *value = strtok(line," \t");
		int valueNumber = 0;
		while(value!=NULL && *value!='\n' && valueNumber < 5) {
		    if(valueNumber == 0) {
		        wake->z[i] = strtod(value,NULL);
		    }else if(valueNumber == 1) {
		        wake->wxDip[i] = strtod(value,NULL)*scaling*bRatiox;
		    }else if(valueNumber == 2) {
		        wake->wyDip[i] = strtod(value,NULL)*scaling*bRatioy;
		    }else if(valueNumber == 3) {
		        wake->wxQuad[i] = strtod(value,NULL)*scaling*bRatiox;
		    }else if(valueNumber == 4) {
		        wake->wyQuad[i] = strtod(value,NULL)*scaling*bRatioy;
		    }
		    value = strtok (NULL, " \t");
		    ++valueNumber;
		}
		if(valueNumber<5) {
		    printf("PARSING ERROR : not enought parameter for wake in file %s\n",fileName);
		    return false;
		}
        ++i;
    }
    wake->size = nLine;
	fclose(file);
    return true;
}

bool parseSynchrotronTable(char* fileName,double** table,int* tableSize) {
	FILE* file = fopen(fileName,"rb");
	if(file == NULL) {
		printf("PARSING ERROR : No synchrotron file %s\n",fileName);
		return false;
	}
	*tableSize = 0.0;
	char line[1000];
	while(fgets(line,sizeof(line),file) != NULL) {
		++(*tableSize);
    }
    fclose(file);
    *table = (double*) malloc((*tableSize)*2*sizeof(double));
    file = fopen(fileName,"rb");
    int i = 0;
	while(fgets(line,sizeof(line),file) != NULL) {
		char *value = strtok(line," \t");
		int valueNumber = 0;
		while(value!=NULL && *value!='\n' && valueNumber < 2) {
		    if(valueNumber == 0) {
		        (*table)[i] = strtod(value,NULL);
		    }else if(valueNumber == 1) {
		        (*table)[(*tableSize)+i] = strtod(value,NULL);
		    }
		    value = strtok (NULL, " \t");
		    ++valueNumber;
		}
		if(valueNumber<2) {
		    printf("PARSING ERROR : not enought parameter in synchrotron file %s\n",fileName);
		    fclose(file);
		    return false;
		}
        ++i;
    }
    fclose(file);
    return true;
}

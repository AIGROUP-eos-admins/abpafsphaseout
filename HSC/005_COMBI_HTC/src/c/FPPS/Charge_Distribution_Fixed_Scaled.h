#ifndef CHARGEDISTRIBUTIONFIXEDSCALED
#define CHARGEDISTRIBUTIONFIXEDSCALED

#include "Charge_Distribution_Fixed.h"
#include "ChangeCoord.h"

class Charge_Distribution_Fixed_Scaled : public Charge_Distribution_Fixed{
 public:

  Charge_Distribution_Fixed_Scaled(const Mesh& mesh, const double& a,const double& d=1.0);
  Charge_Distribution_Fixed_Scaled(const int& n_theta, const int& m_r, const double& d=1.0);
	Charge_Distribution_Fixed_Scaled(const Charge_Distribution_Fixed_Scaled& d);
	virtual ~Charge_Distribution_Fixed_Scaled();

	
	void addParticle(const double& x,const double& y, const double& charge);

	virtual Charge_Distribution_Fixed_Scaled* copy() const;

	virtual void FillRhoConst(const double& ConstChargeDensity, const double& rmax);
	virtual void FillRhoGaussian(const double& totalCharge, const double& sigma);
	virtual void FillRhoBiGaussian(const double& totalCharge, const double& sigmax, const double& sigmay);

	void setChangeCoord(const ChangeCoord& g);
	virtual void normaliseRho();

	virtual double computeEr(const unsigned int& r, const unsigned int& theta) const;

	virtual double computeEthetaFD(const unsigned int& r, const unsigned int& theta) const;


 protected:
	ChangeCoord* f;

};


#endif

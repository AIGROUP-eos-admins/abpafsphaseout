#include "Charge_Distribution_Fixed.h"
#include <math.h>
#include <iostream>
#include "functionsFPS.h"

Charge_Distribution_Fixed::Charge_Distribution_Fixed(const Mesh& mesh,const double& d):
  Charge_Distribution(mesh),
  depth(d)
{

}

Charge_Distribution_Fixed::Charge_Distribution_Fixed(const Charge_Distribution_Fixed& d):
  Charge_Distribution(*d.mesh),
  depth(d.depth)
{
}

Charge_Distribution_Fixed::Charge_Distribution_Fixed(const int& n_theta, const int& m_r, const double& r_max, const double& d):
  Charge_Distribution(n_theta,m_r,r_max),
  depth(d)
{

}

void Charge_Distribution_Fixed::addParticle(const double& x, const double& y, const double& charge){

  double r=computeR(x,y);
  double theta=computeTheta(x,y);
  unsigned int r_ind=mesh->which_R(r);
  unsigned int theta_ind=mesh->which_Theta(theta);

  double dr=mesh->getDeltaR();
  double dtheta=mesh->getDeltaTheta();
  
  double r_i=mesh->getR(r_ind+1);
  double r_iplus1=r_i+dr;
  double theta_i=mesh->getTheta(theta_ind);

  double norm=dtheta*(sq2(r_iplus1)-sq2(r_i))/2.;

  //std::cout<<norm<<std::endl;
    
  if(r_ind<mesh->getM_r() && theta_ind<mesh->getN_theta()){ 

    double a=theta-theta_i;
   
   
    //double d_i_i=cosineTheorem(r_i,r,theta-theta_i);
    //double d_i_i_plus1=cosineTheorem(r_i,r,theta_iplus1-theta);
    //double d_i_plus1_i=cosineTheorem(r_iplus1,r,theta-theta_i);
    //double d_i_plus1_i_plus1=cosineTheorem(r_iplus1,r,theta_iplus1-theta);


    //std::cout<<r_ind<<" "<<theta_ind<<std::endl;
    
    Func[r_ind][theta_ind]+=(dtheta-a)*(sq2(r_iplus1)-sq2(r))*charge/norm/2.;
    if(theta_ind+1<mesh->getN_theta()) Func[r_ind][theta_ind+1]+=(a)*(sq2(r_iplus1)-sq2(r))/2./norm*charge;
    else Func[r_ind][(theta_ind+1)%mesh->getN_theta()]+=(a)*(sq2(r_iplus1)-sq2(r))/2./norm*charge;
    if(r_ind+1<mesh->getM_r()) Func[r_ind+1][theta_ind]+=(sq2(r)-sq2(r_i))*(dtheta-a)/2./norm*charge;
    if(r_ind+1<mesh->getM_r() && theta_ind+1<mesh->getN_theta()) Func[r_ind+1][theta_ind+1]+=(sq2(r)-sq2(r_i))*(a)/2./norm*charge;
    else if(r_ind+1<mesh->getM_r() && theta_ind+1>=mesh->getN_theta()) Func[r_ind+1][(theta_ind+1)%mesh->getN_theta()]+=(sq2(r)-sq2(r_i))*(a)/2./norm*charge;

    //Func[r_ind][theta_ind]+=charge;

  }
  else Func[mesh->getM_r()-1][mesh->getN_theta()-1]+=charge;
  }


void Charge_Distribution_Fixed::setDeltaz(const double& z)
{
  depth=z;
}

Charge_Distribution_Fixed* Charge_Distribution_Fixed::copy() const
{
  return new Charge_Distribution_Fixed(*this);
}

void Charge_Distribution_Fixed::FillRhoConst(const double& ConstChargeDensity, const double& rmax)
{

	//	std::cout<<"theta="<<computeTheta(particles[2*i+nx],particles[2*i+ny])<<std::endl;
	//	std::cout<<"r="<<computeR(particles[2*i+nx],particles[2*i+ny])<<std::endl;
	for(size_t i=0;i<mesh->getM_r();++i){
		for(size_t j=0; j<mesh->getN_theta(); ++j)
		{
			if(mesh->getR(i+1)<=rmax) Func[i][j]=ConstChargeDensity/(M_PI*rmax);
			else Func[i][j]=0.0;
		}
	}
	//normaliseRho();
}

void Charge_Distribution_Fixed::FillRhoGaussian(const double& totalCharge, const double& sigma)
{
	double r=0.0;
	for(size_t i=0;i<mesh->getM_r();++i){
		for(size_t j=0; j<mesh->getN_theta(); ++j)
		{
			r=mesh->getR(i+1);
			Func[i][j]=totalCharge/(2.0*M_PI*sigma*sigma)*exp(-(r*r)/(2.0*sigma*sigma));
		}
	}
	//normaliseRho();
}

void Charge_Distribution_Fixed::FillRhoBiGaussian(const double& totalCharge, const double& sigmax, const double& sigmay)
{
	double r=0.0;
	double theta=0.0;
	double x=0.0;
	double y=0.0;
	for(size_t i=0;i<mesh->getM_r();++i){
		for(size_t j=0; j<mesh->getN_theta(); ++j)
		{
			r=mesh->getR(i+1);
			theta=mesh->getTheta(j);
			x=r*cos(theta);
			y=r*sin(theta);
			
			Func[i][j]=totalCharge/(2.0*M_PI*sigmax*sigmay)*exp(-(x*x)/(2.0*sigmax*sigmax))*exp(-(y*y)/(2.0*sigmay*sigmay));
		}
	}
	//normaliseRho();
}

void Charge_Distribution_Fixed::FillRhoFunction1(double rmax)
{
	double r=0.0;
	double theta=0.0;
	for(size_t i=0;i<mesh->getM_r();++i){
		for(size_t j=0; j<mesh->getN_theta(); ++j)
		{
			r=mesh->getR(i+1);
			theta=mesh->getTheta(j);
			Func[i][j]=-((1.0/r)*cos(theta)*cos(theta)+(2.0*(r-rmax))/(r*r)*(sin(theta)*sin(theta)-cos(theta)*cos(theta)));
		}
	}

}

void Charge_Distribution_Fixed::FillRhoFunction2(double rmax)
{
	double r=0.0;
	double theta=0.0;
	double sintheta=0.0;
	double costheta=0.0;
	double sinrcostheta=0.0;
	double cosrcostheta=0.0;


	for(size_t i=0;i<mesh->getM_r();++i){
		for(size_t j=0; j<mesh->getN_theta(); ++j)
		{
			r=mesh->getR(i+1);
			theta=mesh->getTheta(j);

			sintheta=sin(theta);
			costheta=cos(theta);

			sinrcostheta=sin((r-rmax)*costheta);
			cosrcostheta=cos((r-rmax)*costheta);


			Func[i][j]=sinrcostheta -2.0*(rmax/r)*pow(sintheta,2)*sinrcostheta + (rmax*rmax)/(r*r)*pow(sintheta,2)*sinrcostheta -(rmax)/(r*r)*costheta*cosrcostheta;
		}
	}
}

void Charge_Distribution_Fixed::normaliseRho(){

  double dr=mesh->getDeltaR();
int M= mesh->getM_r();
int N=mesh->getN_theta();
#pragma omp parallel for firstprivate(M,N,dr) schedule(guided,10)
	for(int i=0;i<M;++i){
		for(int j=0; j<N; ++j)
		{
		  if(i!=M-1) Func[i][j]/=((sq2(mesh->getR(i+1)+dr/2.)-sq2(mesh->getR(i+1)-dr/2.))*mesh->getDeltaTheta()/2.);//((i+1)*mesh.getDeltaR()*mesh.getDeltaR()*mesh.getDeltaTheta()); //divide by the volume of the cell.
		  else Func[i][j]/=((sq2(mesh->getR(i+1)+dr/2.)-sq2(mesh->getR(i+1)-dr/2.))*mesh->getDeltaTheta()/2.);
			//std::cout<<Func[i][j]<<std::endl;
		}
	}
}


double Charge_Distribution_Fixed::getCharge(const int& theta, const int& r) const
{
  return ((sq2(mesh->getR(r+1)+mesh->getDeltaR()/2.)-sq2(mesh->getR(r+1)-mesh->getDeltaR()/2.))*mesh->getDeltaTheta()/2.)*Func[theta][r];
}

double Charge_Distribution_Fixed::checkSum() const
{
	double sum=0;
	for(size_t i=0;i<mesh->getM_r();++i){
		for(size_t j=0; j<mesh->getN_theta(); ++j)
		{
			sum+=Func[i][j];
		}
	}

	return sum;
}

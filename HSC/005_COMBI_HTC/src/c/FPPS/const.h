#define NPARAM 16
#define NCOORD 7
#define NSLOT 3564
#define NPOSMAX 2*NSLOT

#define PROGRESSMETER 1
#define VERSION "2.0"

#define RNDSEED 0

#define ABORTACTION 84543

#define GAUSS

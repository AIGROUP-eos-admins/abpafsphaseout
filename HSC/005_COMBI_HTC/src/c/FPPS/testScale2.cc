#include "FastPolarPoissonSolver_FD2_Scaled.h"
#include <random>
#include <stdlib.h> 
#include <fstream>
#include <iostream>
#include <sys/time.h>
#include <sys/resource.h>
#include "Charge_Distribution_Fixed_Scaled.h"
#include "Scalar_Potential_Scaled.h"
#include "MeshScaled.h"
#include "ChangeCoord_Frac.h"
#include "ChangeCoord_Tanh.h"


using namespace std;

int main(int argc, char* argv[]){

  timeval Start;
  timeval End;
  int M(0);
  int N=0;
  if(argc>1)  M=pow(2,atoi(argv[1]));
  else M=256;

	M=600;
	N=100;


  int compteurint=1;
  compteurint=atoi(argv[2]);
  std::string compteurstr="1";
  if(argc>2) compteurstr=argv[2];
 
  double rmax=1.;
  Mesh* me;
  me = new Mesh(M,N,rmax);

  unsigned int npart=1000000;
  if(argc>3) npart=atoi(argv[3]);

  std::random_device rd;
  unsigned int seed(rd());
  std::default_random_engine generator(seed);
  
  double min_r(0.0);
  double sigma(0.2);
  double min_t(0.0);
  double max_t(2*M_PI);
  std::normal_distribution<double> dist_x(0.0, sigma);
  std::normal_distribution<double> dist_y(0.0, sigma);
  std::uniform_real_distribution<double> dist_theta(0.0,M_PI); 

  std::ofstream* out;
  out=new std::ofstream;
  std::string filename1="randomdist.txt";
  out->open(filename1.c_str());

  double* part = (double*) malloc(sizeof(double)*2*npart);
  double x_,y_,theta;
  int compteur=0;
  
  for(int i=0; i<npart; ++i)
    {
      x_=dist_x(generator);
      y_=dist_y(generator);
      if(x_*x_+y_*y_<=rmax*rmax) compteur+=1;
      theta=dist_theta(generator);
      part[2*i]=x_;//x_*cos(theta);
      part[2*i+1]=y_;//x_*sin(theta);      
    }
//Test of the finite element solver

    FastPolarPoissonSolver_FD2_Scaled* p_;

    double a=0.5;


    Scalar_Potential_Scaled pot(M,N+1);
    
    Function_on_a_mesh* Er ;
    Function_on_a_mesh* Etheta ;
    Charge_Distribution_Fixed_Scaled* chd;

    
    Electric_Field_Solver* El;

    ChangeCoord_Frac g(a);


    p_=new FastPolarPoissonSolver_FD2_Scaled(M,N,rmax);
    p_->setChangeCoord(g);

    Mesh msc(M,N,1.0);
    p_->setMesh(msc);

    chd=new Charge_Distribution_Fixed_Scaled(M,N,rmax);
    chd->setMesh(msc);
    chd->setChangeCoord(g);


    //chd->FillRhoGaussian(1.0,0.2);

    chd->FillRho(part,npart,0,1,1./npart,2);

    //chd->printFunc();

    pot.setMesh(msc);

    pot.setChangeCoord(g);
    
    gettimeofday(&Start,NULL);


    p_->solvePoisson(*chd,pot);

    El = new Electric_Field_Solver(pot.getE(true));  
    Er=new Function_on_a_mesh(El->getEr());
    Etheta =new Function_on_a_mesh(El->getEtheta());
    gettimeofday(&End,NULL);


     double Duration = End.tv_sec-Start.tv_sec+(End.tv_usec-Start.tv_usec)/1E6;

    cout<<M<<" "<<npart<<" "<<compteurint<<" "<<Duration<<endl;


    /*//pot.printOutFunc("/Users/adrienflorio/Documents/EPFL/F2M2/OpenMPcode/testAlgorithm/pot_scaled_nm.txt");
    if(compteurint==1 && npart==100000) msc.printOutR("/Users/adrienflorio/Documents/EPFL/F2M2/FastPolarPoissonSolver/analyse_noise/noise/r_"+std::to_string(M)+".txt");
    std::cout<<npart<<std::endl;
    Er->printOutFunc("/Users/adrienflorio/Documents/EPFL/F2M2/FastPolarPoissonSolver/analyse_noise/noise/Er_dist_N"+std::to_string(M)+"_npart_"+std::to_string(npart)+"_"+compteurstr+".txt");
    //Etheta->printOutFunc("/Users/adrienflorio/Documents/EPFL/F2M2/OpenMPcode/testAlgorithm/FPS/Et_"+std::to_string(M)+".txt");*/
    
   delete me;
   delete El;
   delete Er;
   delete Etheta;
   delete p_;
   delete chd;

   free(part);
   
   return 0;
}

#include "damper.h"
#include "const.h"
#include <stdio.h>
#include <cmath>

#include <stdlib.h>

void perfectDamperKick(double* beam, int npart, double* bparam, double Hgain, double Vgain){
    double xkick = -bparam[4]*Hgain;
    double ykick = -bparam[6]*Vgain;
    //printf("PERFECT DAMPER KICK : gain (%E,%E) kicks (%E,%E) \n",Hgain,Vgain,xkick,ykick);
    //no gain from parallelization
    //#pragma omp parallel for schedule(guided,10000) default(none) firstprivate(xkick,ykick,npart) shared(beam)
    for(int i = 0;i<npart;++i) {
        beam[i*NCOORD + 1] += xkick;
        beam[i*NCOORD + 3] += ykick;
    }
}

//TODO normalize to rms
void thresholdedDamperKick(double* beam, int npart, double* bparam, double Hgain, double Vgain,double Hthreshold,double Vthreshold){
    double xkick = (fabs(bparam[0]) > Hthreshold) ? -bparam[4]*Hgain : 0.0;
    double ykick = (fabs(bparam[2]) > Vthreshold) ? -bparam[6]*Vgain : 0.0;
    //printf("PERFECT DAMPER KICK : gain (%E,%E) kicks (%E,%E) \n",Hgain,Vgain,xkick,ykick);
    //no gain from parallelization
    //#pragma omp parallel for schedule(guided,10000) default(none) firstprivate(xkick,ykick,npart) shared(beam)
    for(int i = 0;i<npart;++i) {
        beam[i*NCOORD + 1] += xkick;
        beam[i*NCOORD + 3] += ykick;
    }
}

// NOTE : this assumes equidistant slicing
void singleTurnADT(double* beam,int npart,double* sliceMoments,int nSlice,double omega,double Hgain, double Vgain) {
    double Isum = 0.0;
    double Qsum = 0.0;
    double Idiff_X = 0.0;
    double Qdiff_X = 0.0;
    double Idiff_Y = 0.0;
    double Qdiff_Y = 0.0; 
    
    for(int iSlice=0;iSlice<nSlice;++iSlice) {
        double c = cos(omega*sliceMoments[2*nSlice + iSlice]*1E-9);
        double s = sin(omega*sliceMoments[2*nSlice + iSlice]*1E-9);
        Isum += c*sliceMoments[3*nSlice + iSlice];
        Qsum += s*sliceMoments[3*nSlice + iSlice];
        Idiff_X += c*sliceMoments[iSlice]*sliceMoments[3*nSlice + iSlice];
        Qdiff_X += s*sliceMoments[iSlice]*sliceMoments[3*nSlice + iSlice];
        Idiff_Y += c*sliceMoments[nSlice + iSlice]*sliceMoments[3*nSlice + iSlice];
        Qdiff_Y += s*sliceMoments[nSlice + iSlice]*sliceMoments[3*nSlice + iSlice];
    }
    
    double norm = Isum*Isum+Qsum*Qsum;
    double xkick = -Hgain*(Idiff_X*Isum+Qdiff_X*Qsum)/norm;
    double ykick = -Vgain*(Idiff_Y*Isum+Qdiff_Y*Qsum)/norm;

    for(int i = 0;i<npart;++i) {
        beam[i*NCOORD] += xkick;
        beam[i*NCOORD + 2] += ykick;
    }
}

// NOTE : this assumes equidistant slicing
void singleTurnADT_debug(double* beam,int npart,double* sliceMoments,int nSlice,double omega,double Hgain, double Vgain,double* bparam,int ibeam) {
    double Isum = 0.0;
    double Qsum = 0.0;
    double Idiff_X = 0.0;
    double Qdiff_X = 0.0;
    double Idiff_Y = 0.0;
    double Qdiff_Y = 0.0; 
    
    for(int iSlice=0;iSlice<nSlice;++iSlice) {
        double c = cos(omega*sliceMoments[2*nSlice + iSlice]*1E-9);
        double s = sin(omega*sliceMoments[2*nSlice + iSlice]*1E-9);
        Isum += c*sliceMoments[3*nSlice + iSlice];
        Qsum += s*sliceMoments[3*nSlice + iSlice];
        Idiff_X += c*sliceMoments[iSlice]*sliceMoments[3*nSlice + iSlice];
        Qdiff_X += s*sliceMoments[iSlice]*sliceMoments[3*nSlice + iSlice];
        Idiff_Y += c*sliceMoments[nSlice + iSlice]*sliceMoments[3*nSlice + iSlice];
        Qdiff_Y += s*sliceMoments[nSlice + iSlice]*sliceMoments[3*nSlice + iSlice];
    }
    
    double norm = Isum*Isum+Qsum*Qsum;
    double xkick = -Hgain*(Idiff_X*Isum+Qdiff_X*Qsum)/norm;
    double ykick = -Vgain*(Idiff_Y*Isum+Qdiff_Y*Qsum)/norm;

    if(ibeam == 1) {
	    FILE* file = fopen("ADT_HB1.csv","a+");
	    fprintf(file,"%.20E,%.20E\n",xkick,-Hgain*bparam[0]);
	    fclose(file);
	    file = fopen("ADT_VB1.csv","a+");
	    fprintf(file,"%.20E,%.20E\n",ykick,-Hgain*bparam[3]);
	    fclose(file);
    } else if (ibeam == 2) {
	    FILE* file = fopen("ADT_HB2.csv","a+");
	    fprintf(file,"%.20E,%.20E\n",xkick,-Hgain*bparam[0]);
	    fclose(file);
	    file = fopen("ADT_VB2.csv","a+");
	    fprintf(file,"%.20E,%.20E\n",ykick,-Hgain*bparam[3]);
	    fclose(file);
    }

    for(int i = 0;i<npart;++i) {
        beam[i*NCOORD] += xkick;
        beam[i*NCOORD + 2] += ykick;
    }
}

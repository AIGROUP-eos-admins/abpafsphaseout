#!/usr/bin/bash

# Pass working directory as argument
if [ -z "$1" ]
  then
    echo "No test directory supplied."
    exit 1
fi
PATHTEST=$1/HSC/afs_phaseout_temptests_bimbim

# Get dir where this test is located
TESTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Setup and run test (testA_run.sh)
MYPATH=$(pwd)
set -e
mkdir -p $PATHTEST
cp -r $TESTDIR/* $PATHTEST/
cd $PATHTEST
source /cvmfs/sft.cern.ch/lcg/views/LCG_93python3/x86_64-centos7-gcc7-opt/setup.sh
export PYTHONPATH=$PYTHONPATH:$PATHTEST/BimBim
python3 $PATHTEST/BimBimTest.py $PATHTEST BimBimTest 11 4 10.0 0.01 15.0

# Setup and check results (testB_check.sh)
source /cvmfs/sft.cern.ch/lcg/views/LCG_93python3/x86_64-centos7-gcc7-opt/setup.sh
$PATHTEST/check.py $PATHTEST
rm -rf $PATHTEST
cd $MYPATH

# AFS Phaseout tests for the ABP-HSC section

This folder contains tests for the ABP-HSC section. 

In each folder there is a script called ```testA_Run.sh``` that launches the tests.
In some cases there is a second script caller ```testB_Check.sh``` to check the outcome of the test.

In order to run the test scripts, the working directory for the test should be specified as an argument to script, such as "testA_Run.sh /path/to/working/directory".
The path specified in ```testA_Run.sh``` and ```testB_Check.sh``` should be the same.

Test 003 still has a variable called PATHTEST (work in progress) which sets the path in which the test will be executed (due to issues while running the test).

The folders with names ending with HTC contain tests that launch jobs on HTCondor.
In those cases it is necessary to verify that all HTCondor jobs are completed (with condor_q) before launching ```testB_Check.sh```.

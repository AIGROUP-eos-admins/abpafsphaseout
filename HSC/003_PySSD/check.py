#!/usr/bin/env python3

import os, sys
import numpy as np

pathTest = sys.argv[1]
data = np.loadtxt(os.path.join(pathTest,'400beta_1.5sep_500.0Ioct_-180crab/H_6.0sigma.sdiag'),delimiter=',')

if np.shape(data)!=(12,2) or np.any(np.isnan(data)) or np.any(np.isinf(data)):
    print('failed')
else:
    print('ok')

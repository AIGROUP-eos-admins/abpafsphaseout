#!/usr/bin/bash

if [ -z "$1" ]
  then
    echo "No test directory supplied."
    exit 1
fi
PATHTEST=$1/HSC/afs_phaseout_temptests_pyssd

# madx is not in the default path - /cvmfs?
MADX=$(type -p madx) ||:
MADX=${MADX:-/afs/cern.ch/project/mad/madx/releases/last-rel/madx-linux64-gnu}

# change workdir to desired location
MYPATH=$(pwd)
set -e
mkdir -p $PATHTEST
cp -r ./400beta_1.5sep_500.0Ioct_-180crab $PATHTEST/
cd $PATHTEST/400beta_1.5sep_500.0Ioct_-180crab

$MADX < footprint.mad > output

cd $MYPATH
source /cvmfs/sft.cern.ch/lcg/views/LCG_93python3/x86_64-centos7-gcc7-opt/setup.sh
export PYTHONPATH=$PYTHONPATH:$MYPATH/PySSD
python3 PySSD_HTC.py $PATHTEST/400beta_1.5sep_500.0Ioct_-180crab/ H


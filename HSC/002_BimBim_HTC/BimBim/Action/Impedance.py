import scipy.sparse as spm
from time import time
import numpy as np
import os
import pickle

from BimBim.Action import Action
from ..Error import BimBimError
from ..Matrix import printMatrix,sparse_insert,sparse_add

def binarySearch(array,value,upper,lower=0,nStep=0):
    pivot = int((lower+upper)/2);
    if (upper-lower)<=1:
        #print nStep;
        return lower;
    if value < array[pivot]:
        upper = pivot;
        nStep+=1;
        return binarySearch(array,value,upper,lower,nStep);
    elif value > array[pivot]:
        lower = pivot;
        nStep+=1;
        return binarySearch(array,value,upper,lower,nStep);
    else:
        #print nStep;
        return pivot;

class Impedance(Action.Action):
    _wakeTableT = None;
    _wakeTable = None;
    _betaX = 0.0; # beta ratio
    _betaX = 0.0;
    _intensityScaling = 1.0;

    _quadWake = False;

    _wakeUnitConversion = 1E15;
    _clight = 2.99792458e8;
    _GeVToKg = 1.78266113385e-27;
    _qe = 1.60217733e-19;

    _keepInMemory = True;
    _singleBunchMatrices = None;

    # deactivate keepInMemory if all bunches are different
    def __init__(self,wakeFileName,betaX,betaY,quadWake=False,keepInMemory = True,pickleFileName=None,intensityScaling = 1.0):
        self._betaX = betaX;
        self._betaY = betaY;
        self._quadWake = quadWake;
        self.parseWakeTable(wakeFileName);
        self._keepInMemory = keepInMemory;
        self._intensityScaling = intensityScaling;
        if pickleFileName == None:
            self._singleBunchMatrices = {};
        elif not os.path.exists(pickleFileName):
            print('Pickle does not exist',pickleFileName);
            self._singleBunchMatrices = {};
        else:
            print('Unpickling impedance matrix')
            pickleFile = open(pickleFileName,'rb');
            denseSingleBunchMatrices = pickle.load(pickleFile);
            pickleFile.close();
            for key in denseSingleBunchMatrices.keys():
                self._singleBunchMatrices[key] = spm.dok_matrix(denseSingleBunchMatrices[key])
        
    def pickleMatrices(self,fileName):
        print('Pickling impedance matrix')
        pickleFile = open(fileName,'wb');
        denseSingleMatrices = {}
        for key in self._singleBunchMatrices.keys():
            denseSingleBunchMatrices[key] = self._singleBunchMatrices[key].todense()
        pickle.dump(denseSingleBunchMatrices,pickleFile);
        pickleFile.close();

    def parseWakeTable(self,filename):
        wakefile = open(filename,'r')
        lines = wakefile.readlines()
        row = len(lines)
        col = len(lines[0].split())
        self._wakeTableT = [0.0 for i in range(row)];
        self._wakeTable = [[0.0 for i in range(col-1)] for j in range(row)]
        for i in range(row):
            thisline = lines[i].split()
            self._wakeTableT[i] = float(thisline[0])
            for j in range(col-1):
                self._wakeTable[i][j]=float(thisline[j+1])*self._wakeUnitConversion;
        wakefile.close();

    #Generate map of indexes in impedance table
    #def getIndexMap(self,basis):
    #    indmat = [[0 for j in range(basis.getNSlice()*basis.getNRing())] for i in range(basis.getNSlice()*basis.getNRing())]
    #    for i in range(basis.getNSlice()*basis.getNRing()):
    #        for j in range(basis.getNSlice()*basis.getNRing()):
    #            if distmap[i][j]>0.0:
	#                index = getTabindex(distmap[i][j])
    #            else:
	#                index = 0
    #            indmat[i][j]=index
    #    return indmat

    #get the wake for a given position
    #def getWakeTab(pos,ipos):
    #    dipx = self._wakeTable[ipos-1][1]+(self._wakeTable[ipos][1]-self._wakeTable[ipos-1][1])*(abs(pos)-self._wakeTable[ipos-1][0])/(self._wakeTable[ipos][0]-self._wakeTable[ipos-1][0])
    #    dipy = self._wakeTable[ipos-1][2]+(self._wakeTable[ipos][2]-self._wakeTable[ipos-1][2])*(abs(pos)-self._wakeTable[ipos-1][0])/(self._wakeTable[ipos][0]-self._wakeTable[ipos-1][0])
    #    quadx = self._wakeTable[ipos-1][3]+(self._wakeTable[ipos][3]-self._wakeTable[ipos-1][3])*(abs(pos)-self._wakeTable[ipos-1][0])/(self._wakeTable[ipos][0]-self._wakeTable[ipos-1][0])
    #    quady = self._wakeTable[ipos-1][4]+(self._wakeTable[ipos][4]-self._wakeTable[ipos-1][4])*(abs(pos)-self._wakeTable[ipos-1][0])/(self._wakeTable[ipos][0]-self._wakeTable[ipos-1][0])
    #    return dipx,dipy,quadx,quady

    def interpolateWake(self,distance,wakeIndex,index):
        return self._wakeTable[index][wakeIndex] + (distance-self._wakeTableT[index])*(self._wakeTable[index+1][wakeIndex]-self._wakeTable[index][wakeIndex])/(self._wakeTableT[index+1]-self._wakeTableT[index])

    # distance in ns
    def getWake(self,distance):
        dipx = 0.0;
        dipy = 0.0;
        quadx = 0.0;
        quady = 0.0;
        i = binarySearch(self._wakeTableT,distance,len(self._wakeTableT));
        dipx = self.interpolateWake(distance,0,i);
        dipy = self.interpolateWake(distance,1,i);
        if len(self._wakeTable[0]) > 2:
            quadx = self.interpolateWake(distance,2,i);
            quady = self.interpolateWake(distance,3,i);
        return dipx,dipy,quadx,quady;
    
    def getSingleBunchImpedanceMatrix(self,basis,bunch):
        cst = self._qe**2/(bunch.getMass()*self._GeVToKg*bunch.getGamma()*bunch.getBeta()**2*self._clight**2)*bunch.getIntensity();
        sfact = self._clight/(bunch.getBeta()*1.0e9);
        key = (basis.getNSlice(),basis.getNRing(),basis.getNDim(),cst,sfact,bunch.getSigS());
        if key in self._singleBunchMatrices.keys():
            return self._singleBunchMatrices[key];
        else:
            zMatrix = spm.identity(basis.getBunchBlockSize(),format='dok');
            for iSliceSource in range(basis.getNSlice()):
                for iSliceTest in range(basis.getNSlice()):
                    for iRingSource in range(basis.getNRing()):
                        for iRingTest in range(basis.getNRing()):
                            distance = basis.getSDiff(iSliceTest,iRingTest,iSliceSource,iRingSource,bunch.getSigS())/sfact;
                            if distance > 0.0:
                                dipx,dipy,quadx,quady = self.getWake(distance);
                                weight = basis.getWeight(iSliceSource,iRingSource);
                                if basis.getNDim() == 2:
                                    basisIndexForTest = basis.getIndexForSlice(iSliceTest,iRingTest);
                                    zMatrix[basisIndexForTest+1,basis.getIndexForSlice(iSliceSource,iRingSource)] = dipx*cst*self._betaX*weight;
                                    if self._quadWake:
                                        zMatrix[basisIndexForTest+1,basisIndexForTest] += quadx*cst*self._betaX*weight*self._intensityScaling;
                                elif basis.getNDim() == 4:
                                    basisIndexForTest = basis.getIndexForSlice(iSliceTest,iRingTest);
                                    zMatrix[basisIndexForTest+1,basis.getIndexForSlice(iSliceSource,iRingSource)] = dipx*cst*self._betaX*weight;
                                    zMatrix[basisIndexForTest+3,basis.getIndexForSlice(iSliceSource,iRingSource)+2] = dipy*cst*self._betaY*weight;
                                    if self._quadWake:
                                        zMatrix[basisIndexForTest+1,basisIndexForTest] = quadx*cst*self._betaX*weight;
                                        zMatrix[basisIndexForTest+3,basisIndexForTest+2] = quady*cst*self._betaY*weight;
                                else:
                                    raise BimBimError('Impedance is not implemented in '+str(basis.getNDim())+' dimensions');
            self._singleBunchMatrices[key] = zMatrix;
            return zMatrix;
    
    def getCouplingMatrix(self,basis,beam,sourceBunch,testBunch):
        retVal = spm.dok_matrix((basis.getBunchBlockSize(),basis.getBeamBlockSize(beam)));
        sourceBunchIndex = basis.getIndexForBunch(sourceBunch.getNumber());
        testBunchIndex = basis.getIndexForBunch(testBunch.getNumber());
        distance = testBunch.getSPosition()-sourceBunch.getSPosition();
        dipx,dipy,quadx,quady = self.getWake(distance);
        cst = self._qe**2/(sourceBunch.getMass()*self._GeVToKg*sourceBunch.getGamma()*sourceBunch.getBeta()**2*self._clight**2)*sourceBunch.getIntensity()*self._intensityScaling;
        for iSliceSource in range(basis.getNSlice()):
            for iSliceTest in range(basis.getNSlice()):
                for iRingSource in range(basis.getNRing()):
                    for iRingTest in range(basis.getNRing()):
                        weight = basis.getWeight(iSliceSource,iRingSource);
                        if basis.getNDim() == 2:
                            basisIndexForTest = basis.getIndexForSlice(iSliceTest,iRingTest);
                            retVal[basisIndexForTest+1,sourceBunchIndex+basis.getIndexForSlice(iSliceSource,iRingSource)] = dipx*cst*self._betaX*weight;
                            if self._quadWake:
                                retVal[basisIndexForTest+1,testBunchIndex + basisIndexForTest] = quadx*cst*self._betaX*weight;
                        elif basis.getNDim() == 4:
                            basisIndexForTest = basis.getIndexForSlice(iSliceTest,iRingTest);
                            retVal[basisIndexForTest+1,sourceBunchIndex + basis.getIndexForSlice(iSliceSource,iRingSource)] = dipx*cst*self._betaX*weight;
                            retVal[basisIndexForTest+3,sourceBunchIndex + basis.getIndexForSlice(iSliceSource,iRingSource)+2] = dipy*cst*self._betaY*weight;
                            if self._quadWake:
                                retVal[basisIndexForTest+1,testBunchIndex + basis.getIndexForSlice(iSliceSource,iRingSource)] = quadx*cst*self._betaX*weight;
                                retVal[basisIndexForTest+3,testBunchIndex + basis.getIndexForSlice(iSliceSource,iRingSource)+2] = quady*cst*self._betaY*weight;
                        else:
                            raise BimBimError('Multibunch impedance is not implemented in '+str(basis.getNDim())+' dimensions');
        return retVal;

    #Build impedance matrix from wake table
    def getImpedanceMatrix(self,basis,beams,beam,bunch):
        singleMatrix = self.getSingleBunchImpedanceMatrix(basis,bunch);
        #singleMatrix = spm.identity(basis.getBunchBlockSize(),format='dok');
        zMatrix = spm.dok_matrix((basis.getBunchBlockSize(),basis.getBeamBlockSize(beam)));
        sparse_insert(zMatrix,singleMatrix,0,basis.getIndexForBunch(bunch.getNumber()));
        for sourceBunch in beams.getBunchConfig(beam):
            if sourceBunch != None:
                if bunch.getSPosition() > sourceBunch.getSPosition():
                    cMatrix = self.getCouplingMatrix(basis,beam,sourceBunch,bunch);
                    sparse_add(zMatrix,cMatrix); # element wise addition
        return zMatrix;

    def getMatrix(self,beams,pos,basis):
        time0 = time();
        retVal = spm.identity(basis.getSize(),format='dok')
        bunchB1 = beams.getBunchB1(pos);
        if bunchB1 != None :
            beam = 1;
            bunchNb = bunchB1.getNumber()
            print('Building impedance matrix for B1b'+str(bunchNb));
            zMatrix = self.getImpedanceMatrix(basis,beams,beam,bunchB1);
            zMatrix = basis.getFullBunchProjection(beam,bunchNb,zMatrix);
            #myFile = open('debug_impedance_B'+str(beam)+'b'+str(bunchNb)+'.mat','w');
            #printMatrix(myFile,zMatrix);
            #myFile.close();
            retVal = retVal.dot(zMatrix);
        bunchB2 = beams.getBunchB2(pos);
        if bunchB2 != None :
            beam = 2;
            bunchNb = bunchB2.getNumber()
            print('Building impedance matrix for B2b'+str(bunchNb));
            zMatrix = self.getImpedanceMatrix(basis,beams,beam,bunchB2);
            zMatrix = basis.getFullBunchProjection(beam,bunchNb,zMatrix);
            #myFile = open('debug_impedance_B'+str(beam)+'b'+str(bunchNb)+'.mat','w');
            #printMatrix(myFile,zMatrix);
            #myFile.close();
            retVal = retVal.dot(zMatrix);
        time1 = time();
        print('Time to build impedance matrix',time1-time0);
        #myFile = open('debug_impedance.mat','w');
        #printMatrix(myFile,retVal);
        #myFile.close();
        return retVal;

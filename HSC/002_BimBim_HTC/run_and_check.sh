#!/usr/bin/bash

if [ -z "$1" ]
  then
    echo "No test directory supplied."
    exit 1
fi

trap 'echo -ne "Error in $0:${LINENO}\nPWD=${PWD}\n\t${LINENO}\t" >&2; sed -n -e "${LINENO}p" "$0" >&2' ERR
set -e

mkdir -p "$1/HSC"
PATHTEST=$(mktemp --directory "$1/HSC/002_BimBim_HTC_XXXXXX")
export PATHTEST
echo "Starting on ${HOSTNAME} with eos-$(rpm -qf /usr/bin/eosxd --queryformat '%{V}-%{R}') at $(date) using ${PATHTEST}"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
MYPATH=$(pwd)

STARTTIME=$( date '+%s.%03N' )

cp -r $DIR/* "${PATHTEST}/"
cd "$PATHTEST"

source /cvmfs/sft.cern.ch/lcg/views/LCG_93python3/x86_64-centos7-gcc7-opt/setup.sh

python3 ./runTest.py "$PATHTEST"
cd "$MYPATH"

echo 'HTCondor job submitted, waiting for completion'
ENDTIME=$( date '+%s.%03N' )
RUNTIME_init=$( echo "$ENDTIME - $STARTTIME" | bc -l )

## wait for CONDOR job(s)
joblog="$PATHTEST"/BimBimTest.log
if [[ ! -s "${joblog}" ]]; then
     echo "$(date): HTCondor joblog '${joblog}' is still empty or missing, waiting"
     sleep 60
     if [[ ! -e "${joblog}" ]]; then
        echo "ERROR: HTCondor joblog ${joblog} still not created after 60sec - job submission failed" >&2
        exit 61
     fi
     if [[ ! -s "${joblog}" ]]; then
        echo "ERROR: HTCondor joblog ${joblog} still empty after 60sec - job submission failed" >&2
        exit 62
     fi
fi
condor_wait -status "$joblog"
jobid=$( sed -ne '1s/^000 (\([.0-9]\+\)) .*/\1/p' "$joblog" )
wallclocktime_tmp=$(condor_history -limit 1 -long "${jobid}" | grep RemoteWallClockTime | cut -d' ' -f 3)
if [[ -z "${wallclocktime_tmp}" ]]; then
     ENDTIME_job=$( date '+%s.%03N' )
     wallclocktime_tmp=$( echo "0$ENDTIME_job - 0$STARTTIME" | bc -l )
     echo "WARN: HTCondor does not return RemoteWallClockTime for job ${jobid}, using wall clock" >&2
fi
RUNTIME=$( echo "0${RUNTIME_init}+0${wallclocktime_tmp}" | bc -l)

ENDTIME_REAL=$( date '+%s.%03N' )
RUNTIME_REAL=$( echo "0${ENDTIME_REAL} - 0${STARTTIME}" | bc -l )

## check
if ! $DIR/check.py "$PATHTEST"; then
  echo 'Test failed' >&2
  echo "Test dir left in place: ${PATHTEST}"
  exit 1
fi

echo 'Test succeeded' >&2
echo "test_complete_test_condor_time_runtime_value_for_our_high_level_driver: ${RUNTIME}"
echo "test_runtime_real: ${RUNTIME_REAL}"
# cleanup
rm -rf "${PATHTEST}" >& /dev/null ||:
exit 0

#!/bin/bash

source /cvmfs/sft.cern.ch/lcg/views/LCG_93python3/x86_64-centos7-gcc7-opt/setup.sh
export PYTHONPATH=$PYTHONPATH:$1:$1/BimBim
echo $PYTHONPATH
python3 BimBimTest.py $1 $2 $3 $4 $5 $6 $7

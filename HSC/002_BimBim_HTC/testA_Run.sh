#!/usr/bin/bash

if [ -z "$1" ]
  then
    echo "No test directory supplied."
    exit 1
fi
PATHTEST=$1/HSC/afs_phaseout_temptests_bimbimhtc

MYPATH=$(pwd)
set -e
mkdir -p $PATHTEST
cp -r ./* $PATHTEST/
cd $PATHTEST
source /cvmfs/sft.cern.ch/lcg/views/LCG_93python3/x86_64-centos7-gcc7-opt/setup.sh

python3 ./runTest.py $PATHTEST
cd $MYPATH

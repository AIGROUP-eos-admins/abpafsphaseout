#!/usr/bin/env python3


import sys,os,resource
import numpy as np

from PySSD.Detuning import LinearDetuning,FootprintDetuning
from PySSD.Distribution import Gaussian
from PySSD.Integrator import FixedTrapezoidalIntegrator
from PySSD.Footprint import Footprint,parseDynapTune

def findQs(detuning,stepSize=5E-5,maxJ=18.0,dJ=0.1,margin=2):
    myMin = 1.0;
    myMax = 0.0;
    for jx in np.arange(0.0,maxJ,dJ):
        for jy in np.arange(0.0,maxJ,dJ):
            value = detuning(jx,jy);
            if value < myMin :
                myMin = value;
            if value > myMax :
                myMax = value;
    return np.arange(myMin-margin*stepSize,myMax+margin*stepSize,stepSize);

# args : input directory, containing MADX output 'dynaptune', plane (H or V)
if __name__ == '__main__':
    resource.setrlimit(resource.RLIMIT_CORE,(0,0));
    inputDir = sys.argv[1];
    if len(sys.argv)>3:
        maxAmpl = float(sys.argv[3])
    else:
        maxAmpl = 6.0

    inputFileName = 'dynaptune';
    if not os.path.exists(inputDir):
        print('Input directory',inputDir,'does not exist, abort');
        exit();
    os.chdir(inputDir);
    if not os.path.exists(inputFileName):
        print(inputFileName,'does not exist, abort');
        exit();
    
    plane = sys.argv[2];
    if  plane == 'H':
        planeIndex = 0;
    elif plane == 'V':
        planeIndex = 1;
    else:
        print('Bad input for plane :'+plane+', abort');
        exit();
    outputFileName = plane+'_'+str(maxAmpl)+'sigma.sdiag';
    
    strRep = parseDynapTune(inputFileName,11,51); # set number of amplitude and angles in the file. (MADX + 1)
    footprint = Footprint(strRep,dSigma=1.0); # dSigma : amplitude difference between two indices in the amplitude loop (in unit of sigma)
    footprint.repair() # remove faulty points of the footprint
    detuning = FootprintDetuning(footprint,planeIndex);
    #TODO flip distribution according to plane, in case of asymetric distr.
    distrib = Gaussian();
        
    maxJ = maxAmpl**2/2;
    epsilon = 2E-6;

    integrator = FixedTrapezoidalIntegrator(distrib,detuning,maxJ=maxJ);
    
    Qs = findQs(detuning,maxJ=maxJ,stepSize=1E-3,margin=2);
    tuneShifts = [];
    for i in range(len(Qs)):
        print(i,'/',len(Qs));
        tuneShift = integrator.integrate(Qs[i],epsilon=epsilon);
        print(tuneShift);
        tuneShifts.append(tuneShift);
        myFile = open(outputFileName,'a');
        myFile.write(str(tuneShift.real)+','+str(tuneShift.imag)+'\n');
        myFile.close();

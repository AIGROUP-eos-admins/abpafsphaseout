#!/usr/bin/sh

#args : directory containing a file footprint.mad

cd $1;

# madx is not in the default path - /cvmfs?
MADX=$(type -p madx) ||:
MADX=${MADX:-/afs/cern.ch/project/mad/madx/releases/last-rel/madx-linux64-gnu}

"$MADX" < footprint.mad > output;
rm -f $1/lyapunov.data;
rm -f $1/._*;
rm -f $1/fort.90;

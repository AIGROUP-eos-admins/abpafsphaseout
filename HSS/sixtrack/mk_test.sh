#!/usr/bin/bash

source /cvmfs/sft.cern.ch/lcg/views/LCG_94python3/x86_64-slc6-gcc8-opt/setup.sh

start=$SECONDS

testdir=`date +%Y%m%dT%H%M%S`
mkdir $testdir

echo "START: $SECONDS seconds" >$testdir/test_summary.txt

(cd $testdir;
 git clone https://github.com/SixTrack/SixTrack &>test_gitclone.txt
 status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`
)
echo "gitclone: $status, $SECONDS seconds, " >>$testdir/test_summary.txt

(cd $testdir/SixTrack;
 ./buildLibraries.sh naff libarchive &>test_build.txt;
 ./cmake_six gfortran release LIBARCHIVE BUILD_TESTING &>>test_build.txt;
 status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`
)
echo "build: $status, $SECONDS seconds, " >>$testdir/test_summary.txt

(cd $testdir/SixTrack/build/SixTrack_cmakesix_BUILD_TESTING_LIBARCHIVE_gfortran_release/;
 ctest -L fast -E CheckBuildManual &>test_test.txt;
 status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`;
)
echo "test: $status, $SECONDS seconds, " >>$testdir/test_summary.txt

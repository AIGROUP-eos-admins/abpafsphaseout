Covers:
- MadX: build and test
- SixTrack: build and test
- SixDesk: complex studies run madx, sixtrack locally and in htcondor

Usage:

cd sixtrack; mk_test.sh
cd madx; mk_test.sh
cd sixdesk; mk_test.sh

Everything is run in the folder where the scripts are located (subfolders will be created). 
The scripts can be copied to a different path to test a different filesystem.

Does not cover:
- UI application in the technical network needs to be tested separately

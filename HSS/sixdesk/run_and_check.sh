#!/usr/bin/bash

## set -e # does not work with explicit checks below

if [ -z "$1" ]; then
  echo "No test directory supplied." >&2
  exit 1
fi
mkdir -p "$1/HSS"
PATHTEST=$(mktemp --directory "$1/HSS/sixdesk_XXXXXX")
export PATHTEST
echo "Starting on ${HOSTNAME} with eos-$(rpm -qf /usr/bin/eosxd --queryformat '%{V}-%{R}') at $(date) using ${PATHTEST}"

status='unknown'


### Definitions
workspace=`date +%Y%m%dT%H%M%S`
export appNameDef="sixtrack"
export newBuildPathDef="/afs/cern.ch/project/sixtrack/build"
export SixDeskTools="/afs/cern.ch/project/sixtrack/SixDesk_utilities/pro"

cd "$PATHTEST"




### Create workspace
$SixDeskTools/utilities/bash/set_env.sh -N "$workspace"
if [[ $? -ne 0 ]]; then
    echo "ERROR: set_env " >&2
    exit 71
fi
## cannot print this earlier: the above creates the 'workspace', the directory must not exist before
echo "START: $SECONDS seconds" >"$workspace"/test_summary.txt


### Submit madx jobs

(
 cd $workspace/sixjobs
 $SixDeskTools/utilities/bash/set_env.sh -n -l -c
 sed -i 's/hl13B1/hl10BaseB1/' sixdeskenv
 $SixDeskTools/utilities/bash/set_env.sh -s
 $SixDeskTools/utilities/bash/mad6t.sh -s
)
status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`

echo "submit madx files: $status, $SECONDS seconds" >>$workspace/test_summary.txt

if [[ "$status" != 'pass' ]]; then
    echo "ERROR: submit madx status=$status" >&2
    exit 72
fi


### Check madx jobs

(
 cd $workspace/sixjobs
 $SixDeskTools/utilities/bash/mad6t.sh -c
 while [ $? -ne 0 ];
 do
    sleep 60
    $SixDeskTools/utilities/bash/mad6t.sh -c
 done
)
status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`

echo "check madx files: $status, $SECONDS seconds" >>$workspace/test_summary.txt

if [[ "$status" != 'pass' ]]; then
    echo "ERROR: check madx status=$status" >&2
    exit 73
fi


### Submit SixTrack Jobs

(
 cd $workspace/sixjobs;
 $SixDeskTools/utilities/bash/run_six.sh -a
)
status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`

echo "run_sixtrack files: $status, $SECONDS seconds" >>$workspace/test_summary.txt


if [[ "$status" != 'pass' ]]; then
    echo "ERROR: run_sixtrack files  status=$status" >&2
    exit 74
fi




### Check Results

check_results () {
    $SixDeskTools/utilities/bash/run_six.sh -t | grep "fort.10.gz FOUND: 1200"
}

(
 cd $workspace/sixjobs;
 check_results
 while [ $? -ne 0 ];
 do
    sleep 60
    check_results
 done
)
status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`

echo "check sixtrack jobs: $status, $SECONDS seconds" >>$workspace/test_summary.txt

if [[ "$status" != 'pass' ]]; then
    echo "ERROR: check sixtrack jobs  status=$status" >&2
    exit 75
fi



### Resubmit sixtrack jobs

(
 cd $workspace/sixjobs;
 # '-S' option is deprecated and caused script to fail, but error was ignored?
 $SixDeskTools/utilities/bash/run_six.sh -s
 check_results
 while [ $? -ne 0 ];
 do
    sleep 60
    check_results
 done
)
status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`

echo "check resubmitted sixtrack jobs: $status, $SECONDS seconds" >>$workspace/test_summary.txt

if [[ "$status" != 'pass' ]]; then
    echo "ERROR: check resubmitted sixtrack jobs status=$status" >&2
    exit 76
fi

### Post processing`
status='fail'
(
 # 'sixdb' needs 'python' to be 3.7+, and needs to have numpy scipy matplotlib ...
 # hence using some random version from CVMFS
 SIXPYTHON="/cvmfs/sft.cern.ch/lcg/releases/LCG_98python3/Python/3.7.6/x86_64-centos7-gcc8-opt"
 source "${SIXPYTHON}/Python-env.sh"
 ### original test continues here, with addition of -P ...
 cd $workspace/sixjobs;
 $SixDeskTools/utilities/bash/sixdb.sh -P "${SIXPYTHON}/bin" . load_dir &>test_da.txt;
 $SixDeskTools/utilities/bash/sixdb.sh -P "${SIXPYTHON}/bin" hl10BaseB1.db da &>>test_da.txt;
 grep "Average: .* Sigma" test_da.txt      # for the logs
 sigmacount=$(grep -c '^Average: 1[23]\.[0-9]\+ Sigma' test_da.txt)
 if [ "0${sigmacount}" -ge 2 ]; then
   echo "found correct number of Sigma lines in test_da.txt"
 else
   echo "ERROR: sixdb check of numeric results failed, got ${sigmacount} instead of 2. See test_da.txt :" >&2
   sed -n -e '/^Angle/,$p'  test_da.txt >&2
   false
 fi
)
status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`

echo "check postprocessing: $status, $SECONDS seconds" >>$workspace/test_summary.txt

if [[ "$status" != 'pass' ]]; then
    echo "ERROR: postprocessing status=$status" >&2
    exit 77
fi

echo 'all OK'
rm -rf "$PATHTEST" >& /dev/null ||:
exit 0

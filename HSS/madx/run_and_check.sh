#!/usr/bin/bash
start=$SECONDS

trap 'echo -ne "Error in $0:${LINENO}\nPWD=${PWD}\n\t${LINENO}\t" >&2; sed -n -e "${LINENO}p" "$0" >&2' ERR
set -e

if [ -z "$1" ]; then
  echo "No test directory supplied." >&2
  exit 1
fi
mkdir -p "$1/HSS"
PATHTEST=$(mktemp --directory "$1/HSS/madx_XXXXXX")
export PATHTEST
echo "Starting on ${HOSTNAME} with eos-$(rpm -qf /usr/bin/eosxd --queryformat '%{V}-%{R}') at $(date) using ${PATHTEST}"

echo "START: $SECONDS seconds" >"$PATHTEST"/test_summary.txt

status='unknown'
if [ -d "$PATHTEST/madx-nightly" ] || [ -f "$PATHTEST/madx-nightly" ]; then
    (cd "$PATHTEST";
     madx-nightly/scripts/build-test-lxplus7.sh update &>stdouterr_buildandtest.txt)
    status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`
    echo "buildandtest: $status, $SECONDS seconds" >>"$PATHTEST"/test_summary.txt
else    
    (cd "$PATHTEST";
     git clone https://github.com/MethodicalAcceleratorDesign/MAD-X.git \
     madx-nightly &>stdouterr_gitclone.txt)

    status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`
    echo "gitclone: $status, $SECONDS seconds, " >>"$PATHTEST"/test_summary.txt

    (cd "$PATHTEST";
     madx-nightly/scripts/build-test-lxplus7.sh update &>stdouterr_buildandtest.txt)

    status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`
    echo "buildandtest: $status, $SECONDS seconds" >>"$PATHTEST"/test_summary.txt
fi

# the above might silently ignore some errors?
# some 'FAILED' are apparently OK, but tests need to have run
if [[ "$status" != 'pass' ]]; then
    echo "ERROR: script ran, and gave status=$status" >&2
    exit 72
fi

if tail "$PATHTEST"/stdouterr_buildandtest.txt | grep -q '^ *total run.*total files.* PASSED .* FAILED'; then
    echo 'all OK'
    rm -rf "$PATHTEST" >& /dev/null ||:
    exit 0
fi

echo "ERROR: script did not finish running the tests." >&2
echo "$PATHTEST"/stdouterr_buildandtest.txt ends with:" >&2
tail "$PATHTEST"/stdouterr_buildandtest.txt" >&2
exit 75

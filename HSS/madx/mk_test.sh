#!/usr/bin/bash
start=$SECONDS

set -e

if [ -z "$1" ]; then
  echo "No test directory supplied." >&2
  exit 1
fi
PATHTEST="$1"/HSS/madx

mkdir -p "$PATHTEST"

echo "START: $SECONDS seconds" >"$PATHTEST"/test_summary.txt

status='unknown'
if [ -d "$PATHTEST/madx-nightly" ] || [ -f "$PATHTEST/madx-nightly" ]; then
    (cd "$PATHTEST";
     madx-nightly/scripts/build-test-lxplus7.sh update &>stdouterr_buildandtest.txt)
    status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`
    echo "buildandtest: $status, $SECONDS seconds" >>"$PATHTEST"/test_summary.txt
else    
    (cd "$PATHTEST";
     git clone https://github.com/MethodicalAcceleratorDesign/MAD-X.git \
     madx-nightly &>stdouterr_gitclone.txt)

    status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`
    echo "gitclone: $status, $SECONDS seconds, " >>"$PATHTEST"/test_summary.txt

    (cd "$PATHTEST";
     madx-nightly/scripts/build-test-lxplus7.sh update &>stdouterr_buildandtest.txt)

    status=`[ $? -eq 0 ] && echo "pass" || echo "fail"`
    echo "buildandtest: $status, $SECONDS seconds" >>"$PATHTEST"/test_summary.txt
fi

# the above might silently ignore some errors?
# some 'FAILED' are apparently OK, but tests need to have run
if [[ "$status" != 'pass' ]]; then
    echo "ERROR: script ran, and gave status=$status" >&2
    exit 72
fi

if tail stdouterr_buildandtest.txt | grep -q '^ *total run.*total files.* PASSED .* FAILED'; then
    echo 'all OK'
    exit 0
fi

echo "ERROR: script did not finish running the tests" >&2
exit 75

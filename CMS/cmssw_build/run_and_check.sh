#!/usr/bin/bash
## how long does it take to build a CMS software package
## ought to be around 5min. Comes from RQF1588841

trap 'echo -ne "Error in $0:${LINENO}\nPWD=${PWD}\n\t${LINENO}\t" >&2; sed -n -e "${LINENO}p" "$0" >&2' ERR
set -e

if [ -z "$1" ]; then
  echo "No test directory supplied." >&2
  exit 1
fi

SECONDS=0

mkdir -p "$1/CMS"
PATHTEST=$(mktemp --directory "$1/CMS/cmssw_XXXXXX")
export PATHTEST

# "git cms-addpkg" wants this. CC7 seems to not honour XDG_CONFIG_HOME, so set bogus HOME
if ! git config  --global --get user.github >/dev/null; then
        echo "## using tmp GIT config" >&2
        export XDG_CONFIG_HOME="${PATHTEST}"
        mkdir -p "${XDG_CONFIG_HOME}/git"
        touch "${XDG_CONFIG_HOME}/git/config"
        HOME="${PATHTEST}" git config --global --add user.github 'nobody'
        HOME="${PATHTEST}" git config --global --add user.name   'No Body'
        HOME="${PATHTEST}" git config --global --add user.email   'noreply@cern.ch'
fi

echo "Starting on ${HOSTNAME} with eos-$(rpm -qf /usr/bin/eosxd --queryformat '%{V}-%{R}') at $(date) using ${PATHTEST}"

status='unknown'

source /cvmfs/cms.cern.ch/cmsset_default.sh
echo "Setup CMS environment took ${SECONDS}s"
cd "$PATHTEST"
SECONDS=0

scram -a slc7_amd64_gcc820 project CMSSW_11_1_0_pre8
echo "Set up the local cmssw release took ${SECONDS}s"
SECONDS=0

cd CMSSW_11_1_0_pre8 ; eval $(scram runtime -sh)
echo "setup cmssw environment took ${SECONDS}s"
SECONDS=0

HOME="${PATHTEST}" git cms-addpkg FWCore/Framework
echo "Initialize git and checkout a cmssw package took ${SECONDS}s"
SECONDS=0

scram build -j 8
echo "build cmssw took ${SECONDS}s"
SECONDS=0

test/*/TestFWCoreFramework
echo "tests took ${SECONDS}s"

echo 'all OK'
rm -rf "$PATHTEST" >& /dev/null ||:
exit 0

#!/usr/bin/bash
## how long does it take to build a MadGraph5_aMCatNLO "gridpack"
## Comes from RQF2098581
## 2022-08-15: only works on CC7

trap 'echo -ne "Error in $0:${LINENO}\nPWD=${PWD}\n\t${LINENO}\t" >&2; sed -n -e "${LINENO}p" "$0" >&2' ERR
set -e

if [ -z "$1" ]; then
  echo "No test directory supplied." >&2
  exit 1
fi

OURDIR=$(dirname "${0}")
SECONDS=0

mkdir -p "$1/CMS"
PATHTEST=$(mktemp --directory "$1/CMS/madgraph_gridpack_XXXXXX")
export PATHTEST

echo "Starting on ${HOSTNAME} with eos-$(rpm -qf /usr/bin/eosxd --queryformat '%{V}-%{R}') at $(date) using ${PATHTEST}"

git clone --quiet https://github.com/cms-sw/genproductions "${PATHTEST}"
echo "git clone took ${SECONDS}s"
SECONDS=0

carddir="bin/MadGraph5_aMCatNLO/cards/test"
mkdir "${PATHTEST}/${carddir}"
cp -av "${OURDIR}"/cards/*dat "${PATHTEST}/${carddir}/"
echo "card data copy took ${SECONDS}s"
SECONDS=0

cd "${PATHTEST}/bin/MadGraph5_aMCatNLO/"
ANALYSIS="WprimeToENu_M-1000_kR-0p01_LO_NNPDF31nnlo"
## very chatty, redirect to file
echo "starting gridpack generation"
./gridpack_generation.sh "${ANALYSIS}" cards/test > "${PATHTEST}/test_out.txt"
echo "gridpack generation took ${SECONDS}s"

## any test we could run at the end? file existence etc?
/bin/ls -lh "${PATHTEST}/bin/MadGraph5_aMCatNLO/${ANALYSIS}"_*_CMSSW_*_tarball.tar.xz

echo 'all OK'
rm -rf "$PATHTEST" >& /dev/null ||:
exit 0

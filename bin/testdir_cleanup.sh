#!/bin/bash
#
# delete old test directories
MAXAGE=4
TEST_EOS='/eos/user/a/acboxtest/be-abp-tests/tests'
TEST_AFS='/afs/cern.ch/work/a/acboxtest/be-abp-tests/tests'

find "${TEST_EOS}" "${TEST_AFS}" -maxdepth 2 -mindepth 2 -type d -name '*_??????' -mtime +"${MAXAGE}" -exec /bin/rm -rf \{\} \;

